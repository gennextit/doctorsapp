package com.sarvodaya.doctorsapp.model;

import java.util.List;

public class UserChatModel {
	private String uname;
	private String cuname;
	private String text;
	private String date;
	private String time;
	private String Position;
	private List<UserChatModel> ChatList;

	
	public String getUName() {
		return uname;
	}

	public void setUName(String uname) {
		this.uname = uname;
	}
	
	public String getCUName() {
		return cuname;
	}

	public void setCUname(String cuname) {
		this.cuname = cuname;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public String getPosition() {
		return Position;
	}

	public void setPosition(String Position) {
		this.Position = Position;
	}
	
	public List<UserChatModel> getChatList() {
		return ChatList;
	}

	public void setChatList(List<UserChatModel> ChatList) {
		this.ChatList = ChatList;
	}
	
}
