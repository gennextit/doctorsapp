package com.sarvodaya.doctorsapp.model.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.opd.OPDFragment;
import com.sarvodaya.doctorsapp.model.OPDModel;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;


import java.util.ArrayList;

/**
 * Created by Abhijit on 26-Sep-16.
 */

public class OPDAdapter extends ArrayAdapter<OPDModel> {
    private final ArrayList<OPDModel> list;


    private int tabSelected;
    private Activity context;
    private ProgressDialog progressDialog;
    AssignTask assignTask;
    OPDFragment opdFragment;
    //public ImageLoader imageLoader;

    public void onAttach(Activity activity) {
        if(assignTask!=null){
            assignTask.onAttach(context);
        }
    }

    public void onDetach() {
        if(assignTask!=null){
            assignTask.onDetach();
        }
    }

    public OPDAdapter(Activity context, int textViewResourceId, ArrayList<OPDModel> list, int tabSelected, OPDFragment opdFragment) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        this.tabSelected = tabSelected;
        this.opdFragment=opdFragment;
        //imageLoader = new ImageLoader(context.getApplicationContext());

    }

    class ViewHolder {
        TextView AppNo, PatientName, Date;
//        public ImageView call, InOut;
        public LinearLayout llcall, llIn,llOut,llUnCall;

        public ViewHolder(View v) {
//            call = (ImageView) v.findViewById(R.id.iv_opd_custom_slot_call);
//            InOut = (ImageView) v.findViewById(R.id.iv_opd_custom_slot_in_out);
            AppNo = (TextView) v.findViewById(R.id.tv_opd_custom_list_appId);
            PatientName = (TextView) v.findViewById(R.id.tv_opd_custom_list_name);
            Date = (TextView) v.findViewById(R.id.tv_opd_custom_slot_date);
            llcall= (LinearLayout) v.findViewById(R.id.ll_opd_custom_slot_call);
            llIn= (LinearLayout) v.findViewById(R.id.ll_opd_custom_slot_in);
            llOut= (LinearLayout) v.findViewById(R.id.ll_opd_custom_slot_out);
            llUnCall= (LinearLayout) v.findViewById(R.id.ll_opd_custom_slot_uncall);

        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_opd, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.llcall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(list.get(position).getCallStatus().toLowerCase().equals("uncall")){
                    assignTask = new AssignTask(context);
                    assignTask.execute(AppSettings.CallPatient + "?DoctorId=" + Doctor.getDoctorId(context)
                            + "&App_ID=" + list.get(position).getAppId());
//                        executeList();
                }
//                switch (list.get(position).getCallStatus().toLowerCase()) {
//                    case "uncall":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.CallPatient+"?DoctorId="+LoadPreferences("DoctorId")+"&App_ID="+list.get(position).getAppId());
//
//
//                        break;
//                    case "call":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.UnCallPatient+"?App_ID="+list.get(position).getAppId());
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.UnCallPatient + "?App_ID=" + list.get(position).getAppId());
////                        getPosts.execute();
////
////                        executeList();
//                        break;
//                }
                // util.showToast( "Message has been send to patient.");
            }
        });
        holder.llUnCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(list.get(position).getCallStatus().toLowerCase().equals("call")){
                    assignTask = new AssignTask(context);
                    assignTask.execute(AppSettings.UnCallPatient + "?App_ID=" + list.get(position).getAppId());
//                        getPosts.execute();
//
                }
//                switch (list.get(position).getCallStatus().toLowerCase()) {
//                    case "uncall":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.CallPatient+"?DoctorId="+LoadPreferences("DoctorId")+"&App_ID="+list.get(position).getAppId());
//
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.CallPatient + "?DoctorId=" + Doctor.getDoctorId(context)
//                                + "&App_ID=" + list.get(position).getAppId());
////                        executeList();
//                        break;
//                    case "call":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.UnCallPatient+"?App_ID="+list.get(position).getAppId());
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.UnCallPatient + "?App_ID=" + list.get(position).getAppId());
////                        getPosts.execute();
////
////                        executeList();
//                        break;
//                }
                // util.showToast( "Message has been send to patient.");
            }
        });
        holder.llIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(list.get(position).getCallStatus().toLowerCase().equals("call")){
                    assignTask = new AssignTask(context);
                    assignTask.execute(AppSettings.InPatient + "?DoctorId=" + Doctor.getDoctorId(context)
                            + "&App_ID=" + list.get(position).getAppId());
                }
//                switch (list.get(position).getCallStatus().toLowerCase()) {
//                    case "call":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.InPatient+"?DoctorId="+LoadPreferences("DoctorId")+"&App_ID="+list.get(position).getAppId());
////                        getPosts.execute(AppSettings.InPatient + "?DoctorId=" + LoadPreferences(AppTokens.DOCTORID)
////                                + "&App_ID=" + list.get(position).getAppId());
////
////                        executeList();
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.InPatient + "?DoctorId=" + Doctor.getDoctorId(context)
//                                + "&App_ID=" + list.get(position).getAppId());
//                        break;
//                    case "in":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.OutPatient+"?App_ID="+list.get(position).getAppId());
////                        getPosts.execute(AppSettings.OutPatient + "?App_ID=" + list.get(position).getAppId());
////
////                        executeList();
//
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.OutPatient + "?App_ID=" + list.get(position).getAppId());
//                        break;
//                }
                // util.showToast( "Message has been send to patient.");
            }
        });

        holder.llOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(list.get(position).getCallStatus().toLowerCase().equals("in")){
                    assignTask = new AssignTask(context);
                    assignTask.execute(AppSettings.OutPatient + "?App_ID=" + list.get(position).getAppId());
                }
//                switch (list.get(position).getCallStatus().toLowerCase()) {
//                    case "call":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.InPatient+"?DoctorId="+LoadPreferences("DoctorId")+"&App_ID="+list.get(position).getAppId());
////                        getPosts.execute(AppSettings.InPatient + "?DoctorId=" + LoadPreferences(AppTokens.DOCTORID)
////                                + "&App_ID=" + list.get(position).getAppId());
////
////                        executeList();
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.InPatient + "?DoctorId=" + Doctor.getDoctorId(context)
//                                + "&App_ID=" + list.get(position).getAppId());
//                        break;
//                    case "in":
//                        // new
//                        // UpdatePatientDetail().execute(AppSettings.OutPatient+"?App_ID="+list.get(position).getAppId());
////                        getPosts.execute(AppSettings.OutPatient + "?App_ID=" + list.get(position).getAppId());
////
////                        executeList();
//
//                        assignTask = new AssignTask(context);
//                        assignTask.execute(AppSettings.OutPatient + "?App_ID=" + list.get(position).getAppId());
//                        break;
//                }
                // util.showToast( "Message has been send to patient.");
            }
        });
        if (list.get(position).getCallStatus() != null) {
            switch (list.get(position).getCallStatus().toLowerCase()) {
                case "uncall":
                    switch (list.get(position).getPaymentStatus().toLowerCase()) {
                        case "unpaid":
//                            v.setBackgroundResource(R.drawable.custom_slot_available);
                            holder.llcall.setVisibility(View.GONE);
                            holder.llUnCall.setVisibility(View.GONE);
                            holder.llIn.setVisibility(View.GONE);
                            holder.llOut.setVisibility(View.GONE);
                            holder.AppNo.setText(list.get(position).getAppNo());
                            holder.PatientName.setText(list.get(position).getPatientName());
                            holder.Date.setText(list.get(position).getAppTime());
                            break;
                        case "paid":
//                            convertView.setBackgroundResource(R.drawable.custom_slot_available);
//                            holder.call.setImageResource(R.drawable.call_60x60);
                            holder.llcall.setVisibility(View.VISIBLE);
                            holder.llUnCall.setVisibility(View.GONE);
                            holder.llIn.setVisibility(View.GONE);
                            holder.llOut.setVisibility(View.GONE);
                            holder.AppNo.setText(list.get(position).getAppNo());
                            holder.PatientName.setText(list.get(position).getPatientName());
                            holder.Date.setText(list.get(position).getAppTime());
                            break;
                    }
                    break;
                case "call":
//                    convertView.setBackgroundResource(R.drawable.custom_slot_available);
//                    holder.call.setImageResource(R.drawable.call_end_60x60);
                    holder.llcall.setVisibility(View.GONE);
                    holder.llUnCall.setVisibility(View.VISIBLE);
                    holder.llIn.setVisibility(View.VISIBLE);
                    holder.llOut.setVisibility(View.GONE);

                    holder.AppNo.setText(list.get(position).getAppNo());
                    holder.PatientName.setText(list.get(position).getPatientName());
                    holder.Date.setText(list.get(position).getAppTime());
                    break;
                case "in":
//                    convertView.setBackgroundResource(R.drawable.custom_slot_available);
                    holder.AppNo.setText(list.get(position).getAppNo());
                    holder.llcall.setVisibility(View.GONE);
                    holder.llUnCall.setVisibility(View.GONE);
                    holder.llIn.setVisibility(View.GONE);
                    holder.llOut.setVisibility(View.VISIBLE);

//                    holder.call.setVisibility(View.GONE);
//                    holder.InOut.setVisibility(View.VISIBLE);
//                    holder.InOut.setImageResource(R.drawable.out_60x60);
                    holder.PatientName.setText(list.get(position).getPatientName());
                    holder.Date.setText(list.get(position).getAppTime());
                    break;
                case "out":
//                    convertView.setBackgroundResource(R.drawable.custom_slot_available);
//                    holder.call.setVisibility(View.GONE);
//                    holder.InOut.setVisibility(View.GONE);

                    holder.llcall.setVisibility(View.GONE);
                    holder.llUnCall.setVisibility(View.GONE);
                    holder.llIn.setVisibility(View.GONE);
                    holder.llOut.setVisibility(View.GONE);

                    holder.AppNo.setText(list.get(position).getAppNo());
                    holder.PatientName.setText(list.get(position).getPatientName());
                    holder.Date.setText(list.get(position).getAppTime());
                    break;

            }
        }


        return v;
    }


    public class AssignTask extends AsyncTask<String, String, String> {

        private Activity activity;

        public AssignTask(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            opdFragment.showProgressDialog(activity, "Processing, please wait... ", Gravity.CENTER);

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            JsonParser jsonParser = new JsonParser(context);
//            response = ob.makeConnection(urls[0], HttpReq.GET, null);
            response= ApiCall.GET(urls[0]);
            response = jsonParser.getOPDSlotStatus(response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {

                if (result != null) {
                    //Toast.makeText(context, result, Toast.LENGTH_LONG).show();
                    opdFragment.executeAssignTask();
                } else {
                    opdFragment.hideProgressDialog();
                    opdFragment.showInternetPopUpDialog(activity,JsonParser.ERRORMESSAGE);
//                    L.m(JsonParser.ERRORMESSAGE);
                }
            }
        }
    }

//    protected void showProgressDialog(Activity context, String Msg, int gravity) {
//        if (progressDialog == null)
//            progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage(Msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(true);
//        progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
//        if (gravity == Gravity.BOTTOM) {
//            progressDialog.getWindow().setGravity(Gravity.BOTTOM);
//        }
//        progressDialog.show();
//    }
//
//    protected void hideProgressDialog() {
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
//    }

}
