package com.sarvodaya.doctorsapp.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class OTBookingModel {

    public static final int LIST_OF_OT=1,REQUEST_OT_BOOKING=2;
    public static final String OT_ID= "OTId";
    public static final String OT_NAME= "OTName";

    private String output;
    private String outputError;
    private ArrayList<OTBookingModel>list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputError() {
        return outputError;
    }

    public void setOutputError(String outputError) {
        this.outputError = outputError;
    }

    public ArrayList<OTBookingModel> getList() {
        return list;
    }

    public void setList(ArrayList<OTBookingModel> list) {
        this.list = list;
    }

    private String OTId;
    private String OTName;

    public String getOTId() {
        return OTId;
    }

    public void setOTId(String OTId) {
        this.OTId = OTId;
    }

    public String getOTName() {
        return OTName;
    }

    public void setOTName(String OTName) {
        this.OTName = OTName;
    }
}
