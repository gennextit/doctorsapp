package com.sarvodaya.doctorsapp.model.adapter;

/**
 * Created by Abhijit on 27-Sep-16.
 */


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.ipd.DischargeDetail;
import com.sarvodaya.doctorsapp.model.IPDModel;

import java.util.ArrayList;

public class IPDAdapter extends ArrayAdapter<IPDModel> {
    private final ArrayList<IPDModel> list;
    private final String doctorId;
    FragmentManager manager;
    private Context context;

    public IPDAdapter(Context context, int textViewResourceId, ArrayList<IPDModel> list, FragmentManager manager, String doctorId) {
        super(context, textViewResourceId, list);
        this.manager=manager;
        this.context = context;
        this.list = list;
        this.doctorId=doctorId;

    }

    class ViewHolder {
        LinearLayout llDischargeSummary,llSummaryStatus;
        TextView summaryStatus,MRNo,PatientName,AdmitDate,AdmitTime,Floor,Room_No,Room_Name,Bed_No;

        public ViewHolder(View v) {
            llDischargeSummary = (LinearLayout) v.findViewById(R.id.ll_discharge_summary);
            llSummaryStatus = (LinearLayout) v.findViewById(R.id.ll_ipd_custom_list_summary_status);
            summaryStatus = (TextView) v.findViewById(R.id.tv_ipd_custom_list_summary_status);
            MRNo = (TextView) v.findViewById(R.id.tv_ipd_custom_list_MRNo);
            PatientName = (TextView) v.findViewById(R.id.tv_ipd_custom_list_PatientName);
            AdmitDate = (TextView) v.findViewById(R.id.tv_ipd_custom_list_AdmitDate);
            AdmitTime = (TextView) v.findViewById(R.id.tv_ipd_custom_list_AdmitTime);
            Floor = (TextView) v.findViewById(R.id.tv_ipd_custom_list_Floor);
            Room_No = (TextView) v.findViewById(R.id.tv_ipd_custom_list_Room_No);
            Room_Name = (TextView) v.findViewById(R.id.tv_ipd_custom_list_Room_Name);
            Bed_No = (TextView) v.findViewById(R.id.tv_ipd_custom_list_Bed_No);


        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_ipd, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final IPDModel ipdModel = list.get(position);
        if (ipdModel.getMRNo() != null) {
            holder.MRNo.setText(ipdModel.getMRNo());
        }

        if (ipdModel.getPatientName() != null) {
            holder.PatientName.setText(ipdModel.getPatientName());
        }
        if (ipdModel.getAdmitDate() != null) {
            holder.AdmitDate.setText("Admit Date: "+ipdModel.getAdmitDate());
        }
        if (ipdModel.getAdmitTime() != null) {
            holder.AdmitTime.setText("Admit Time: "+ipdModel.getAdmitTime());
        }
        if (ipdModel.getFloor() != null) {
            holder.Floor.setText(ipdModel.getFloor());
        }
        if (ipdModel.getRoom_No() != null) {
            holder.Room_No.setText("Room No: "+ipdModel.getRoom_No());
        }
        if (ipdModel.getRoom_Name() != null) {
            holder.Room_Name.setText("Room Name: "+ipdModel.getRoom_Name());
        }
        if (ipdModel.getBed_No() != null) {
            holder.Bed_No.setText("Bed No: "+ipdModel.getBed_No());
        }
        if (ipdModel.getIsSummaryPrapared() != null && ipdModel.getIsSummaryPrapared().equalsIgnoreCase("No")) {
            holder.llDischargeSummary.setVisibility(View.INVISIBLE);
//            holder.llSummaryStatus.setVisibility(View.GONE);
        }else{
            holder.llDischargeSummary.setVisibility(View.VISIBLE);
//            holder.llSummaryStatus.setVisibility(View.VISIBLE);
        }

        if (ipdModel.getSummary_Status() != null) {
            holder.summaryStatus.setText(ipdModel.getSummary_Status());
        }

        holder.summaryStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DischargeDetail dischargeDetail=DischargeDetail.newInstance(doctorId,ipdModel.getPatientName(),ipdModel.getIPNo(),ipdModel.getSummary_Status());
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(android.R.id.content, dischargeDetail, "dischargeDetail");
                transaction.addToBackStack("dischargeDetail");
                transaction.commit();
            }
        });


        holder.llDischargeSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DischargeSummary dischargeSummary=new DischargeSummary();
//                dischargeSummary.setDetail(ipdModel.getPatientName(),ipdModel.getIPNo());
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.add(android.R.id.content, dischargeSummary, "dischargeSummary");
//                transaction.addToBackStack("dischargeSummary");
//                transaction.commit();
                DischargeDetail dischargeDetail=DischargeDetail.newInstance(doctorId,ipdModel.getPatientName(),ipdModel.getIPNo(),ipdModel.getSummary_Status());
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(android.R.id.content, dischargeDetail, "dischargeDetail");
                transaction.addToBackStack("dischargeDetail");
                transaction.commit();
            }
        });

        return v;
    }


}
