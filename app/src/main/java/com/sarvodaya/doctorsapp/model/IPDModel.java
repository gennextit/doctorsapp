package com.sarvodaya.doctorsapp.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class IPDModel {

    public static final String IS_SUMMARY_PRAPARED= "IsSummaryPrapared";
    public static final String SUMMARY_STATUS= "Summary_Status";
    public static final String TAG_MRNO= "MRNo";
    public static final String TAG_IPNO= "IPNo";
    public static final String TAG_PATIENT_NAME= "PatientName";
    public static final String TAG_ADMIT_DATE= "AdmitDate";
    public static final String TAG_ADMIT_TIME= "AdmitTime";
    public static final String TAG_FLOOR= "Floor";
    public static final String TAG_ROOM_NO= "Room_No";
    public static final String TAG_NAME= "RoomName";
    public static final String TAG_BED_NO= "BedNo";

    // for patient detail
    public static final String TESTNAME= "TestName";
    public static final String PRESCRIBEDATE= "PrescribeDate";
    public static final String SAMPLEDATE= "SampleDate";
    public static final String RESULTDATE= "ResultDate";
    public static final String URL_KEY= "Url";


    private ArrayList<IPDModel>list;

    private String output;
    private String outputError;

    private String IsSummaryPrapared;
    private String Summary_Status;
    private String MRNo;
    private String IPNo;
    private String PatientName;
    private String AdmitDate;
    private String AdmitTime;
    private String Floor;
    private String Room_No;
    private String Room_Name;
    private String Bed_No;

    //for patient detail
    private String TestName;
    private String PrescribeDate;
    private String SampleDate;
    private String ResultDate;
    private String Url;

    public String getSummary_Status() {
        return Summary_Status;
    }

    public void setSummary_Status(String summary_Status) {
        Summary_Status = summary_Status;
    }

    public String getIsSummaryPrapared() {
        return IsSummaryPrapared;
    }

    public void setIsSummaryPrapared(String isSummaryPrapared) {
        IsSummaryPrapared = isSummaryPrapared;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String testName) {
        TestName = testName;
    }

    public String getPrescribeDate() {
        return PrescribeDate;
    }

    public void setPrescribeDate(String prescribeDate) {
        PrescribeDate = prescribeDate;
    }

    public String getSampleDate() {
        return SampleDate;
    }

    public void setSampleDate(String sampleDate) {
        SampleDate = sampleDate;
    }

    public String getResultDate() {
        return ResultDate;
    }

    public void setResultDate(String resultDate) {
        ResultDate = resultDate;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputError() {
        return outputError;
    }

    public void setOutputError(String outputError) {
        this.outputError = outputError;
    }

    public String getMRNo() {
        return MRNo;
    }

    public void setMRNo(String MRNo) {
        this.MRNo = MRNo;
    }

    public String getIPNo() {
        return IPNo;
    }

    public void setIPNo(String IPNo) {
        this.IPNo = IPNo;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getAdmitDate() {
        return AdmitDate;
    }

    public void setAdmitDate(String admitDate) {
        AdmitDate = admitDate;
    }

    public String getAdmitTime() {
        return AdmitTime;
    }

    public void setAdmitTime(String admitTime) {
        AdmitTime = admitTime;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getRoom_No() {
        return Room_No;
    }

    public void setRoom_No(String room_No) {
        Room_No = room_No;
    }

    public String getRoom_Name() {
        return Room_Name;
    }

    public void setRoom_Name(String room_Name) {
        Room_Name = room_Name;
    }

    public String getBed_No() {
        return Bed_No;
    }

    public void setBed_No(String bed_No) {
        Bed_No = bed_No;
    }

    public ArrayList<IPDModel> getList() {
        return list;
    }

    public void setList(ArrayList<IPDModel> list) {
        this.list = list;
    }
}
