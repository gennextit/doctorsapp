package com.sarvodaya.doctorsapp.model;

import java.util.List;

public class JSONModel {

    private String Name;
    private String SurName;
    private String Address;
    private String ChatDetail;
	private List<JSONModel> ChatList;

    // get and set
    public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}
    
	public String getSurName() {
		return SurName;
	}

	public void setSurName(String SurName) {
		this.SurName = SurName;
	}
	
	public String getAddress() {
		return Address;
	}

	public void setAddress(String Address) {
		this.Address = Address;
	}
	
	//Array Model start
	
	
	public String getChatDetail() {
		return ChatDetail;
	}

	public void setChatDetail(String ChatDetail) {
		this.ChatDetail = ChatDetail;
	}
	
	// get and set      
    public List<JSONModel> getChatList() {
		return ChatList;
	}

	public void setChatList(List<JSONModel> ChatList) {
		this.ChatList = ChatList;
	}
	
	
	 	
        
   
}