package com.sarvodaya.doctorsapp.model;


import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class DischargeSummaryModel {

    private String name;
    private String category;
    private String output;
    private String outputMsg;

    private ArrayList<DischargeSummaryModel> groupList;
    private ArrayList<DischargeSummaryModel> childList;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<DischargeSummaryModel> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<DischargeSummaryModel> groupList) {
        this.groupList = groupList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<DischargeSummaryModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<DischargeSummaryModel> childList) {
        this.childList = childList;
    }
}
