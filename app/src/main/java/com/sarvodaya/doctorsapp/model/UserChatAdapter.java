package com.sarvodaya.doctorsapp.model;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sarvodaya.doctorsapp.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class UserChatAdapter extends ArrayAdapter<UserChatModel> {
		private final List<UserChatModel> list;
		TextView utext,uTime,cutext,cuTime,gDate;
		//ImageView online;
		
	  private final Activity context;

	  public UserChatAdapter(Activity context,int textViewResourceId, List<UserChatModel> list) {
		    super(context, textViewResourceId, list);
		    this.context = context;
		    this.list = list;
	  }
	  @Override
	    public int getViewTypeCount() {
	        return 3;
	    }

	    @Override
	    public int getItemViewType(int position) {
	        if (list.get(position).getPosition().equalsIgnoreCase("right")) {
	            return 0;
	        }else if (list.get(position).getPosition().equalsIgnoreCase("left")){
	        	return 1;
	        }
	        return 2;
	    }

	    
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		  View v = convertView; 
		  int type = getItemViewType(position);
		 
		  if (v == null) { 
			  // Inflate the layout according to the view type 
			  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
			  
			  if (type == 0) { 
				  // Inflate the layout with image 
				  v = inflater.inflate(R.layout.user_chat_custom_right_slot, parent, false);
			  } 
			  else if(type == 1) { 
				  v = inflater.inflate(R.layout.user_chat_custom_left_slot, parent, false); 
			  } 
			  else if(type == 2) { 
				  v = inflater.inflate(R.layout.online_offline_custom_slot, parent, false); 
			  } 
		  }
		  if (type == 0) {
			  utext = (TextView) v.findViewById(R.id.tv_user_chat_custom_utext);
		      uTime = (TextView) v.findViewById(R.id.tv_user_chat_custom_uTime);
		      
		      if(!list.get(position).getUName().equalsIgnoreCase("")){
		    	  utext.setText(list.get(position).getText());
			      uTime.setText((convertTime(list.get(position).getTime())));
		      }
		  }else if(type == 1){
			  cutext = (TextView) v.findViewById(R.id.tv_user_chat_custom_text);
		      cuTime = (TextView) v.findViewById(R.id.tv_user_chat_custom_Time);
		       
	    	
		      if(!list.get(position).getUName().equalsIgnoreCase("")){
		    	  cutext.setText(list.get(position).getText());
		    	  cuTime.setText(convertTime(list.get(position).getTime()));
		      }
		  }else if(type == 2){
			  TextView Lable = (TextView) v.findViewById(R.id.label);
			  Lable.setText(list.get(position).getText());
		  }
		
		  
		  return v;
	  }
	  public String convertTime(String date){
			//String s = "12:18:00";
			
		  String  time = "" ;

		     SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
		     Date d = null;
		        try {
		             d = f1.parse(date);
		             SimpleDateFormat f2 = new SimpleDateFormat("h:mm a");
		             time = f2.format(d).toUpperCase(); // "12:18am"

		    } catch (ParseException e) {

		        // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		    
		    
		    return time;
		}
	  public void CompDate(String Date){
		  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		  Date strDate = null;
		try {
			strDate = sdf.parse(Date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  if (System.currentTimeMillis() > strDate.getTime()) {
		      int catalog_outdated = 1;
		  }
	  }
	} 