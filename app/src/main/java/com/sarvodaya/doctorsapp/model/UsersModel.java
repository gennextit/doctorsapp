package com.sarvodaya.doctorsapp.model;

public class UsersModel {
	private String doctor_name;
	private String doctor_id;
	private String mobile;
	private String status;
	 
	public String getdoctor_name() {
		return doctor_name;
	}

	public void setdoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	
	public String getdoctor_id() {
		return doctor_id;
	}

	public void setdoctor_id(String doctor_id) {
		this.doctor_id = doctor_id;
	}

	
	public String getstatus() {
		return status;
	}

	public void setstatus(String status) {
		this.status = status;
	}
	
	public String getmobile() {
		return mobile;
	}

	public void setmobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
