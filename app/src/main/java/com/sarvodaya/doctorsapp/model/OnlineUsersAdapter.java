package com.sarvodaya.doctorsapp.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.util.Utility;

import java.util.List;

public class OnlineUsersAdapter extends ArrayAdapter<UsersModel> {
		private final List<UsersModel> list;
		TextView userName;
		ImageView online,TextChat,VidChat;
		Utility util;
	  private final Activity context;

	  public OnlineUsersAdapter(Activity context, List<UsersModel> list) {
		    super(context, R.layout.online_users_custom_slot, list);
		    this.context = context;
		    this.list = list;
	  }
	  static class ViewHolder {
		    protected TextView userName;
		    protected ImageView online;
	  }
	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    
	    if(convertView == null){
            LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
                    Activity.LAYOUT_INFLATER_SERVICE);
  
            convertView = lInflater.inflate(R.layout.online_users_custom_slot, null);       
        }

	      userName = (TextView) convertView.findViewById(R.id.tv_online_users_userName);
	      online = (ImageView) convertView.findViewById(R.id.iv_online_users_onlineStatus);
	      TextChat = (ImageView) convertView.findViewById(R.id.iv_online_user_textChat);
	      VidChat = (ImageView) convertView.findViewById(R.id.iv_online_user_vidChat);
	      
	   
//	      TextChat.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				util =new Utility(context);
//				// TODO Auto-generated method stub
//				String name =list.get(position).getName();
//				String cUName=list.get(position).getUName();
//				Intent in = new Intent(context,UserChat.class);
//                in.putExtra("name", name);
//                in.putExtra("cUName", cUName);
//                context.startActivity(in);
//			}
//	      });
//	      VidChat.setOnClickListener(new View.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					
//					
//				}
//			});
	      if(!list.get(position).getdoctor_name().equals("")){
		    	switch(list.get(position).getstatus()){
	    		case "Online":
			    	online.setImageResource(R.drawable.circle_green);
	  	    	  	userName.setText(list.get(position).getdoctor_name());
	    			break;
	    		case "Offline":
			    	online.setImageResource(R.drawable.circle_grey);
	    			userName.setText(list.get(position).getdoctor_name());
	    			break;
		    	}
	      }
		    
//	      if (list.get(position).getstatus().equalsIgnoreCase("Online")) {
//              //view.setBackgroundColor(Color.parseColor("#E1E4A0"));
//	    	  //convertView.setBackgroundResource(R.drawable.custom_slot_online);
//	    	  online.setImageResource(R.drawable.circle_green);
//	    	  userName.setText(list.get(position).getdoctor_name());
//              
//          } else if (list.get(position).getstatus().equalsIgnoreCase("Offline")){
//              //view.setBackgroundColor(Color.parseColor("#e51937"));
//        	  //convertView.setBackgroundResource(R.drawable.custom_slot_offline);
//        	  online.setImageResource(R.drawable.circle_grey);
//        	  userName.setText(list.get(position).getdoctor_name());
//        	  
//          }
	      
	    
	    /*if(list.get(position).getLike().equalsIgnoreCase("")){
	    	text4.setText("Like : "+"0");
	    }else{
	    	
	    }
	    */
	    return convertView;
	  }
	} 