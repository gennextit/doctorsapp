package com.sarvodaya.doctorsapp.model.adapter;

/**
 * Created by Abhijit on 27-Sep-16.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.OPDModel;

import java.util.ArrayList;

public class OPDHistoryAdapter extends ArrayAdapter<OPDModel> {
    private final ArrayList<OPDModel> list;

    private Context context;

    public OPDHistoryAdapter(Context context, int textViewResourceId, ArrayList<OPDModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;

    }

    class ViewHolder {
        TextView Date, DoctorName, TypeofTnx;

        public ViewHolder(View v) {
            Date = (TextView) v.findViewById(R.id.tv_opd_history_DATE);
            DoctorName = (TextView) v.findViewById(R.id.tv_opd_history_DoctorName);
            TypeofTnx = (TextView) v.findViewById(R.id.tv_opd_history_TypeofTnx);

        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_opd_history, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        if (list.get(position).getDate() != null) {
            holder.Date.setText(list.get(position).getDate());
        }
        if (list.get(position).getDoctorName() != null) {
            holder.DoctorName.setText(list.get(position).getDoctorName());
        }
        if (list.get(position).getTypeofTnx() != null) {
            holder.TypeofTnx.setText(list.get(position).getTypeofTnx());
        }


        return v;
    }


}
