package com.sarvodaya.doctorsapp.model.adapter;

/**
 * Created by Abhijit on 27-Sep-16.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.OTBookingModel;

import java.util.ArrayList;

public class OTListAdapter extends ArrayAdapter<OTBookingModel> {
    private final ArrayList<OTBookingModel> list;

    private Context context;

    public OTListAdapter(Context context, int textViewResourceId, ArrayList<OTBookingModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;

    }

    class ViewHolder {
        TextView tvOTList;

        public ViewHolder(View v) {
            tvOTList = (TextView) v.findViewById(R.id.tv_customslot_otlist);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.custom_slot_otlist, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        if (list.get(position).getOTName() != null) {
            holder.tvOTList.setText(list.get(position).getOTName());
        }
        return v;
    }


}
