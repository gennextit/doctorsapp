package com.sarvodaya.doctorsapp.model.adapter;


/**
 * Created by Abhijit on 17-Sep-16.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.DischargeSummaryModel;

import java.util.ArrayList;

public class DischargeSummaryAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private ArrayList<DischargeSummaryModel> groups;
    private int lastExpandedGroupPosition;
    ExpandableListView listView;
    private AlertDialog dialog = null;

    public DischargeSummaryAdapter(Activity context, ArrayList<DischargeSummaryModel> groups, ExpandableListView listView) {
        this.context = context;
        this.groups = groups;
        this.listView = listView;

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<DischargeSummaryModel> chList = groups.get(groupPosition).getChildList();
        return chList.get(childPosition);
    }

    class ChildHolder {
        TextView tvName;

        public ChildHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.child_name);
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        //collapse the old expanded group, if not the same
        //as new group to expand
        if (groupPosition != lastExpandedGroupPosition) {
            listView.collapseGroup(lastExpandedGroupPosition);
        }

        super.onGroupExpanded(groupPosition);
        lastExpandedGroupPosition = groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final DischargeSummaryModel child = (DischargeSummaryModel) getChild(groupPosition, childPosition);
        View v = convertView;
        ChildHolder childHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.child_summary, parent, false);
            childHolder = new ChildHolder(v);
            v.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) v.getTag();
        }
        childHolder.tvName.setText(child.getName().toString());

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<DischargeSummaryModel> chList = groups.get(groupPosition).getChildList();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    class GroupHolder {
        TextView tvName;
        ImageView ivGroup;

        public GroupHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.group_name);
            ivGroup = (ImageView) v.findViewById(R.id.iv_group_icon);

        }
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        DischargeSummaryModel group = (DischargeSummaryModel) getGroup(groupPosition);
        View v = convertView;
        GroupHolder groupHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.group_summary, parent, false);
            groupHolder = new GroupHolder(v);
            v.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) v.getTag();
        }
        if (isExpanded) {
            lastExpandedGroupPosition = groupPosition;
            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_up);
        } else {
            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_down);
        }
        groupHolder.tvName.setText(group.getCategory());
        return v;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
