package com.sarvodaya.doctorsapp.model.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.RecommendModel;

import java.util.ArrayList;

public class RecommendDialogAdapter extends BaseAdapter {

    private ArrayList<RecommendModel> list;
    private Activity context;

    public RecommendDialogAdapter(Activity context, ArrayList<RecommendModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public RecommendModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<RecommendModel> getList() {
        return list;
    }

    public ArrayList<RecommendModel> getCheckedList() {
        ArrayList<RecommendModel> checkList = new ArrayList<>();
        for (RecommendModel model : list) {
            if (model.getChecked()) {
                RecommendModel slot = new RecommendModel();
                slot.setRecId(model.getRecId());
                slot.setRecName(model.getRecName());
                checkList.add(slot);
            }
        }
        return checkList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_recommend_grid, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final RecommendModel model = list.get(position);
        viewHolder.tvRecName.setText(model.getRecName());

        if (model.getChecked()) {
            viewHolder.cbCheck.setChecked(true);
            viewHolder.tvRecName.setBackgroundResource(R.color.dialog_select);

        } else {
            viewHolder.cbCheck.setChecked(false);
            viewHolder.tvRecName.setBackgroundResource(R.color.dialog_unselect);
        }
        viewHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getChecked()) {
                    list.get(position).setChecked(false);
                    viewHolder.tvRecName.setBackgroundResource(R.color.dialog_unselect);
                } else {
                    list.get(position).setChecked(true);
                    viewHolder.tvRecName.setBackgroundResource(R.color.dialog_select);
                }
                notifyDataSetChanged();
            }
        });


        return convertView;
    }


    private static class ViewHolder {
        private final CheckBox cbCheck;
        private TextView tvRecName;

        public ViewHolder(View v) {
            tvRecName = (TextView) v.findViewById(R.id.tv_day);
            cbCheck = (CheckBox) v.findViewById(R.id.checkBox_header);
        }
    }


    private void updateTime(int position, RecommendModel timeModel) {
        list.set(position, timeModel);
        notifyDataSetChanged();
    }
}
