package com.sarvodaya.doctorsapp.model.adapter;

/**
 * Created by Abhijit on 27-Sep-16.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.SelfHelpModel;

import java.util.ArrayList;

public class SelfHelpMenuAdapter extends ArrayAdapter<SelfHelpModel> {
    private final ArrayList<SelfHelpModel> list;

    private Context context;

    public SelfHelpMenuAdapter(Context context, int textViewResourceId, ArrayList<SelfHelpModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;

    }

    class ViewHolder {
        TextView tvMenu;
        ImageView ivMenu;

        public ViewHolder(View v) {
            ivMenu = (ImageView) v.findViewById(R.id.iv_selfhelp_logo);
            tvMenu = (TextView) v.findViewById(R.id.tv_selfhelp_Name);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_selfhelp, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        if (list.get(position).getMenuName() != null) {
            holder.tvMenu.setText(list.get(position).getMenuName());
        }
        holder.ivMenu.setImageResource(list.get(position).getImage());

        return v;
    }


}
