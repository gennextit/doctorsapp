package com.sarvodaya.doctorsapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONCreatorClass {

	public static String toJSon(JSONModel person) {
	      try {
	        // Here we convert Java Object to JSON 
	        JSONObject jsonObj = new JSONObject();
	        jsonObj.put("name", person.getName()); // Set the first name/pair 
	        jsonObj.put("surname", person.getSurName());
	        jsonObj.put("address", person.getAddress());
	        
	        JSONArray jsonArr = new JSONArray();
	        jsonArr.put(jsonObj);

	        
	        return jsonArr.toString();
	
	    }
	    catch(JSONException ex) {
	        ex.printStackTrace();
	    }
	
	    return null;
	
	}
	
	public static String toJSonArray(UserChatModel person) {
	      try {
	    	// In this case we need a json array to hold the java list
	          JSONArray jsonArr = new JSONArray();

	          for (UserChatModel model : person.getChatList() ) {
	              JSONObject pnObj = new JSONObject();
	              pnObj.put("uname", model.getUName());
	              pnObj.put("cuname", model.getCUName());
	              pnObj.put("text", model.getText());
	              pnObj.put("date", model.getDate());
	              pnObj.put("time", model.getTime());
	              pnObj.put("Position", model.getPosition());
	              
	              jsonArr.put(pnObj);
	          }

	         
	          return jsonArr.toString();

	
	    }
	    catch(JSONException ex) {
	        ex.printStackTrace();
	    }
	
	    return null;
	
	}
	
//	public class JsonUtil {
//
//		public static String toJSon(Person person) {
//		      try {
//		        // Here we convert Java Object to JSON 
//		        JSONObject jsonObj = new JSONObject();
//		        jsonObj.put("name", person.getName()); // Set the first name/pair 
//		        jsonObj.put("surname", person.getSurname());
//
//		        JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
//		        jsonAdd.put("address", person.getAddress().getAddress());
//		        jsonAdd.put("city", person.getAddress().getCity());
//		        jsonAdd.put("state", person.getAddress().getState());
//
//		        // We add the object to the main object
//		        jsonObj.put("address", jsonAdd);
//
//		        // and finally we add the phone number
//		        // In this case we need a json array to hold the java list
//		        JSONArray jsonArr = new JSONArray();
//
//		        for (PhoneNumber pn : person.getPhoneList() ) {
//		            JSONObject pnObj = new JSONObject();
//		            pnObj.put("num", pn.getNumber());
//		            pnObj.put("type", pn.getType());
//		            jsonArr.put(pnObj);
//		        }
//
//		        jsonObj.put("phoneNumber", jsonArr);
//
//		        return jsonObj.toString();
//
//		    }
//		    catch(JSONException ex) {
//		        ex.printStackTrace();
//		    }
//
//		    return null;
//
//		}
}