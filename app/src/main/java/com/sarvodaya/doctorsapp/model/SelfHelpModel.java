package com.sarvodaya.doctorsapp.model;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class SelfHelpModel {

    private String menuName;
    private int image;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
