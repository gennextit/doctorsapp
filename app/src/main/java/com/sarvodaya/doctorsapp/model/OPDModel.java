package com.sarvodaya.doctorsapp.model;


import java.util.ArrayList;

public class OPDModel {

	private String output;
	private String outputPaid;
	private String outputUnPaid;
	private String outputOut;
	private String outputError;
	private String AppNo;
	private String PatientName;
	private String AppTime;
	private String ReceiptNo;
	private String Amount;
	private String SeqNo;
	private String AppId;
	private String CallStatus;
	private String PaymentStatus;

	private String Date;
	private String DoctorName;
	private String TypeofTnx;

	public static final String APP_NO = "AppNo";
	public static final String PATIENT_NAME = "PatientName";
	public static final String APP_TIME = "AppTime";
	public static final String RECEIPT_NO = "ReceiptNo";
	public static final String AMOUNT = "Amount";
	public static final String SEQ_NO = "SeqNo";
	public static final String APP_ID = "AppId";
	public static final String CALL_STATUS = "CallStatus";
	public static final String PAYMENT_STATUS = "PaymentStatus";

	public static final String DATE_KEY= "Date";
	public static final String DOCTOR_NAME= "DoctorName";
	public static final String TYPEOFTNX= "TypeofTnx";

	private ArrayList<OPDModel>paidList;
	private ArrayList<OPDModel>unPaidList;
	private ArrayList<OPDModel>outList;
	private ArrayList<OPDModel>List;


	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getTypeofTnx() {
		return TypeofTnx;
	}

	public void setTypeofTnx(String typeofTnx) {
		TypeofTnx = typeofTnx;
	}

	public String getOutputError() {
		return outputError;
	}

	public void setOutputError(String outputError) {
		this.outputError = outputError;
	}

	public String getOutputPaid() {
		return outputPaid;
	}

	public void setOutputPaid(String outputPaid) {
		this.outputPaid = outputPaid;
	}

	public String getOutputUnPaid() {
		return outputUnPaid;
	}

	public void setOutputUnPaid(String outputUnPaid) {
		this.outputUnPaid = outputUnPaid;
	}

	public String getOutputOut() {
		return outputOut;
	}

	public void setOutputOut(String outputOut) {
		this.outputOut = outputOut;
	}

	public ArrayList<OPDModel> getList() {
		return List;
	}

	public void setList(ArrayList<OPDModel> list) {
		List = list;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public ArrayList<OPDModel> getPaidList() {
		return paidList;
	}

	public void setPaidList(ArrayList<OPDModel> paidList) {
		this.paidList = paidList;
	}

	public ArrayList<OPDModel> getUnPaidList() {
		return unPaidList;
	}

	public void setUnPaidList(ArrayList<OPDModel> unPaidList) {
		this.unPaidList = unPaidList;
	}

	public ArrayList<OPDModel> getOutList() {
		return outList;
	}

	public void setOutList(ArrayList<OPDModel> outList) {
		this.outList = outList;
	}

	public static final int PAID=1,UNPAID=2,OUT=3;
	
	public String getAppNo() {
		return AppNo;
	}

	public void setAppNo(String AppNo) {
		this.AppNo = AppNo;
	}
	
	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String PatientName) {
		this.PatientName = PatientName;
	}

	public String getAppTime() {
		return AppTime;
	}

	public void setAppTime(String AppTime) {
		this.AppTime = AppTime;
	}
	
	public String getReceiptNo() {
		return ReceiptNo;
	}

	public void setReceiptNo(String ReceiptNo) {
		this.ReceiptNo = ReceiptNo;
	}
	
	public String getAmount() {
		return Amount;
	}

	public void setAmount(String Amount) {
		this.Amount = Amount;
	}
	
	public String getSeqNo() {
		return SeqNo;
	}

	public void setSeqNo(String SeqNo) {
		this.SeqNo = SeqNo;
	}
	public String getAppId() {
		return AppId;
	}

	public void setAppId(String AppId) {
		this.AppId = AppId;
	}
	
	public String getCallStatus() {
		return CallStatus;
	}

	public void setCallStatus(String CallStatus) {
		this.CallStatus = CallStatus;
	}
	
	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String PaymentStatus) {
		this.PaymentStatus = PaymentStatus;
	}
}
