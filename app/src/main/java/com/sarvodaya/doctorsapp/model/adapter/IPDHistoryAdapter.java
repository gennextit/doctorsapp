package com.sarvodaya.doctorsapp.model.adapter;

/**
 * Created by Abhijit on 27-Sep-16.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.IPDModel;

import java.util.ArrayList;

public class IPDHistoryAdapter extends ArrayAdapter<IPDModel> {
    private final ArrayList<IPDModel> list;

    private Context context;

    public IPDHistoryAdapter(Context context, int textViewResourceId, ArrayList<IPDModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }

    class ViewHolder {
        TextView TestName, PrescribeDate, SampleDate, ResultDate;
        LinearLayout llLabReport, llPrescribeDate, llSampleDate, llResultDate;

        public ViewHolder(View v) {
            TestName = (TextView) v.findViewById(R.id.tv_opd_history_TestName);
            PrescribeDate = (TextView) v.findViewById(R.id.tv_opd_history_PrescribeDate);
            SampleDate = (TextView) v.findViewById(R.id.tv_opd_history_SampleDate);
            ResultDate = (TextView) v.findViewById(R.id.tv_opd_history_ResultDate);
            llLabReport = (LinearLayout) v.findViewById(R.id.ll_ipd_history_lab_report);
            llPrescribeDate = (LinearLayout) v.findViewById(R.id.ll_ipd_history_lab_prescribe_date);
            llSampleDate = (LinearLayout) v.findViewById(R.id.ll_ipd_history_lab_sample_date);
            llResultDate = (LinearLayout) v.findViewById(R.id.ll_ipd_history_lab_result_date);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_ipd_history, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        if (list.get(position).getTestName() != null) {
            holder.TestName.setText(list.get(position).getTestName());
        }
        String prDate = list.get(position).getPrescribeDate();
        if (prDate != null && !prDate.equals("")) {
            holder.PrescribeDate.setText(prDate);
            holder.llPrescribeDate.setVisibility(View.VISIBLE);
        }else{
            holder.llPrescribeDate.setVisibility(View.GONE);
        }
        String samDate=list.get(position).getSampleDate();
        if ( samDate!= null && !samDate.equals("")) {
            holder.SampleDate.setText(list.get(position).getSampleDate());
            holder.llSampleDate.setVisibility(View.VISIBLE);
        }else {
            holder.llSampleDate.setVisibility(View.GONE);
        }
        String resDate=list.get(position).getResultDate();
        if (resDate != null && !resDate.equals("")) {
            holder.ResultDate.setText(list.get(position).getResultDate());
            holder.llResultDate.setVisibility(View.VISIBLE);
        }else {
            holder.llResultDate.setVisibility(View.GONE);
        }
        String url = list.get(position).getUrl();
        if (url != null && !url.equals("")) {
            holder.llLabReport.setVisibility(View.VISIBLE);
        } else {
            holder.llLabReport.setVisibility(View.GONE);
        }
        return v;
    }


}
