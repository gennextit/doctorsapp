package com.sarvodaya.doctorsapp.model;


import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 23-Feb-17.
 */
public class RecommendModel {

    private String recId;
    private String recName;
    private Boolean checked;

    private ArrayList<RecommendModel>list;

    public void setFields(String recId, String recName, Boolean checked) {
        this.recId = recId;
        this.recName = recName;
        this.checked = checked;
    }

    public ArrayList<RecommendModel> getList() {
        return list;
    }

    public void setList(ArrayList<RecommendModel> list) {
        this.list = list;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getRecName() {
        return recName;
    }

    public void setRecName(String recName) {
        this.recName = recName;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
