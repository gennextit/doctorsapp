package com.sarvodaya.doctorsapp.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;

/**
 * Created by Abhijit-PC on 20-Mar-17.
 */



public class ChangeDoctorDetail extends DialogFragment {
    private Dialog dialog;
    private ChangeDoctorListener mListener;
    private String doctorId,doctorName;


    public interface ChangeDoctorListener {
        void onChangeDoctorClick(ChangeDoctorDetail modeOfPaymentDialog, String newDoctorId, String newDoctorName);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ChangeDoctorDetail newInstance(ChangeDoctorListener listener, String doctorId, String doctorName) {
        ChangeDoctorDetail fragment = new ChangeDoctorDetail();
        fragment.mListener = listener;
        fragment.doctorId = doctorId;
        fragment.doctorName = doctorName;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_change_doctor, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);

        final EditText etDoctorId = (EditText) v.findViewById(R.id.et_doctor_id);
        final EditText etDoctorName = (EditText) v.findViewById(R.id.et_doctor_name);

        etDoctorId.setText(doctorId);
        etDoctorName.setText(doctorName);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onChangeDoctorClick(ChangeDoctorDetail.this,etDoctorId.getText().toString(),etDoctorName.getText().toString());
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }


}

