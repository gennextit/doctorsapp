package com.sarvodaya.doctorsapp.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;

/**
 * Created by Abhijit-PC on 20-Mar-17.
 */



public class EditSummaryDialog extends DialogFragment {
    private Dialog dialog;
    private EditSummListener mListener;
    private String summaryTitle,summaryDescription;
    private int groupPos,choldPos;


    public interface EditSummListener {
        void onSubmitClick(EditSummaryDialog modeOfPaymentDialog,int groupPos,int choldPos,String summaryDescription);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static EditSummaryDialog newInstance(EditSummListener listener,int groupPos,int choldPos,String summaryTitle,String summaryDescription) {
        EditSummaryDialog fragment = new EditSummaryDialog();
        fragment.mListener = listener;
        fragment.groupPos = groupPos;
        fragment.choldPos = choldPos;
        fragment.summaryTitle = summaryTitle;
        fragment.summaryDescription = summaryDescription;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_edit_summary, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);

        final EditText etEdit = (EditText) v.findViewById(R.id.et_edit_summary);

        tvTitle.setText(summaryTitle);
        etEdit.setText(summaryDescription);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onSubmitClick(EditSummaryDialog.this,groupPos,choldPos,etEdit.getText().toString());
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }


}

