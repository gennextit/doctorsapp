package com.sarvodaya.doctorsapp.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.RecommendModel;
import com.sarvodaya.doctorsapp.model.adapter.RecommendDialogAdapter;
import com.sarvodaya.doctorsapp.util.JsonParser;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 20-Mar-17.
 */



public class RecommendDialog extends DialogFragment {
    private Dialog dialog;
    private RecommendListener mListener;
    private RecommendDialogAdapter adapter;
    private ArrayList<RecommendModel> originalList;
    private ArrayList<RecommendModel> tList;


    public interface RecommendListener {
        void onSubmitClick(RecommendDialog modeOfPaymentDialog, ArrayList<RecommendModel> checkedList, ArrayList<RecommendModel> originalList);
        void onCancelClick(DialogFragment dialog);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static RecommendDialog newInstance(RecommendListener listener, ArrayList<RecommendModel> originalList) {
        RecommendDialog fragment = new RecommendDialog();
        fragment.mListener = listener;
        fragment.originalList = originalList;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_recommend, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);

        final CheckBox cbHeader = (CheckBox) v.findViewById(R.id.checkBox_header);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);

        tvTitle.setText("Recommend");

        cbHeader.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (cbHeader.isChecked()) {
                    for (RecommendModel model : tList) {
                        model.setChecked(true);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    for (RecommendModel model : tList) {
                        model.setChecked(false);
                    }
                    adapter.notifyDataSetChanged();
                }

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    RecommendModel model = adapter.getItem(pos);
                    if (model != null)
                        if (model.getChecked()) {
                            model.setChecked(false);
                            tList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        } else {
                            model.setChecked(true);
                            tList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        }
                }
            }
        });

        if (originalList == null) {
            ArrayList<RecommendModel> listM = getAllSelectedDayAndBusinessList();
            if (listM != null) {
                tList = listM;
                adapter = new RecommendDialogAdapter(getActivity(), tList);
                lvMain.setAdapter(adapter);
            }
        } else {
            tList = originalList;
            adapter = new RecommendDialogAdapter(getActivity(), tList);
            lvMain.setAdapter(adapter);
        }

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (adapter != null) {
                    if (mListener != null) {
                        mListener.onSubmitClick(RecommendDialog.this, adapter.getCheckedList(), adapter.getList());
                    }
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onCancelClick(RecommendDialog.this);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private ArrayList<RecommendModel> getAllSelectedDayAndBusinessList() {
        RecommendModel allModel = getAllDayAndBusinessList();
        return allModel.getList();
    }

    private RecommendModel getAllDayAndBusinessList() {
        return JsonParser.parseRecommendJson();
    }

}

