package com.sarvodaya.doctorsapp.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppTokens {


	private static final String APP_VERSION = "1.0.2";
	public static final String FolderDirectory= "DoctorsApp";
	public static final int APP_UNDER_TESTING = 1;
	public static final int APP_UNDER_PRODUCTION = 2;

	public static final String drImage= "drImage";
	public static final String storeOPD= "storeOPD";
	public static final String AppStop= "AppStop";

	
//  /************* Chat Detail **************/
	public static final String ClientDOCTORNAME= "clientdoctorname";
	public static final String ClientDOCTORId= "clientdoctorid";
	public static final String ClientMOBILE = "clientmobile";
	
	public static final String PUBLISH_KEY = "pub-c-7cc15fb1-cf83-4184-bf46-7e6c1b73fb92";
	public static final String SUBSCRIBE_KEY = "sub-c-d2cb8f06-ce5c-11e5-b522-0619f8945a4f";
	public static final String CHANNEL = "DrApp";
    
	
	// Tokens 
	   
	
	public static final String tempChatHistory = "tempChatHistory";
	public static final String currentChatHistory = "currentChatHistory";
	public static final String othersChatHistory = "currentChatHistory";
	public static final String isServicesStart = "isServicesStart";
	public static final String isNotificationActive = "isNotificationActive";
	public static final String onlineusers = "onlineusers";
	
	

	public String LoadClientMobile(Activity act){
	     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(act);
	     String  data = sharedPreferences.getString(AppTokens.ClientMOBILE, "") ;
	     return data;
	 }

//	public String LoadUserMobile(Activity act){
//	     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(act);
//	     String  data = sharedPreferences.getString(AppTokens.ClientMOBILE, "") ;
//	     return data;
//	}
}
