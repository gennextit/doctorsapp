package com.sarvodaya.doctorsapp.util;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;

import com.sarvodaya.doctorsapp.model.DischargeSummaryModel;
import com.sarvodaya.doctorsapp.model.IPDModel;
import com.sarvodaya.doctorsapp.model.OPDModel;
import com.sarvodaya.doctorsapp.model.OTBookingModel;
import com.sarvodaya.doctorsapp.model.RecommendModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.ArrayList;

import javax.xml.transform.Source;

import static com.sarvodaya.doctorsapp.model.IPDModel.*;
import static com.sarvodaya.doctorsapp.model.OPDModel.*;
import static com.sarvodaya.doctorsapp.model.OTBookingModel.*;

/**
 * Created by Rishabh on 7/29/2016.
 */
public class JsonParser {

    Context activity;
    private static final String DOCTOR_ID = "doctor_id";
    private static final String DR_ID = "Doctor_ID";
    private static final String DOCTORNAME = "DoctorName";
    private static final String IMAGEURL = "ImageURL";
    private static final String DRMOBILE = "DoctorMobile";

    public static String ERRORMESSAGE = "Not Available";

    public static final int NOTICES = 1, CIRCULAR = 2;

    public JsonParser(Context activity) {
        this.activity = activity;
    }

    public String[] getDefaultTask(String result) {
        String[] output=new String[2];
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Result").equalsIgnoreCase("")){
                        output[0]="success";
                        output[1]=data.optString("Result");

                    }
                    if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        output[0]="failure";
                        output[1]=data.optString("ErrorMsg");
                    }

                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }


    public String setDoctorProfile(Context context, String result) {
        String output = "not available";
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                JSONObject data = jArray.getJSONObject(0);

                if (!data.optString("doctor_id").equalsIgnoreCase("")) {
                    //	Change//if(util.LoadPreferences(AppTokens.DOCTORID).equalsIgnoreCase(data.optString(DOCTOR_ID))){

                    output = Doctor.setDoctorDetail(context, data.optString("doctor_id"), data.optString("doctor_name"), data.optString("doctor_id_for_IPD"));

//                    if(!LoadPref(AppTokens.DOCTORID).equalsIgnoreCase("")){
//
//                        // Change for testing
//                        util.SavePreferences(AppTokens.DOCTORID,"LSHHI10");
//                        util.SavePreferences(AppTokens.DOCTORNAME,"Dr. Shruti Kohli");
//
//                        //
//
//        			util.SavePreferences(AppTokens.DOCTORID,data.optString(DOCTOR_ID));
//        			util.SavePreferences(AppTokens.DOCTORNAME,data.optString("doctor_name"));
//
////                        Intent ob=new Intent(SplashScreen.this,MainActivity.class);
////                        startActivity(ob);
////                        finish();
//                    }else{
////                        Intent ob=new Intent(SplashScreen.this,LogInActivity.class);
////                        startActivity(ob);
////                        finish();
//                    }


                } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                    output = "failure";
                    ERRORMESSAGE = data.optString("ErrorMsg");
//                    util.showToast(data.optString("ErrorMsg"),false);
//                    Intent ob=new Intent(SplashScreen.this,LogInActivity.class);
//                    startActivity(ob);
//                    finish();
                }


            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public String parseValidateDoctor(Context context, String result) {
        String output = "not available";
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
            try {
                JSONArray jArray = new JSONArray(finalResult);

                JSONObject data = jArray.getJSONObject(0);

                if (!data.optString(DOCTOR_ID).equalsIgnoreCase("")) {
                    String drId=data.optString(DOCTOR_ID);
                    String drName=data.optString("doctor_name");
                    String drIDForIPD=data.optString("doctor_id_for_IPD");
                    Doctor.setDoctorId(context, drId);
                    Doctor.setDoctorName(context, drName);
                    Doctor.setDoctorIPDId(context, drIDForIPD);
                    output = "success";
                } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                    output = "failure";
                    ERRORMESSAGE = data.optString("ErrorMsg");
                }


            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public String parseLoadDoctor(FragmentActivity activity, String result, String drId) {
        String output = "not available";
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
            try {
                JSONArray jArray = new JSONArray(finalResult);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject data = jArray.getJSONObject(i);
                    if (data.optString(DR_ID).equalsIgnoreCase(drId)) {
//                        drImage = data.optString(IMAGEURL);
                        output = "success";
                        Doctor.setDoctorImage(activity, data.optString(IMAGEURL));
                    } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                        output = "failure";
                        ERRORMESSAGE = data.optString("ErrorMsg");
                    }
                }
//                SavePreferences(AppTokens.DOCTORID, tempDrId);
//                Log.e("SavePreferencesUserMOBILE", AppTokens.UserMOBILE + mobile.getText().toString());
//                SavePreferences(AppTokens.UserMOBILE, mobile.getText().toString());

                //SavePreferences(AppTokens.DOCTORNAME,data.optString(DOCTORNAME));
//                Log.e("SavePreferencesDOCTOR_IMAGE", AppTokens.DOCTOR_IMAGE + drImage);
//                SavePreferences(AppTokens.DOCTOR_IMAGE, drImage);
//                SavePreferences(AppTokens.drImage, "");

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }


    public OPDModel getOPDList(String result, String date, int tabFlag) {
        OPDModel model = new OPDModel();
        model.setList(new ArrayList<OPDModel>());
        model.setOutput("not available");
        model.setList(new ArrayList<OPDModel>());
        //ArrayList<OPDModel>opdList=new ArrayList<>();
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject data = jArray.getJSONObject(i);

                    switch (tabFlag) {
                        case PAID:
                            if (data.optString(PAYMENT_STATUS).equalsIgnoreCase("Paid")
                                    && !data.optString(PAYMENT_STATUS).equalsIgnoreCase("")) {
                                if (!data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                                        && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                                    OPDModel slot = new OPDModel();
                                    model.setOutput("success");
                                    slot.setAppNo(data.optString(APP_NO));
                                    slot.setPatientName(data.optString(PATIENT_NAME));
                                    slot.setAppTime("Time" + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                                    slot.setReceiptNo(data.optString(RECEIPT_NO));
                                    slot.setAmount(data.optString(AMOUNT));
                                    slot.setSeqNo(data.optString(SEQ_NO));
                                    slot.setAppId(data.optString(APP_ID));
                                    slot.setCallStatus(data.optString(CALL_STATUS));
                                    slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                                    model.getList().add(slot);//opdList.add(slot);
                                }
                            } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                                //output = "failure";
                                model.setOutput("failure");
                                ERRORMESSAGE = data.optString("ErrorMsg");
                            }

                            break;
                        case UNPAID:
                            // view un Paid list
                            if (data.optString(PAYMENT_STATUS).equalsIgnoreCase("Unpaid")
                                    && !data.optString(PAYMENT_STATUS).equalsIgnoreCase("")) {
                                if (!data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                                        && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                                    model.setOutput("success");
                                    OPDModel slot = new OPDModel();
                                    slot.setAppNo(data.optString(APP_NO));
                                    slot.setPatientName(data.optString(PATIENT_NAME));
                                    slot.setAppTime(" Time " + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                                    slot.setReceiptNo(data.optString(RECEIPT_NO));
                                    slot.setAmount(data.optString(AMOUNT));
                                    slot.setSeqNo(data.optString(SEQ_NO));
                                    slot.setAppId(data.optString(APP_ID));
                                    slot.setCallStatus(data.optString(CALL_STATUS));
                                    slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                                    model.getList().add(slot);
                                }
                            } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                                //output = "failure";
                                model.setOutput("failure");
                                ERRORMESSAGE = data.optString("ErrorMsg");
                            }
                            break;
                        case OUT:
                            if (data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                                    && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                                model.setOutput("success");
                                OPDModel slot = new OPDModel();
                                slot.setAppNo(data.optString(APP_NO));
                                slot.setPatientName(data.optString(PATIENT_NAME));
                                slot.setAppTime(" Time " + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                                slot.setReceiptNo(data.optString(RECEIPT_NO));
                                slot.setAmount(data.optString(AMOUNT));
                                slot.setSeqNo(data.optString(SEQ_NO));
                                slot.setAppId(data.optString(APP_ID));
                                slot.setCallStatus(data.optString(CALL_STATUS));
                                slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                                model.getList().add(slot);
                            } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
//                                output = "failure";
                                model.setOutput("failure");
                                ERRORMESSAGE = data.optString("ErrorMsg");
                            }
                            break;

                    }

                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return model;

    }

    public OPDModel getOPDList2(String result, String date) {
        OPDModel model = new OPDModel();
        model.setList(new ArrayList<OPDModel>());
        model.setOutputPaid("NA");
        model.setOutputUnPaid("NA");
        model.setOutputOut("NA");
        model.setOutputError("NA");

        model.setPaidList(new ArrayList<OPDModel>());
        model.setUnPaidList(new ArrayList<OPDModel>());
        model.setOutList(new ArrayList<OPDModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject data = jArray.getJSONObject(i);

                    if (data.optString(PAYMENT_STATUS).equalsIgnoreCase("Paid")
                            && !data.optString(PAYMENT_STATUS).equalsIgnoreCase("")) {
                        if (!data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                                && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                            OPDModel slot = new OPDModel();
                            model.setOutputPaid("success");
                            slot.setAppNo(data.optString(APP_NO));
                            slot.setPatientName(data.optString(PATIENT_NAME));
                            slot.setAppTime("Time " + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                            slot.setReceiptNo(data.optString(RECEIPT_NO));
                            slot.setAmount(data.optString(AMOUNT));
                            slot.setSeqNo(data.optString(SEQ_NO));
                            slot.setAppId(data.optString(APP_ID));
                            slot.setCallStatus(data.optString(CALL_STATUS));
                            slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                            model.getPaidList().add(slot);//opdList.add(slot);
                        }
                    }else if (data.optString(PAYMENT_STATUS).equalsIgnoreCase("Unpaid")
                            && !data.optString(PAYMENT_STATUS).equalsIgnoreCase("")) {
                        if (!data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                                && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                            model.setOutputUnPaid("success");
                            OPDModel slot = new OPDModel();
                            slot.setAppNo(data.optString(APP_NO));
                            slot.setPatientName(data.optString(PATIENT_NAME));
                            slot.setAppTime(" Time " + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                            slot.setReceiptNo(data.optString(RECEIPT_NO));
                            slot.setAmount(data.optString(AMOUNT));
                            slot.setSeqNo(data.optString(SEQ_NO));
                            slot.setAppId(data.optString(APP_ID));
                            slot.setCallStatus(data.optString(CALL_STATUS));
                            slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                            model.getUnPaidList().add(slot);
                        }
                    }  else  if (data.optString(CALL_STATUS).equalsIgnoreCase("Out")
                            && !data.optString(CALL_STATUS).equalsIgnoreCase("")) {
                        model.setOutputOut("success");
                        OPDModel slot = new OPDModel();
                        slot.setAppNo(data.optString(APP_NO));
                        slot.setPatientName(data.optString(PATIENT_NAME));
                        slot.setAppTime(" Time " + DateTimeUtility.convertTime24to12Hours(data.optString(APP_TIME)));
                        slot.setReceiptNo(data.optString(RECEIPT_NO));
                        slot.setAmount(data.optString(AMOUNT));
                        slot.setSeqNo(data.optString(SEQ_NO));
                        slot.setAppId(data.optString(APP_ID));
                        slot.setCallStatus(data.optString(CALL_STATUS));
                        slot.setPaymentStatus(data.optString(PAYMENT_STATUS));

                        model.getOutList().add(slot);
                    }

                    else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
//                                output = "failure";
                        model.setOutputOut("failure");
                        model.setOutputError(data.optString("ErrorMsg"));
                    }


                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return model;

    }

    public String getOPDSlotStatus(String result) {
        String output="Something wen't wrong. Please retry or try after sometime.";
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject data = jArray.getJSONObject(i);
                    if (!data.optString("Msg").equalsIgnoreCase("")) {
                        output = data.optString("Msg");
                    }
                    if(!data.optString("ErrorCode").equalsIgnoreCase("")){
                        output = data.optString("ErrorMsg");
                    }
                }
            }catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;

    }

    public OPDModel getOPDPatientHistory(String result) {
        OPDModel model=new OPDModel();
        model.setOutput("NA");
        model.setList(new ArrayList<OPDModel>());
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {

                    JSONObject data = jArray.getJSONObject(i);
                    OPDModel slot=new OPDModel();
                    if(!data.optString(DOCTOR_NAME).equals("")){
                        model.setOutput("success");
                        slot.setDate(data.optString(DATE_KEY));
                        slot.setDoctorName(data.optString(DOCTOR_NAME));
                        slot.setTypeofTnx(data.optString(TYPEOFTNX));
                        model.getList().add(slot);
//                        sList.add(slot);
                    }
                    if(!data.optString("ErrorCode").equalsIgnoreCase("")){
                        model.setOutput("failure");
                        model.setOutputError(data.optString("ErrorMsg"));
//                        ErrorMsg=data.optString("ErrorMsg");
                    }
                    //slot.setATime(data.optString(TAG_APPTIME).substring(0, 5));
                    //HashMap<String, String> patient = new HashMap<String, String>();
                    // adding each child node to HashMap key => value

                    // adding contact to contact list


                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return model;
    }

    public IPDModel getIPDDetail(String result) {

        IPDModel ipdModel=new IPDModel();
        ipdModel.setOutput("NA");
        ipdModel.setList(new ArrayList<IPDModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    IPDModel slot=new IPDModel();
                    if(!data.optString(TAG_MRNO).equalsIgnoreCase("")){
                        ipdModel.setOutput("success");
                        slot.setIsSummaryPrapared(data.optString(IS_SUMMARY_PRAPARED));
                        slot.setSummary_Status(data.optString(SUMMARY_STATUS));
                        slot.setMRNo(data.optString(TAG_MRNO));
                        slot.setIPNo(data.optString(TAG_IPNO));
                        slot.setPatientName(data.optString(TAG_PATIENT_NAME));
                        slot.setAdmitDate(data.optString(TAG_ADMIT_DATE));
                        slot.setAdmitTime(data.optString(TAG_ADMIT_TIME));
                        slot.setFloor(data.optString(TAG_FLOOR));
                        slot.setRoom_No(data.optString(TAG_ROOM_NO));
                        slot.setRoom_Name(data.optString(TAG_NAME));
                        slot.setBed_No(data.optString(TAG_BED_NO));
                        ipdModel.getList().add(slot);
                    }
                    if(!data.optString("ErrorCode").equalsIgnoreCase("")){
                        ipdModel.setOutput("failure");
                        ipdModel.setOutputError(data.optString("ErrorMsg"));

                    }
                }


            }catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return ipdModel;
    }

    public IPDModel getIPDPatientDetail(String result) {
        IPDModel ipdModel=new IPDModel();
        ipdModel.setOutput("NA");
        ipdModel.setList(new ArrayList<IPDModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    IPDModel slot=new IPDModel();
                    if(!data.optString(TESTNAME).equals("")){
                        ipdModel.setOutput("success");
                        slot.setPrescribeDate(data.optString(PRESCRIBEDATE));
                        slot.setSampleDate(data.optString(SAMPLEDATE));
                        slot.setResultDate(data.optString(RESULTDATE));
                        slot.setTestName(Html.fromHtml(data.optString(TESTNAME)).toString());
                        slot.setUrl(data.optString(URL_KEY));

                        ipdModel.getList().add(slot);
                    }
                    if(!data.optString("ErrorCode").equals("")){
                        ipdModel.setOutput("failure");
                        ipdModel.setOutputError(data.optString("ErrorMsg"));
                    }
                }

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return ipdModel;
    }

    public OTBookingModel getOTListDetail(String result) {
        OTBookingModel otBookingModel=new OTBookingModel();
        otBookingModel.setOutput("NA");
        otBookingModel.setList(new ArrayList<OTBookingModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    OTBookingModel slot=new OTBookingModel();
                    if(!data.optString(OT_ID).equalsIgnoreCase("")){
                        otBookingModel.setOutput("success");
                        slot.setOTId(data.optString(OT_ID));
                        slot.setOTName(data.optString(OT_NAME));

                        otBookingModel.getList().add(slot);
                    }
                    if(!data.optString("ErrorCode").equalsIgnoreCase("")){
                        otBookingModel.setOutput("failure");
                        otBookingModel.setOutputError(data.optString("ErrorMsg"));
                    }
                }

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return otBookingModel;
    }

    public OTBookingModel getOTBookingRequestDetail(String result) {
        OTBookingModel otBookingModel=new OTBookingModel();
        otBookingModel.setOutput("NA");
        otBookingModel.setList(null);

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Result").equalsIgnoreCase("")){
                        otBookingModel.setOutput("success");
                        otBookingModel.setOutputError(data.optString("Result"));
                    }
                    if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        otBookingModel.setOutput("success");
                        otBookingModel.setOutputError(data.optString("ErrorMsg"));
                    }

                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return otBookingModel;
    }

    public String[] getBookALeaveDetail(String result) {
        String[] output=new String[2];
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Result").equalsIgnoreCase("")){
                        output[0]="success";
                        output[1]=data.optString("Result");
                    }
                    if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        output[0]="failure";
                        output[1]=data.optString("ErrorMsg");
                    }

                }

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public String[] getPostponeReqDetail(String result) {
        String[] output=new String[2];
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Result").equalsIgnoreCase("")){
                        output[0]="success";
                        output[1]=data.optString("Result");

                    }
                    if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        output[0]="failure";
                        output[1]=data.optString("ErrorMsg");
                    }

                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public String[] getDischargeDetail(String result) {
        String[] output=new String[2];
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf("</string>"));

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Url").equalsIgnoreCase("")){
                        output[0]="success";
//                        output[1]=data.optString("IsSummaryPrapared");
                        output[1]=data.optString("Url");
//                        output[3]=data.optString("Summary_Status");
//                        output[4]=data.optString("RejectedBy");
//                        output[5]=data.optString("RejectedDate");
//                        output[6]=data.optString("RejectedRemark");

                    }
                    if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        output[0]="failure";
                        output[1]=data.optString("ErrorMsg");
                    }

                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public String[] getApproveRejectInfo(String result) {
        String[] output=new String[2];
        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("Msg").equalsIgnoreCase("")){
                        output[0]="success";
                        output[1]=data.optString("Msg");
                    }else if(!data.optString("ErrorMsg").equalsIgnoreCase("")){
                        output[0]="failure";
                        output[1]=data.optString("ErrorMsg");
                    }
                }
            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return output;
    }

    public static RecommendModel parseRecommendJson() {
        RecommendModel model = new RecommendModel();
        model.setList(new ArrayList<RecommendModel>());

        model.getList().add(setSlotRecommendModel("1","X-Ray"));
        model.getList().add(setSlotRecommendModel("2","Lab"));
        model.getList().add(setSlotRecommendModel("3","MRI"));
        model.getList().add(setSlotRecommendModel("4","CT scan"));
        model.getList().add(setSlotRecommendModel("5","Blood test"));

        return model;
    }

    private static RecommendModel setSlotRecommendModel(String id,String day) {
        RecommendModel slot = new RecommendModel();
        slot.setFields(id,day,false);
        return slot;
    }

    public static ArrayList<DischargeSummaryModel> parseDischargeSummary() {
        DischargeSummaryModel model;
        ArrayList<DischargeSummaryModel> gList=new ArrayList<>();
        model = new DischargeSummaryModel();
        model.setCategory("Ct Whole Abdomen Contrast");
        model.setChildList(setChildList("Abnormally distended GB with hyperdense sludge in GB lumen.\n" +
                "Mild fibrofatty mural thickening in caecum & right sided ascending\n" +
                "colon."));
        gList.add(model);

        model = new DischargeSummaryModel();
        model.setCategory("Ultrasound Whole Abdomen");
        model.setChildList(setChildList("Grade II fatty changes in liver.\n" +
                "Cholelithiasis."));
        gList.add(model);

        model = new DischargeSummaryModel();
        model.setCategory("X RAY CHEST PA");
        model.setChildList(setChildList("Abnormally distended GB with hyperdense sludge in GB lumen. " +
                "Mild fibrofatty mural thickening in caecum & right sided ascending colon."));
        gList.add(model);

        return gList;
    }

    private static ArrayList<DischargeSummaryModel> setChildList(String description) {
        ArrayList<DischargeSummaryModel> list=new ArrayList<>();
        DischargeSummaryModel child = new DischargeSummaryModel();
        child.setName(description);
        list.add(child);
        return list;
    }

    public DischargeSummaryModel GETIPDDischargeDetails(String result) {
        DischargeSummaryModel ipdModel=new DischargeSummaryModel();
        ipdModel.setOutput("NA");
        ipdModel.setGroupList(new ArrayList<DischargeSummaryModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf("</string>"));

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("ErrorCode").equals("")){
                        ipdModel.setOutput("failure");
                        ipdModel.setOutputMsg(data.optString("ErrorMsg"));
                    }else{
                        ipdModel.setOutput("success");
                        ipdModel.getGroupList().add(setDischargeEntity(data,"Diagnosis"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"PhysicalExam"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"GeneralExam"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"TreatmentGiven"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"CourseinHospital"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"ConditionAtTheTimeOfDischarge"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"Diet"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"FollowUp"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"PresentingSymptoms"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"InCaseOF"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"OnFollowingTreatment"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"CauseofDeath"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"Optional"));
                        ipdModel.getGroupList().add(setDischargeEntity(data,"Header"));
                    }
                }

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return ipdModel;
    }

    private DischargeSummaryModel setDischargeEntity(JSONObject data, String category) {
        DischargeSummaryModel model = new DischargeSummaryModel();
        model.setCategory(category);
        model.setChildList(setChildList(stripHtml(data.optString(category))));
        return model;
    }

    private String stripHtml(String html) {
       return Html.fromHtml(Html.fromHtml(html).toString()).toString();
    }

    public DischargeSummaryModel updateDischargeSummary(String result) {
        DischargeSummaryModel ipdModel=new DischargeSummaryModel();
        ipdModel.setOutput("NA");
        ipdModel.setGroupList(new ArrayList<DischargeSummaryModel>());

        if (result.contains("[")) {
            String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

            try {
                JSONArray jArray = new JSONArray(finalResult);
                for(int i=0;i<jArray.length();i++)
                {
                    JSONObject data = jArray.getJSONObject(i);
                    if(!data.optString("ErrorCode").equals("")){
                        ipdModel.setOutput("failure");
                        ipdModel.setOutputMsg(data.optString("ErrorMsg"));
                    }else if(!data.optString("Msg").equals("")){
                        ipdModel.setOutput("success");
                        ipdModel.setOutputMsg(data.optString("Msg"));
                    }
                }

            } catch (JSONException e) {
                L.m(e.toString());
                ERRORMESSAGE = e.toString() + result;
                return null;
            }
        } else {
            L.m(result);
            ERRORMESSAGE = result;
            return null;
        }
        return ipdModel;
    }
}
