package com.sarvodaya.doctorsapp.util;

import java.io.IOException;


import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.util.AsyncRequest.OnAsyncRequestComplete;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BGServices extends Service {

@Override
public IBinder onBind(Intent intent) {
    // TODO Auto-generated method stub
    return null;
}

@Override
public int onStartCommand(Intent intent, int flags, int startId) {
    
	new Handler().postDelayed(new Runnable() {
        public void run() {
        	new AsyncRequest().execute(AppSettings.ALL_USERS);
        }
    }, 10000);
	
    return START_STICKY;  
}

public class AsyncRequest extends AsyncTask<String, Void, String> {
	private ProgressDialog pDialog;
	Context context;
	OnAsyncRequestComplete caller;
	String gravity="CENTER";
	String Msg="Processing, Keep patience...";
	boolean isTaskCompleted = false;
	Handler handler;
	private static final int INTERNET_TIME_OUT = 20000;
	int toastFlag=0,recordFlag=0;
	boolean isCancel=false;
	int finishFlag=0;
	int Task=0;
	String method="null",key,value;
	Utility util;
	 
	 
	 
	@Override
    protected void onPreExecute() {
	   pDialog = new ProgressDialog(context);
       pDialog.setMessage(Msg);
       pDialog.setIndeterminate(false);
       pDialog.setCancelable(false);
       pDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
       if(gravity.equalsIgnoreCase("BOTTOM")){
    	   pDialog.getWindow().setGravity(Gravity.BOTTOM);
       }
       pDialog.show();
       
       toastFlag=0;
       finishDialog();
    }
	private void finishDialog() {
		 handler=  new Handler();
		 handler.postDelayed( new Runnable() {
 				@Override
 				public void run() {
 					isTaskCompleted=true;
 					if(toastFlag==0){
 						pDialog.dismiss();
	 					finishFlag=1;
	 					cancel(true);
	 					//caller.asyncResponse("error");
	 					
 					}
 				}
 			}, INTERNET_TIME_OUT);
     }
	@Override
	protected String doInBackground(String... urls) {
		Log.e("AsyncBg", "execute");
		util =new Utility(context);
		if(!method.equalsIgnoreCase("null")&&!util.LoadPreferences(key).equalsIgnoreCase("")){
			return util.LoadPreferences(key);
		}
		String json = null;
//		try {
//			json = GETURL(urls[0]);
//			return json;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		json=ApiCall.GET(urls[0]);
		if(!method.equalsIgnoreCase("null")){
			util.SavePreferences(key,json);
		}
		return null;
	}

	// onPostExecute displays the results of the AsyncTask.
	@Override
	protected void onPostExecute(String result) {
		Log.e("BGServices_postExecute", "execute");
		
		if (pDialog != null && pDialog.isShowing()) {
			   pDialog.dismiss();
		}
		if(!isTaskCompleted){
			handler.removeCallbacksAndMessages(null);
		}
		toastFlag=1;
		
		util.SavePreferences("ALLUSERS", result);
		
		//finish();
	}
	@Override
    protected void onCancelled(){
		Log.e("AsyncCancel", "execute");
		
		showToast(context.getResources().getString(R.string.server_time_out),false);
	}
	public void showToast(String txt,Boolean status){
		// Inflate the Layout
		LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
	            Activity.LAYOUT_INFLATER_SERVICE);
	    //LayoutInflater inflater = context.getLayoutInflater();
		
	    //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.custom_toast_layout_id));
		View layout=lInflater.inflate(R.layout.custom_toast, null);  
		TextView a=(TextView)layout.findViewById(R.id.tv_custom_toast_ViewMessage);
		ImageView b=(ImageView)layout.findViewById(R.id.iv_custom_toast);
		layout.setBackgroundResource((status) ? R.drawable.bg_toast : R.drawable.bg_toast_red);
		b.setImageResource((status) ? R.mipmap.ic_success : R.drawable.fail);
	    a.setText(txt);
	    a.setTextColor((status) ? context.getResources().getColor(R.color.icon_green) : context.getResources().getColor(R.color.icon_red));
		// Create Custom Toast
	    Toast toast = new Toast(context);
	    toast.setGravity(Gravity.CENTER, 0, 0);
	    toast.setDuration(Toast.LENGTH_LONG);
	    toast.setView(layout);
	    toast.show();
  }
}

}