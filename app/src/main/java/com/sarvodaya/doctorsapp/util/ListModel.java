package com.sarvodaya.doctorsapp.util;

/**
 * Created by gennext on 11/23/2015.
 */
public class ListModel {

    private  String AppId="";
    private  String AppName="";
    private  String AppTime="";
    private  String AppDate="";

    /*********** Set Methods ******************/

    public void setAppId(String AppId)
    {
        this.AppId = AppId;
    }

    public void setAppName(String AppName)
    {
        this.AppName = AppName;
    }

    public void setAppTime(String AppTime)
    {
        this.AppTime = AppTime;
    }

    public void setAppDate(String AppDate)
    {
        this.AppDate = AppDate;
    }

    /*********** Get Methods ****************/


    public String getAppId()
    {
        return this.AppId;
    }

    public String getAppName()
    {
        return this.AppName;
    }

    public String getAppTime()
    {
        return this.AppTime;
    }

    public String getAppDate()
    {
        return this.AppDate;
    }
}
