package com.sarvodaya.doctorsapp.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.R;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utility {
	protected Context context;
	boolean conn=false;
	public Utility(Context context){
        this.context = context.getApplicationContext();
        
    }
	//Change String color
	public void strColor(String text1,String text2){
		SpannableStringBuilder builder = new SpannableStringBuilder();

		SpannableString redSpannable= new SpannableString(text1);
		redSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, text1.length(), 0);
		builder.append(redSpannable);

		SpannableString whiteSpannable= new SpannableString(text2);
		whiteSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, text2.length(), 0);
		builder.append(whiteSpannable);

	}

	public static String urlEn(String res){
		String UrlEncoding=null;
		try {
			UrlEncoding = URLEncoder.encode(res,"UTF-8");

		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return UrlEncoding;
	}
	
	public String convert(String res){
		String UrlEncoding=null;
		try {
			UrlEncoding = URLEncoder.encode(res,"UTF-8");
			
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return UrlEncoding;
	}
	
	public boolean isConnectionAvailable(){
		  
		  if (isOnline()==false){
			  	return false;
		  }else{
				return true;
		  }
	  }
	
	public boolean isOnline() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;
	    
	    ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    if(haveConnectedWifi==true || haveConnectedMobile==true){
	    	Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
	    	Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
	    	conn=true;
	    }else{
	    	Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
	    	Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
	    	conn=false;
	    }
	    
	    return conn;
	}
	public String getDate() {
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    String formattedDate = df.format(c.getTime());
	    return formattedDate;
	}
	public String getTime12(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("h:mm a");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df.format(c.getTime());
		
	}
	public String getTime24(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
	    df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df1.format(c.getTime());
	}
	
	public String getTime(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df.format(c.getTime());
	}

	public String viewDate() {
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	    String formattedDate = df.format(c.getTime());
	    return formattedDate;
	}
	
	public String vDate() {
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    String formattedDate = df.format(c.getTime());
	    return formattedDate;
	}
	
	/***** Pic  time *****/
	public String viewTime(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("h:mm a");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df.format(c.getTime());
		
	}
	public static void showToast(Activity context,String txt,Boolean status){
		// Inflate the Layout
		LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
	            Activity.LAYOUT_INFLATER_SERVICE);
	    //LayoutInflater inflater = context.getLayoutInflater();
		
	    //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.custom_toast_layout_id));
		View layout=lInflater.inflate(R.layout.custom_toast, null);  
		TextView a=(TextView)layout.findViewById(R.id.tv_custom_toast_ViewMessage);
		ImageView b=(ImageView)layout.findViewById(R.id.iv_custom_toast);
		layout.setBackgroundResource((status) ? R.drawable.bg_toast : R.drawable.bg_toast_red);
		b.setImageResource((status) ? R.mipmap.ic_success : R.drawable.fail);
	    a.setText(txt);
	    a.setTextColor((status) ? context.getResources().getColor(R.color.icon_green) : context.getResources().getColor(R.color.icon_red));
		// Create Custom Toast
	    Toast toast = new Toast(context);
	    toast.setGravity(Gravity.CENTER, 0, 0);
	    toast.setDuration(Toast.LENGTH_LONG);
	    toast.setView(layout);
	    toast.show();
  }
	public String getMACAddress() {
		// TODO Auto-generated method stub
		String address;
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

		if(wifiManager.isWifiEnabled()) {
		    // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    //Toast.makeText(getBaseContext(),address, Toast.LENGTH_SHORT).show();
	        
		} else {
		    // ENABLE THE WIFI FIRST
		    wifiManager.setWifiEnabled(true);

		    // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    //Toast.makeText(getBaseContext(),address, Toast.LENGTH_SHORT).show();
		    
		    // DISABLE THE WIFI
		    wifiManager.setWifiEnabled(false);

		}
		return address;
	}
	public void showAlertDialog(String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		if(status != null)
			// Setting alert dialog icon
			alertDialog.setIcon((status) ? R.mipmap.ic_success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
	// Showing Alert Message
	alertDialog.show();
	}
	
	
	
	 public String LoadPreferences(String key){   
	     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	     String  data = sharedPreferences.getString(key, "") ;
	     return data;
	 }
	 public void SavePreferences(String key, String value){
	        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

	        SharedPreferences.Editor editor = sharedPreferences.edit();
	        editor.putString(key, value);
	        editor.commit();
	        
	 }
	// Add Minute in given time
	public String HalfAnHour(String getTime,int Minutes){
		// key 
		String startTime = getTime;
		int minutes = Minutes;

		String[] hm = startTime.split(":");
		int h = Integer.parseInt(hm[0]);
		int m = Integer.parseInt(hm[1]);

		int t = h * 60 + m;      // total minutes
		t += minutes;            // add the desired offset

		while (t < 0) {          // fix `t` so that it's never negative
		  t += 1440;             // 1440 minutes in a day
		}

		int nh = (t / 60) % 24;  // calculate new hours
		int nm = t % 60;         // calculate new minutes

		String newTime = String.format("%02d:%02d", nh, nm); 
		return newTime;
	}
	// enter format eg. dd-MM-yyyy
	public String convertDate(String Date,String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date cDate = null;
		try {
			cDate = sdf.parse(Date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String formattedDate = sdf.format(cDate);
	    return formattedDate;
	}
	
	public int[] getDateTime(){
    	final Calendar cal = Calendar.getInstance();
    	int[] DateTime = null;
    	DateTime[0]= cal.get(Calendar.YEAR);
    	DateTime[1]= cal.get(Calendar.MONTH);
    	DateTime[2]= cal.get(Calendar.DAY_OF_MONTH);
        
    	DateTime[3]= cal.get(Calendar.HOUR);
    	DateTime[4]= cal.get(Calendar.MINUTE);
    	return DateTime;
    }
    public boolean CompDate(String Date1,String Date2){
  	  SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  	  Date strDate = null;
  	  Date endDate = null;
  	  
  	try {
  		strDate = sdf.parse(Date1);
  		endDate = sdf.parse(Date2);
  	} catch (ParseException e) {
  		e.printStackTrace();
  	}
  	  if (strDate.getDate()==endDate.getDate()) {
  		return true;
  	  }
  	 return false;
  }
    
    public boolean CompTime(String Time1,String Time2){
	  	  SimpleDateFormat sdf = new SimpleDateFormat("HH:MM");
	  	  Date strTime = null;
	  	  Date endTime = null;
	  	  
	  	try {
	  		strTime = sdf.parse(Time1);
	  		endTime = sdf.parse(Time2);
	  	} catch (ParseException e) {
	  		e.printStackTrace();
	  	}
	  	  if (strTime.getTime()<endTime.getTime()) {
	  		 return true;
	  	  }
	  	 return false;
	  }
    public String convertTime(String time){
		//String s = "12:18:00";
		
	  String  finaltime = "" ;

	     SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
	     Date d = null;
	        try {
	             d = f1.parse(time);
	             SimpleDateFormat f2 = new SimpleDateFormat("h:mm a");
	             finaltime = f2.format(d).toUpperCase(); // "12:18am"

	    } catch (ParseException e) {

	        // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	    
	    
	    return finaltime;
	}
    
	    
		
		 
		
		
public int convertTimeInMinute(String time){
	// key 
	int str=0;
	//String Hora = "12:34";
	int hour = Integer.parseInt(time.substring(0, 2));
	int minute = Integer.parseInt(time.substring(3, 5));
	
	str=(hour*60)+minute;
	
	return str;
}
public int trimTime(String time){
	// key 
	int str=0;
	//String Hora = "12:34";
	int hour = Integer.parseInt(time.substring(0, 2));
	int minute = Integer.parseInt(time.substring(3, 5));
	
	str=(hour*60)+minute;
	
	return str;
}
    
	    
/*public String getDate(){
	
	// using Calendar class
	Calendar ci = Calendar.getInstance();
	 
	String Date =ci.get(Calendar.DAY_OF_MONTH) + "-" + 
	    (ci.get(Calendar.MONTH) + 1) + "-" +
	    ci.get(Calendar.YEAR);
	return Date;
}*/

	public static File getExternalDirectory(String fileName) {

//		// External sdcard location
//		File storageFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),);
//
//		// Create the storage directory if it does not exist
//		if (!storageFolder.exists()) {
//			if (!storageFolder.mkdirs()) {
//				Log.d(AppTokens.DOWNLOAD_DIRECTORY_NAME,
//						"Oops! Failed create " + AppTokens.DOWNLOAD_DIRECTORY_NAME + " directory");
//				return null;
//			}
//		}
		File pdfFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
//		try{
//			pdfFile.createNewFile();
//		}catch (IOException e){
//			e.printStackTrace();
//		}
		return pdfFile;
	}

	public static String getExternalDownloadDirectory() {

//		// External sdcard location
//		File storageFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),);
//
//		// Create the storage directory if it does not exist
//		if (!storageFolder.exists()) {
//			if (!storageFolder.mkdirs()) {
//				Log.d(AppTokens.DOWNLOAD_DIRECTORY_NAME,
//						"Oops! Failed create " + AppTokens.DOWNLOAD_DIRECTORY_NAME + " directory");
//				return null;
//			}
//		}
//		File pdfFile = new File();
//		try{
//			pdfFile.createNewFile();
//		}catch (IOException e){
//			e.printStackTrace();
//		}
		return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
	}

	public static String getNameWithTimeStamp(String fileName) {
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

		return fileName  +"_DR_APP_" + timeStamp + ".pdf";
	}
}