package com.sarvodaya.doctorsapp.util;

import android.location.Location;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {
    public static RequestBody Default(String consultantId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .build();
        return formBody;
    }
    public static RequestBody LoginBody(String username, String password) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("userId", username)
                .addEncoded("password", password)
                .build();
        return formBody;
    }

    public static RequestBody FeedbackDetail(String consultantId, String feedback) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("from", consultantId)
                .addEncoded("feedback", feedback)
                .build();
        return formBody;
    }

    public static RequestBody UpdateDomain(String userId, String domainId, String domainName) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("domainId", domainId)
                .addEncoded("domainName", domainName)
                .build();
        return formBody;
    }

    public static RequestBody DeleteTimeSlot(String consultantId, String slotId, String date, String sTime
            , String eTime) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("date", date)
                .addEncoded("startTime", sTime)
                .addEncoded("endTime", eTime)
                .build();
        return formBody;
    }

    public static RequestBody EditTimeSlot(String consultantId, String slotId, String date, String sTime,
                                           String eTime, String daya) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("id", slotId)
                .addEncoded("startTime", sTime)
                .addEncoded("endTime", eTime)
                .build();
        return formBody;
    }
    // consultantId,statusId,date,time
    public static RequestBody UpdateStatus(String consultantId, String date, String time, String statusId
            , String remarks) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("statusId", statusId)
                .addEncoded("date", date)
                .addEncoded("time", time)
                .addEncoded("remark", remarks)
                .build();
        return formBody;
    }

    // consultantId,date,time
    public static RequestBody SlotCancel(String consultantId, String date, String time) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("date", date)
                .addEncoded("time", time)
                .build();
        return formBody;
    }

    public static RequestBody SlotReportToAdmin(String consultantId, String userId, String slotId) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("userId", userId)
                .addEncoded("slotId", slotId)
                .build();
        return formBody;
    }

    //consultantId,startDate,endDate,startTime,endTime,days
     public static RequestBody AddTimeSlot(String consultantId, String startDate, String endDate, String startTime
            , String endTime, String sltDaysIds) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("startDate", startDate)
                .addEncoded("endDate", endDate)
                .addEncoded("startTime", startTime)
                .addEncoded("endTime", endTime)
                .addEncoded("days", sltDaysIds)
                .build();
        return formBody;
    }

    public static RequestBody ConsultantFeedback(String consultantId, String date, String time, String feedback) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("date", date)
                .addEncoded("time", time)
                .addEncoded("feedback", feedback)
                .build();
        return formBody;
    }

    public static RequestBody UpdateCRecord(String consultantId, String callJson) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("callJson", callJson)
                .build();
        return formBody;
    }

    public static RequestBody verifyOtpMobile(String mobile, String otp) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("mobile", mobile)
                .addEncoded("otp", otp)
                .build();
        return formBody;
    }

    public static RequestBody verifyMobile(String mobile) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("mobile", mobile)
                .build();
        return formBody;
    }

    public static RequestBody verifyEmail(String email) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("email", email)
                .build();
        return formBody;
    }

    public static RequestBody verifyOtpEmail(String email, String otp) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("email", email)
                .addEncoded("otp", otp)
                .build();
        return formBody;
    }

    public static RequestBody createPassThroughMobile(String password, String input) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("mobile", input)
                .addEncoded("password", password)
                .build();
        return formBody;
    }

    public static RequestBody createPassThroughEmail(String password, String input) {
        RequestBody formBody =  new FormBody.Builder()
                .addEncoded("email", input)
                .addEncoded("password", password)
                .build();
        return formBody;
    }

    public static RequestBody ErrorReport(String report) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
        return formBody;
    }

    public static RequestBody UserLogin(String name, String mobile, String address) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("name", name)
                .addEncoded("mobile", mobile)
                .addEncoded("address", address)
                .build();
        return formBody;
    }

    public static RequestBody getShopInfo(Location location, String defaultRange) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("latLng", String.valueOf(location.getLatitude())+","+ String.valueOf(location.getLongitude()))
                .addEncoded("range", defaultRange)
                .build();
        return formBody;
    }

    public static RequestBody GETDD(String drId, String ipNo) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("Doctor_id", drId)
                .addEncoded("IPNO", ipNo)
                .build();
        return formBody;
    }

    public static RequestBody UpdateDD(String drId, String ipNo, String macAddress, String[] temp) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("Doctor_ID", drId)
                .addEncoded("IPNo", ipNo)
                .addEncoded("MacAddress", macAddress)
                .addEncoded("Diagnosis", temp[0])
                .addEncoded("PhysicalExam", temp[1])
                .addEncoded("GeneralExam", temp[2])
                .addEncoded("TreatmentGiven", temp[3])
                .addEncoded("CourseinHospital", temp[4])
                .addEncoded("ConditionAtTheTimeOfDischarge", temp[5])
                .addEncoded("Diet", temp[6])
                .addEncoded("FollowUp", temp[7])
                .addEncoded("PresentingSymptoms", temp[8])
                .addEncoded("InCaseOF", temp[9])
                .addEncoded("OnFollowingTreatment", temp[10])
                .addEncoded("CauseofDeath", temp[11])
                .addEncoded("Optional", temp[12])
                .addEncoded("Header", temp[13])
                .build();
        return formBody;
    }

    public static RequestBody setDoctorId(String drId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("Doctor_id", drId)
                .build();
        return formBody;
    }

    public static RequestBody setIpNo(String ipNo) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("IPNO", ipNo)
                .build();
        return formBody;
    }

    public static RequestBody setMobileAndMack(String mobile, String macAddress) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("mobile", mobile)
                .addEncoded("mac_addr", macAddress)
                .build();
        return formBody;
    }
    public static String setMobilenMack(String mobile, String macAddress) {
        return "?mobile="+mobile+"&mac_addr="+macAddress;
    }
    public static RequestBody setDrDate(String drId, String date) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("DoctorId", drId)
                .addEncoded("Date", date)
                .build();
        return formBody;
    }

    public static RequestBody setAppId(String appId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("App_ID", appId)
                .build();
        return formBody;
    }

    public static RequestBody setDate(String Date) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("Date", Date)
                .build();
        return formBody;
    }

    public static RequestBody setOtBooking(String OTId, String DoctorId, String FromDate, String ToDate
            , String FromTime, String ToTime, String Reason) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("OTId", OTId)
                .addEncoded("DoctorId", DoctorId)
                .addEncoded("FromDate", FromDate)
                .addEncoded("ToDate", ToDate)
                .addEncoded("FromTime", FromTime)
                .addEncoded("ToTime", ToTime)
                .addEncoded("Reason", Reason)
                .build();
        return formBody;
    }

    public static RequestBody setReqLeave(String DoctorId, String FromDate, String ToDate
            , String FromTime, String ToTime, String Reason) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("DoctorId", DoctorId)
                .addEncoded("FromDate", FromDate)
                .addEncoded("ToDate", ToDate)
                .addEncoded("FromTime", FromTime)
                .addEncoded("ToTime", ToTime)
                .addEncoded("Reason", Reason)
                .build();
        return formBody;
    }

    public static RequestBody postponeAppointment(String DoctorId, String option) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("DoctorId", DoctorId)
                .addEncoded("option", option)
                .build();
        return formBody;
    }


    public static RequestBody dischargeDetail(String DoctorId, String IPNo, String Type, String Remark) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("DoctorId", DoctorId)
                .addEncoded("IPNo", IPNo)
                .addEncoded("Type", Type)
                .addEncoded("Remark", Remark)
                .build();
        return formBody;
    }
}
