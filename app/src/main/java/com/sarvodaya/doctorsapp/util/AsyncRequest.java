package com.sarvodaya.doctorsapp.util;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;

import com.sarvodaya.doctorsapp.R;


import java.io.IOException;

public class AsyncRequest extends AsyncTask<String, Void, String> {
    private ProgressDialog pDialog;
    Activity context;
    OnAsyncRequestComplete caller;
    String gravity = "CENTER";
    String Msg = "Processing, Keep patience...";
    boolean isTaskCompleted = false;
    Handler handler;
    private static final int INTERNET_TIME_OUT = 20000;
    int toastFlag = 0, recordFlag = 0;
    boolean isCancel = false;
    int finishFlag = 0;
    int Task = 0;
    String method = "null", key, value;
    Utility util;

    public AsyncRequest(Activity a, int task, String gravity, String Msg) {
        this.caller = (OnAsyncRequestComplete) a;
        this.gravity = gravity;
        this.Msg = Msg;
        this.context = a;
        this.Task = task;

    }

    public AsyncRequest(Activity a, int task, String Msg) {
        this.caller = (OnAsyncRequestComplete) a;
        this.Msg = Msg;
        this.context = a;
        this.Task = task;
    }

    public AsyncRequest(Activity a, int task) {
        this.caller = (OnAsyncRequestComplete) a;
        this.context = a;
        this.Task = task;
    }

    //method = save/load
    public AsyncRequest(Activity a, int task, String method, String key, String Value) {
        this.caller = (OnAsyncRequestComplete) a;
        this.context = a;
        this.method = method;
        this.key = key;
        this.value = Value;
        this.Task = task;
    }

    // Interface to be implemented by calling activity
    public interface OnAsyncRequestComplete {
        public void asyncResponse(String result, int task);
    }

    @Override
    protected void onPreExecute() {
        if (!Msg.equals("")) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(Msg);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
            if (gravity.equalsIgnoreCase("BOTTOM")) {
                pDialog.getWindow().setGravity(Gravity.BOTTOM);
            }
            pDialog.show();

        }


    }

    @Override
    protected String doInBackground(String... urls) {
        Log.e("AsyncBg", "execute");
        util = new Utility(context);
        if (!method.equalsIgnoreCase("null") && !util.LoadPreferences(key).equalsIgnoreCase("")) {
            return util.LoadPreferences(key);
        }
        String json = null;
//        try {
//            json = GETURL(urls[0]);
//            return json;
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        json=ApiCall.GET(urls[0]);
        if (!method.equalsIgnoreCase("null")) {
            util.SavePreferences(key, json);
        }
        return json;
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        Log.e("AsyncPost", "execute");
        if (!Msg.equals("")) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }


        caller.asyncResponse(result, Task);
        //finish();
    }

    @Override
    protected void onCancelled() {
        Log.e("AsyncCancel", "execute");

        //showToast(context.getResources().getString(R.string.server_time_out),false);
    }


}
