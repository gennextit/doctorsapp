package com.sarvodaya.doctorsapp.util;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class RequestHandler {

	static InputStream is=null;
	static String json="";
	static JSONArray jObj=null;
	
	static String response = null;
	public final static int GET = 1;
	public final static int POST = 2;

	public RequestHandler(){
		
	}
	/***********************************************/
/**************** Main Http Request Handler ************/
	/***********************************************/	

//	public String GETURL(String url) {
//		InputStream inputStream = null;
//		String result = "";
//		try {
//			
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (ClientProtocolException e) {
//		Log.e("GETURL",  e.toString());
//			result = "Server Exception";
//		} catch (IOException e) {
//			Log.e("GETURL",  e.toString());
//			result = "Input/Output Exception";
//		}
//
//		return result;
//	}

	
	/***********************************************/
/**************** Http Post Url Method  ***************/
	/***********************************************/	
	
//	public String POSTURL(String url) {
//		InputStream inputStream = null;
//		String result = "";
//		try {
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//			HttpPost httpPost=new HttpPost(url);
//			   List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
//				  nvps.add(new BasicNameValuePair("uname", "Aj"));
//				  nvps.add(new BasicNameValuePair("pass", "aman"));
//				  nvps.add(new BasicNameValuePair("name", "hsdg"));
//				  nvps.add(new BasicNameValuePair("email", "hds"));
//
//		        UrlEncodedFormEntity p_entity = new UrlEncodedFormEntity(nvps,HTTP.UTF_8);
//		        httpPost.setEntity(p_entity);
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(httpPost);
//
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (ClientProtocolException e) {
//			Log.e("GETURL",  e.toString());
//			result = "Server Exception";
//		} catch (IOException e) {
//			Log.e("GETURL",  e.toString());
//			result = "Input/Output Exception";
//		}
//		Log.e("tag", result);
//		return result;
//	}
	
	/***********************HELPER Method*************************/
	
	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	
	/***********************************************/
/************** Get JSON from Assets folder  ***********/
	/***********************************************/	
	
	//Method that will parse the JSON file and will return a JSONObject 
	public String parseAssetsData(InputStream data) {
	        String JSONString = null;
	        JSONObject JSONObject = null;
	        try {

	            //open the inputStream to the file 
	            InputStream inputStream = data;

	            int sizeOfJSONFile = inputStream.available();

	            //array that will store all the data 
	            byte[] bytes = new byte[sizeOfJSONFile];

	            //reading data into the array from the file
	            inputStream.read(bytes);

	            //close the input stream
	            inputStream.close();

	            JSONString = new String(bytes, "UTF-8");
	            
	        }  catch (IOException e) {
				Log.e("GETURL",  e.toString());
				JSONString = "Input/Output Exception";
			}
	        return JSONString;
	   } 
	
	
}