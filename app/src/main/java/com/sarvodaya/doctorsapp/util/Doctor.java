package com.sarvodaya.doctorsapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class Doctor {

    // @test
//    public static String testMobile="7840079095";//"LSHHI10";
    public static String testMAC = "34:AA:8B:F0:04:4E"; //"LSHHI81";
    public static String testDrId = "LSHHI204";//"LSHHI10";
    public static String testDrIdIPD = "LSHHI204";//"LSHHI10";
    //
    public static String testDrName = "Amit Gupta";
    //    public static String testImageURL="http://www.sarvodayahospital.com/appoint/thumbnail/LSHHI10.jpg";
    public static String testImageURL = "";


    public static final String COMMON = "dr";

    public static final String DOCTORID = "doctorid" + COMMON;
    public static final String DOCTOR_IPD_ID = "doctorIpdId" + COMMON;
    public static final String DOCTORNAME = "doctorname" + COMMON;
    public static final String DOCTOR_IMAGE = "doctor_image" + COMMON;
    public static final String DOCTOR_MOBILE = "doctormobile" + COMMON;


    public static String setDoctorDetail(Context context, String doctorId, String doctorName, String doctorIdForIpd) {

        if (!LoadPref(context, DOCTORID).equals("")) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTORID, doctorId);
            editor.putString(DOCTORNAME, doctorName);
            editor.putString(DOCTOR_IPD_ID, doctorIdForIpd);
            editor.commit();

            return "success";
        } else {
            JsonParser.ERRORMESSAGE = "Doctor not found";
            return "failure";
        }
    }

    public static void setDoctorId(Context context, String drId) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTORID, drId);
            editor.apply();
        }
    }

    public static void setDoctorIPDId(Context context, String drIpdId) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTOR_IPD_ID, drIpdId);
            editor.apply();
        }
    }

    public static void setDoctorName(Context context, String doctorName) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTORNAME, doctorName);
            editor.apply();
        }
    }

    public static void setDoctorImage(Context context, String doctorImage) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTOR_IMAGE, doctorImage);
            editor.apply();
        }
    }

    public static void setDoctorMobile(Context context, String doctorMobile) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DOCTOR_MOBILE, doctorMobile);
            editor.apply();
        }
    }

//    public String LoadDrName(Activity act){
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(act);
//        String  data = sharedPreferences.getString(AppTokens.DOCTORNAME, "") ;
//        return data;
//    }
//
//    public String LoadDrId(Activity act){
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(act);
//        String  data = sharedPreferences.getString(AppTokens.DOCTORID, "") ;
//        return data;
//    }


    public static String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public static String getDoctorName(Context context) {
        return LoadPref(context, DOCTORNAME);
    }

    public static String getDoctorId(Context context) {
        return LoadPref(context, DOCTORID);
    }

    public static String getDoctorIpdId(Context context) {
        return LoadPref(context, DOCTOR_IPD_ID);
    }

    public static String getDoctorImage(Context context) {
        return LoadPref(context, DOCTOR_IMAGE);
    }

    public static String getDoctorMobile(Context context) {
        return LoadPref(context, DOCTOR_MOBILE);
    }
}
