package com.sarvodaya.doctorsapp.util;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Abhijit on 07-Oct-16.
 */

public class HttpRedirectUrl {

    public static String fileUrl(String fAddress) {
        URL url = null;
        String Location = null;
        try {
            url = new URL(fAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection ucon = null;
        try {
            ucon = (HttpURLConnection) url.openConnection();
            ucon.setInstanceFollowRedirects(false);
            Location =ucon.getHeaderField("Location");
//            URLConnection conn = secondURL.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Location;
    }

    public static void fileDownload(String fAddress, String destinationDir) {

        int slashIndex = fAddress.lastIndexOf('/');
        int periodIndex = fAddress.lastIndexOf('.');

        String fileName = fAddress.substring(slashIndex + 1);

        if (periodIndex >= 1 && slashIndex >= 0
                && slashIndex < fAddress.length() - 1) {
//            fileUrl(fAddress, fileName, destinationDir);
        } else {
            System.err.println("path or file name.");
        }
    }
}
