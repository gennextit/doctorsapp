package com.sarvodaya.doctorsapp.util;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import java.util.regex.Pattern;

public class Validation {
 
	//test Regular Exp
	//http://www.regular-expressions.info/javascriptexample.html
	
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "^[7-9][0-9]{9}$";
    
    private static final String PHONE_REGEX_2 = "^[0-9][0-9]{6,10}$";
    
    private static final String AGE_REGEX = "^[7-9][0-9]{9}$";
    private static final String NAME_REGEX = "[a-zA-Z ]{3,100}";
    //dd-MM-yyyy,dd/MM/yyyy
    private static final String DATE_REGEX = "^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$";
    //MM-dd-yyyy,MM/dd/yyyy
    private static final String DATE1_REGEX = "^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$";
    
    //Support exp 2300,23:00,4 am ,4am ,4pm ,4 pm,04:30pm ,04:30 pm ,4:30pm ,4:30 pm,04.30pm,04.30 pm,4.30pm,4.30 pm ,23:59 ,0000 ,00:00
    private static final String TIME_REGEX = "";
    
    // Error Messages
    private static final String REQUIRED_MSG = "required";
    private static final String EMAIL_MSG = "invalid email";
    private static final String PHONE_MSG = "invalid phone";
    private static final String NAME_MSG = "invalid Name";
    private static final String STATE_MSG = "invalid State";
    private static final String CITY_MSG = "invalid City";
    private static final String AGE_MSG = "invalid Age";
    private static final String DISCOUNT_MSG = "invalid Discount";
    private static final String EMPTY = "Empty";
    
    // call this method when you need to check email validation
    public static boolean isEmail(EditText editText, boolean required) {
        return isValidEmail(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }
    
 
    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }
    
    
    public static boolean isName(EditText editText, boolean required) {
        return isValid(editText, NAME_REGEX,NAME_MSG, required);
    }
    
//    public static boolean isString(EditText editText,String invalidMsg, boolean required) {
//        return isValid(editText, NAME_REGEX, invalidMsg, required);
//    }
    
//    public static boolean isSpinner(Spinner spinner, boolean required) {
//    	if(hasText(spinner)){
//    		return true;
//    	}
//        return false;
//    }
//    
//    public static boolean isGender(RadioButton male,RadioButton female,RadioGroup ll, boolean required) {
//    	if(hasRadioButton(male,female,ll)){
//    		return true;
//    	}
//        return false;
//    }
//    
    

	public static boolean isEmpty(EditText editText, boolean required) {
    	if(hasText(editText)){
    		return true;
    	}
        return false;
    }
 
    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
 
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
 
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;
 
        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        };
 
        return true;
    }
 
    public static boolean isValidEmail(EditText editText, String regex, String errMsg, boolean required) {
    	 
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
//        if(editText.getText().toString().equals("")){
//        	return true;
//        }
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;
 
        // pattern doesn't match so returning false
        if ( required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        };
 
        return true;
    }
    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText) {
 
        String text = editText.getText().toString().trim();
        editText.setError(null);
 
        // length 0 means there is no text
        if (text.length() == 0) {
        	
        	SpannableString s = new SpannableString(EMPTY);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
            editText.requestFocus();
            editText.setError(s); 
            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
            //editText.setError(REQUIRED_MSG);
            return false;
        }
 
        return true;
    }
//    public static boolean hasText(Spinner spinner) {
//    	 
//        String text = spinner.getSelectedItem().toString().trim();
//        // spinner.setError(null);
// 
//        // length 0 means there is no text
//        if (text.equalsIgnoreCase("Select Option")||text.equalsIgnoreCase("Record not Available")) {
//        	
//        	
//            spinner.setBackgroundResource(R.drawable.sp_error_bg);
//            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
//            //editText.setError(REQUIRED_MSG);
//            return false;
//        }
// 
//        return true;
//    }
    
//    public static boolean hasRadioButton(RadioButton male, RadioButton female, RadioGroup ll) {
//		// TODO Auto-generated method stub
//    	
//    	if (!male.isChecked()&&!female.isChecked()) {
//        	
//        	
//            ll.setBackgroundResource(R.drawable.sp_error_bg);
//            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
//            //editText.setError(REQUIRED_MSG);
//            return false;
//        }
// 
//        return true;
//	}
//    public static boolean hasAutoCompleteEditText(String matchChar,String cat,ArrayList<EmpModel> emp, AutoCompleteTextView ac) {
//		// TODO Auto-generated method stub
//    	
//    			matchChar = matchChar.toLowerCase(Locale.getDefault());
//    	    	String StoreEmpId;
//    	    	if(!emp.isEmpty()) {
//    	    		for (EmpModel model : emp) 
//    	    		{
//    	    			if(cat.equalsIgnoreCase("state")){
//    	    				if (model.getstate_name().toLowerCase(Locale.getDefault()).equalsIgnoreCase(matchChar)) 
//        	    			{
//        	    				return true;
//        	    			}
//    	    			}else{
//    	    				if (model.getcity_name().toLowerCase(Locale.getDefault()).equalsIgnoreCase(matchChar)) 
//        	    			{
//        	    				return true;
//        	    			}
//    	    			}
//    	    			
//    	    		}
//    	    	}
//    	    	if(cat.equalsIgnoreCase("state")){
//    	    		SpannableString s = new SpannableString(STATE_MSG);
//        			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
//        			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
//        			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        			ac.requestFocus();
//                    ac.setError(s);
//    	    	}else{
//    	    		SpannableString s = new SpannableString(CITY_MSG);
//        			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
//        			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
//        			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        			ac.requestFocus();
//                    ac.setError(s);
//    	    	}
//    	    	
//                
//    	    	return false;
//    	   
//	}
    public static boolean isEmpty(AutoCompleteTextView ac) {
    	String text = ac.getText().toString().trim();
    	ac.setError(null);
 
        // length 0 means there is no text
        if (text.length() == 0) {
        	
        	SpannableString s = new SpannableString(EMPTY);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			ac.requestFocus();
			ac.setError(s); 
            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
            //editText.setError(REQUIRED_MSG);
            return false;
        }
 
        return true;
    	   
	}
    public static boolean isAge(EditText editText) {
    	String text = editText.getText().toString().trim();
        editText.setError(null);
        int ageValue=0;
        if (text.length() != 0){
        	ageValue = Integer.parseInt(text); 
        }
        
        // length allow between 1-150
        if (ageValue == 0 || ageValue > 150) {
        	
        	SpannableString s = new SpannableString(AGE_MSG);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
            editText.requestFocus();
            editText.setError(s); 
            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
            //editText.setError(REQUIRED_MSG);
            return false;
        }
 
        return true;
    }
    public static boolean isValidDiscount(EditText editText) {
    	String text = editText.getText().toString().trim();
        editText.setError(null);
        float discValue=0.0f;
        if (text.length() != 0){
        	discValue = Float.parseFloat(text);
        }
        
        // length allow between 1-150
        if (discValue > 100) {
        	
        	SpannableString s = new SpannableString(DISCOUNT_MSG);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
            editText.requestFocus();
            editText.setError(s); 
            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
            //editText.setError(REQUIRED_MSG);
            return false;
        }
 
        return true;
    }
    
    public static boolean isMobile(EditText editText) {
    	String text = editText.getText().toString().trim();
        editText.setError(null);
//        int res=0;
//        if (text.length() != 0){
//        	res = Integer.valueOf(text); 
//        }
//        
//        // length allow between 1-150
//        if (text.length()|| res>10||res ==9) {
//        	L.m("MyCondition");
//        	SpannableString s = new SpannableString(PHONE_MSG);
//			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
//			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
//			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//			
//            editText.requestFocus();
//            editText.setError(s); 
//            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
//            //editText.setError(REQUIRED_MSG);
//            return false;
//        }
        if (!Pattern.matches(PHONE_REGEX_2, text)) {
        	L.m("regExp");
            editText.setError(PHONE_MSG);
            return false;
        };
 
        return true;
    }
    
    public static boolean is10DigitMobile(EditText editText) {
    	String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0|| text.length() <10) {
        	
        	SpannableString s = new SpannableString(PHONE_MSG);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
			s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			editText.requestFocus();
			editText.setError(s); 
            //editText.setError(Html.fromHtml("<font color='blue'>this is the error</font>"));
            //editText.setError(REQUIRED_MSG);
            return false;
        }
 
        return true;
    }
    
    public static boolean isString(String value) {
    	
        if (value==null||value.equals("")) {
        	
        	return false;
        };
 
        return true;
    }
    
}