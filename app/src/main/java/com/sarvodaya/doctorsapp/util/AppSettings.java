package com.sarvodaya.doctorsapp.util;

import static com.sarvodaya.doctorsapp.util.AppTokens.*;

public class AppSettings {

	// @test
	public static final int APP_TEST_PROD = APP_UNDER_PRODUCTION;


	//	public static final String WEB_SERVICE_URL =  "http://103.43.4.141/mobileApp/mobileapp.asmx/";
	public static final String COMMON_LIVE =  "http://services.sarvodayahospital.com/mobileApp/mobileapp.asmx/";
	//	public static final String COMMON_LIVE =  "http://103.43.4.141/mobileApp/mobileapp.asmx/";
//	public static final String COMMON________________DEMO =  "http://103.43.4.141/mobileApp_Demo/mobileapp.asmx/";
	public static final String COMMON =COMMON_LIVE;
	
//	register Mobile No
//	http://sarvodayahospital.com/doctorapp/index.php/admin

	
	/*************************** Authentication ***********************************/
	//public static final String VALIDATE_DOCTOR="http://192.168.0.67/doctor_chat/index.php/admin/validate_doctor";//?mobile=9560248029&mac_addr=

	public static final String VALIDATE_DOCTOR="http://SarvodayaHospital.com/doctorapp/index.php/admin/validate_doctor";//?mobile=9560248029&mac_addr=

	/*************************** Main ***********************************/
	public static final String LOADDOCTOR =COMMON+ "LoadDoctor";
	
	/*************************** OPD ***********************************/
	//public static final String UPDATE_PATIENT_DETAIL ="update_patient_details/";//?appId=&visitedId=&date=&time=&status
	public static final String GetOPDPatientList =COMMON+ "GetOPDPatientList";//?DoctorId=LSHHI81&Date=2012-10-06
	public static final String GetOPDPatientDetails=COMMON+  "GetOPDPatientDetails";//?App_ID=APP/121006/00004
	public static final String CallPatient =COMMON+  "CallPatient";//?DoctorId=LSHHI81&App_ID=APP/121006/00004
	public static final String UnCallPatient =COMMON+  "UnCallPatient";//?App_ID=APP/121006/00004
	public static final String InPatient = COMMON+ "InPatient";//?DoctorId=LSHHI81&App_ID=APP/121006/00004
	public static final String OutPatient =COMMON+  "OutPatient";//?App_ID=APP/121006/00004

	/*************************** IPD ***********************************/
	//public static final String UPDATE_PATIENT_DETAIL ="update_patient_details/";//?appId=&visitedId=&date=&time=&status
	public static final String GetIPDPatientList = COMMON+ "GetIPDPatientList";//?Doctor_id=LSHHI81
	public static final String GetIPDPatientDetails=COMMON+  "GetIPDPatientDetails";//?IPNO=
	
	/*************************** OTBooking ***********************************/
	public static final String ListofOT =COMMON+ "ListofOT";//?Date=
	public static final String RequestOTBooking =COMMON+ "RequestOTBooking";//?OTId=&DoctorId=&FromDate=&FromTime=&ToDate=&ToTime=&Reason=
	
	/*************************** SelfHelp ***********************************/
	public static final String RequestLeave =COMMON+ "RequestLeave";//?DoctorId=&FromDate=&FromTime=&ToDate=&ToTime=&Reason=
	public static final String InformDelayToPatient =COMMON+ "InformDelayToPatient";//?DoctorId=&option=
	public static final String ApproveRejectDischargeSummary =COMMON+ "ApproveRejectDischargeSummary";//?DoctorId=&option=
	public static final String GETIPDDischargeDetails =COMMON+ "GETIPDDischargeDetails";//?Doctor_ID=&IPNO=
	public static final String UpdateDischargeSummary =COMMON+ "UpdateDischargeSummary";//?Doctor_ID=&IPNO=

	/*************************** My Chat *******************************/
//	public static final String LOCALHOST ="http://192.168.0.64/";
//	public static final String COMMON_LOCAL =LOCALHOST+"Doctor_chat/index.php/";
//	public static final String ALL_USERS =COMMON_LOCAL+"doctor/all_users";
//	public static final String ONLINE =COMMON_LOCAL+"doctor/online";//?doctor_id=LSHHI161
//	public static final String OFFLINE =COMMON_LOCAL+"doctor/offline";//?mobile=
//	public static final String SEND_TEXT =COMMON_LOCAL+"chat/send_text";//?text=&date=&time=&userid=&cuserid=
//	public static final String VIEW_TEXT =COMMON_LOCAL+"chat/view_text";//?userid=Abhijit%20Rao&cuserid=pankaj@gmail.com
//	public static final String CHAT_HISTORY =COMMON_LOCAL+"chat/chat_history";//?userid=LSHHI32&cuserid=LSHHI161&date=20160203&time=12:20"
	
	/************* Online Users **************/
	public static final String ALL_USERS ="http://SarvodayaHospital.com/doctorapp/index.php/Doctor/all_users";

	public static final String ONLINE ="http://SarvodayaHospital.com/doctorapp/index.php/Doctor/online";//?doctor_id=LSHHI161
	public static final String OFFLINE ="http://SarvodayaHospital.com/doctorapp/index.php/Doctor/offline";//?mobile=
	
	/************* Users Chat **************/
	public static final String SEND_TEXT ="http://SarvodayaHospital.com/doctorapp/index.php/Doctor/send_text";//?text=&date=&time=&userid=&cuserid=
	public static final String VIEW_TEXT ="http://SarvodayaHospital.com/doctorapp/index.php/Doctor/view_text";//?userid=Abhijit%20Rao&cuserid=pankaj@gmail.com
	public static final String REPORT_SERVER_ERROR = "http://SarvodayaHospital.com/doctorapp/index.php/Doctor/serverErrorReporting";


//	/************* Registration **************/
//	public static final String REGISTRATION =ALL_USERS+"/registration";//?name=&uname=&pass=&mobile=
//	public static final String LOGIN =ALL_USERS+"/login";//?uname=f&pass=12345
//	public static final String LOGOUT =ALL_USERS+"/logout";//?uname=f&pass=12345

	//public static final String VIEW_LOCATION ="http://kushwaha.freevar.com/track/location/set_position";//?lattitude1=28.6281752&longitude1=77.3739567&lattitude2=28.618888&longitude2=77.3724585

	
	
}

