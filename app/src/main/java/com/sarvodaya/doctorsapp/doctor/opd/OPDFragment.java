package com.sarvodaya.doctorsapp.doctor.opd;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.global.DatePickerDialog;
import com.sarvodaya.doctorsapp.model.OPDModel;
import com.sarvodaya.doctorsapp.model.adapter.OPDAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppAnimation;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.DateTimeUtility;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Calendar;

import static com.sarvodaya.doctorsapp.model.OPDModel.OUT;
import static com.sarvodaya.doctorsapp.model.OPDModel.PAID;
import static com.sarvodaya.doctorsapp.model.OPDModel.UNPAID;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class OPDFragment extends CompactFragment implements View.OnClickListener, DatePickerDialog.DateSelectListener {
    private int sltTab;
    ListView lvMain;
    LinearLayout progressBar;
    ArrayList<OPDModel> sPaidList, sUnpaidList, sOutList;
    Button PaidTab, UnpaidTab, OutTab;
    TextView ViewDate;
    LinearLayout SltDate;
    private String paidError;
    private String unPaidError;
    private String outError;

    static String sltDate;
    int sday, smonth, syear;
    AssignTask assignTask;
    private ArrayList<OPDModel> opdList;

    OPDAdapter opdAdapter;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (opdAdapter != null) {
            opdAdapter.onDetach();
        }
    }

    public static Fragment newInstance(Activity context) {
        OPDFragment opdFragment = new OPDFragment();
        AppAnimation.setSlideAnimation(context, opdFragment, Gravity.BOTTOM);
        return opdFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_opd, container, false);
        onActionBack(v, "OPD");

        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.lv_school);
        progressBar = (LinearLayout) v.findViewById(R.id.ll_progress);

        ViewDate = (TextView) v.findViewById(R.id.tv_opd_viewDate);
        SltDate = (LinearLayout) v.findViewById(R.id.ll_opd_selectDate);
        PaidTab = (Button) v.findViewById(R.id.btn_opd_paidTab);
        UnpaidTab = (Button) v.findViewById(R.id.btn_opd_unpaidTab);
        OutTab = (Button) v.findViewById(R.id.btn_opd_outTab);

        SltDate.setOnClickListener(this);
        PaidTab.setOnClickListener(this);
        UnpaidTab.setOnClickListener(this);
        OutTab.setOnClickListener(this);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String appNo = null, patientName = null, appId = null;

                switch (sltTab) {
                    case PAID:
                        if (sPaidList != null) {
                            appNo = sPaidList.get(position).getAppNo();
                            patientName = sPaidList.get(position).getPatientName();
                            appId = sPaidList.get(position).getAppId();
                            OPDPatientHistory opdPatientHistory = new OPDPatientHistory();
                            opdPatientHistory.setBasicDetail(appNo, patientName, appId);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.add(android.R.id.content, opdPatientHistory, "opdPatientHistory");
                            transaction.addToBackStack("opdPatientHistory");
                            transaction.commit();
                        }
                        break;
                    case UNPAID:
                        if (sUnpaidList != null) {
                            showPopUpDialog(getActivity(), "No Detail Available for patient\n" + sUnpaidList.get(position).getPatientName(), PopUpDialog.DEFAULT);
                        }
                        break;
                    case OUT:
                        if (sOutList != null) {
                            appNo = sOutList.get(position).getAppNo();
                            patientName = sOutList.get(position).getPatientName();
                            appId = sOutList.get(position).getAppId();
                            OPDPatientHistory opdPatientHistory = new OPDPatientHistory();
                            opdPatientHistory.setBasicDetail(appNo, patientName, appId);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.add(android.R.id.content, opdPatientHistory, "opdPatientHistory");
                            transaction.addToBackStack("opdPatientHistory");
                            transaction.commit();
                        }
                        break;
                }


            }
        });

        sltTab = PAID;
        getDateTime();

        executeAssignTask();


    }

    public void executeAssignTask() {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(), sltDate);
            assignTask.execute(AppSettings.GetOPDPatientList);
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    public void getDateTime() {
        final Calendar cal = Calendar.getInstance();
        syear = cal.get(Calendar.YEAR);
        smonth = cal.get(Calendar.MONTH);
        sday = cal.get(Calendar.DAY_OF_MONTH);
        sltDate = DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear);
        ViewDate.setText(convertDate2(sltDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_opd_selectDate:
//                DialogFragment newFragment = new SelectDateFragment();
//                newFragment.show(getFragmentManager(), "DatePicker");
                DatePickerDialog.newInstance(OPDFragment.this, sday, smonth, syear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.btn_opd_paidTab:
                selTab(PAID);
                break;
            case R.id.btn_opd_unpaidTab:
                selTab(UNPAID);
                break;
            case R.id.btn_opd_outTab:
                selTab(OUT);
                break;
        }
    }

    public void selTab(int selkey) {
//        AsyncRequest getPosts = new AsyncRequest(OPDActivity.this,1,"save","storeOPD","");

        switch (selkey) {
            case PAID:
                sltTab = PAID;
                PaidTab.setBackgroundResource(R.drawable.tab_select_bg);
                PaidTab.setTextColor(Color.WHITE);
                UnpaidTab.setBackgroundResource(R.drawable.tab_bg);
                UnpaidTab.setTextColor(Color.parseColor("#404040"));
                OutTab.setBackgroundResource(R.drawable.tab_bg);
                OutTab.setTextColor(Color.parseColor("#404040"));
//                getPosts.execute(AppSettings.GetOPDPatientList+"?DoctorId="+DoctorId+"&Date="+date);
                setSlotData();

                break;
            case UNPAID:
                sltTab = UNPAID;
                PaidTab.setBackgroundResource(R.drawable.tab_bg);
                PaidTab.setTextColor(Color.parseColor("#404040"));
                UnpaidTab.setBackgroundResource(R.drawable.tab_select_bg);
                UnpaidTab.setTextColor(Color.WHITE);
                OutTab.setBackgroundResource(R.drawable.tab_bg);
                OutTab.setTextColor(Color.parseColor("#404040"));

//                getPosts.execute(AppSettings.GetOPDPatientList+"?DoctorId="+DoctorId+"&Date="+date);
                setSlotData();

                break;
            case OUT:
                sltTab = OUT;
                PaidTab.setBackgroundResource(R.drawable.tab_bg);
                PaidTab.setTextColor(Color.parseColor("#404040"));
                UnpaidTab.setBackgroundResource(R.drawable.tab_bg);
                UnpaidTab.setTextColor(Color.parseColor("#404040"));
                OutTab.setBackgroundResource(R.drawable.tab_select_bg);
                OutTab.setTextColor(Color.WHITE);

//                getPosts.execute(AppSettings.GetOPDPatientList+"?DoctorId="+DoctorId+"&Date="+date);
                setSlotData();

                break;

        }

    }

    @Override
    public void onSelectDateClick(DialogFragment dialog, int date, int month, int year) {
        sltDate = DateTimeUtility.convertDateYYYYMMDD(date, month, year);
        ViewDate.setText(convertDate2(sltDate));
        syear = year;
        smonth = month;
        sday = date;
        executeAssignTask();
    }

//    @SuppressLint("ValidFragment")
//    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Calendar calendar = Calendar.getInstance();
//            DatePickerDialog dpd;
//
//            dpd = new DatePickerDialog(getActivity(), this, syear, smonth, sday);
//
//            // Set the DatePicker minimum date selection to current date
////            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
//            // dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way
//            // to get the current day
//
//            // //Add 6 days with current date
//            // calendar.add(Calendar.DAY_OF_MONTH,6);
//            //
//            // //Set the maximum date to select from DatePickerDialog
//            // dp.setMaxDate(calendar.getTimeInMillis());
//
//            return dpd;
//        }
//
//        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
//            populateSetDate(yy, mm, dd);
////			storeTimeStamp(yy,mm,dd);
//
//
//        }
//
//        public void populateSetDate(int year, int month, int day) {
//            sltDate = DateTimeUtility.convertDateYYYYMMDD(day, month, year);
//            ViewDate.setText(convertDate2(sltDate));
//            syear = year;
//            smonth = month;
//            sday = day;
//
//            executeAssignTask();
//        }
//    }

    private class AssignTask extends AsyncTask<String, Void, String> {

        Context activity;
        String date;

        public AssignTask(Activity activity, String date) {
            onAttach(activity);
            this.date = date;
        }

        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
//            showProgressWithAnimation(progressBar);
            showProgressDialog(activity, "Processing, please wait... ", Gravity.CENTER);
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error", drId = "";
            OPDModel result;
//            List<BasicNameValuePair> params = new ArrayList<>();
            if (activity != null) {
                drId = Doctor.getDoctorId(activity);
//                params.add(new BasicNameValuePair("DoctorId", Doctor.getDoctorId(activity)));
//                params.add(new BasicNameValuePair("Date", date));
            }

            JsonParser jsonParser = new JsonParser(getActivity());

//            response = ob.makeConnection(urls[0], HttpReq.GET, params);
            response = ApiCall.POST(urls[0], RequestBuilder.setDrDate(drId, date));
            result = jsonParser.getOPDList2(response, date);
            if (result != null) {
                if (result.getOutputPaid().equals("success")) {
                    sPaidList = result.getPaidList();
                } else if (result.getOutputPaid().equals("failure")) {
                    paidError = result.getOutputError();
                } else if (result.getOutputPaid().equals("NA")) {
                    sPaidList = null;
                }

                if (result.getOutputUnPaid().equals("success")) {
                    sUnpaidList = result.getUnPaidList();
                } else if (result.getOutputUnPaid().equals("failure")) {
                    unPaidError = result.getOutputError();
                } else if (result.getOutputUnPaid().equals("NA")) {
                    sUnpaidList = null;
                }

                if (result.getOutputOut().equals("success")) {
                    sOutList = result.getOutList();
                } else if (result.getOutputOut().equals("failure")) {
                    outError = result.getOutputError();
                } else if (result.getOutputOut().equals("NA")) {
                    sOutList = null;
                }
                return "success";
            } else {
                return null;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (activity != null) {
                //progressBar.setVisibility(View.GONE);
//                hideProgressWithAnimation(progressBar);
                hideProgressDialog();
                if (result != null) {
                    setSlotData();
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask();
                        }
                    });

//                    Toast.makeText(getActivity(), JsonParser.ERRORMESSAGE, Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    private void setSlotData() {

        switch (sltTab) {
            case PAID:
                if (sPaidList != null) {
                    opdAdapter = new OPDAdapter(getActivity(), R.layout.slot_opd, sPaidList, PAID, OPDFragment.this);
                    lvMain.setAdapter(opdAdapter);
//                }else if(paidError!=null){
//                    Toast.makeText(getActivity(), paidError, Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<String> errorList = new ArrayList<>();
                    errorList.add("No paid patient available");
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_error,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                }
                break;
            case UNPAID:
                if (sUnpaidList != null) {
                    opdAdapter = new OPDAdapter(getActivity(), R.layout.slot_opd, sUnpaidList, UNPAID, OPDFragment.this);
                    lvMain.setAdapter(opdAdapter);
//                }else if(unPaidError!=null){
//                    Toast.makeText(getActivity(), unPaidError, Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<String> errorList = new ArrayList<>();
                    errorList.add("No Unpaid patient available");
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_error,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                }
                break;
            case OUT:
                if (sOutList != null) {
                    opdAdapter = new OPDAdapter(getActivity(), R.layout.slot_opd, sPaidList, OUT, OPDFragment.this);
                    lvMain.setAdapter(opdAdapter);
//                }else if(outError!=null){
//                    Toast.makeText(getActivity(), outError, Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<String> errorList = new ArrayList<>();
                    errorList.add("No out patient available");
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_error,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                }
                break;
        }


    }


}