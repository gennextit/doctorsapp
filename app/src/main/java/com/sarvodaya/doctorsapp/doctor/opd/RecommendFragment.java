package com.sarvodaya.doctorsapp.doctor.opd;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.model.RecommendModel;
import com.sarvodaya.doctorsapp.model.adapter.RecommendDialogAdapter;
import com.sarvodaya.doctorsapp.util.JsonParser;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 20-Mar-17.
 */

public class RecommendFragment extends CompactFragment{

    private GridView gridView;
    private RecommendDialogAdapter adapter;
    private ArrayList<RecommendModel> rcList;

    public static RecommendFragment newInstance() {
        RecommendFragment fragment=new RecommendFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragent_recommend,container,false);
        onActionBack(v,"Recommend" );
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        gridView = (GridView) v.findViewById(R.id.gv_profile_selection);

        rcList = getAllSelectedDayAndBusinessList();
        if (rcList != null) {
            adapter = new RecommendDialogAdapter(getActivity(), rcList);
            gridView.setAdapter(adapter);
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    RecommendModel model = adapter.getItem(pos);
                    if (model != null)
                        if (model.getChecked()) {
                            model.setChecked(false);
                            rcList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        } else {
                            model.setChecked(true);
                            rcList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        }
                }
            }
        });

    }

    private ArrayList<RecommendModel> getAllSelectedDayAndBusinessList() {
        RecommendModel allModel = getAllDayAndBusinessList();
        return allModel.getList();
    }

    private RecommendModel getAllDayAndBusinessList() {
        return JsonParser.parseRecommendJson();
    }
}
