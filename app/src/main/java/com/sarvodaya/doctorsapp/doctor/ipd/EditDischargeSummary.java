package com.sarvodaya.doctorsapp.doctor.ipd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.EditDischargeEntityActivity;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.global.PopupDialog;
import com.sarvodaya.doctorsapp.model.DischargeSummaryModel;
import com.sarvodaya.doctorsapp.model.adapter.DischargeSummaryAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 20-Mar-17.
 */

public class EditDischargeSummary extends CompactFragment implements PopupDialog.DialogListener {

    private static final int TASK_GET = 1, TASK_UPDATE = 2;
    private static final int TASK_UPDATE_DETAIL = 2;
    private static final int REQUEST_EDIT_DETAIL = 11;
    private ExpandableListView lvMain;
    private DischargeSummaryAdapter adapter;
    private ArrayList<DischargeSummaryModel> sList;
    AssignTask assignTask;
    private String doctorId;
    private String ipNo;
    private Button btnUpdate;
    private TextView tvError;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static EditDischargeSummary newInstance(String doctorId, String ipNo) {
        EditDischargeSummary fragment = new EditDischargeSummary();
        fragment.doctorId = doctorId;
        fragment.ipNo = ipNo;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_edit_summary, container, false);
        onActionBackAct(v, "Edit Discharge Summary", getActivity());
        ((EditDischargeSummaryActivity) getActivity()).setmDataModified(false);
        initUi(v);
        return v;
    }

    public void onActionBackAct(View v, String title, final Activity activity) {
        LinearLayout backButton = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        TextView actionBarTitle = (TextView) v.findViewById(R.id.actionbar_title);
        actionBarTitle.setText(title);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    private void initUi(View v) {
        tvError = (TextView) v.findViewById(R.id.tvError);
        tvError.setVisibility(View.GONE);
        btnUpdate = (Button) v.findViewById(R.id.btn_update_changes);
        lvMain = (ExpandableListView) v.findViewById(R.id.expandableListView1);
        lvMain.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                if (sList != null) {
                    String category = sList.get(groupPosition).getCategory();
                    String name = sList.get(groupPosition).getChildList().get(childPosition).getName();
                    TextView tvName = (TextView) v.findViewById(R.id.group_name);
                    startEditDischargeEntity(tvName, category, name, groupPosition, childPosition);

                }
                return false;
            }
        });
        btnUpdate.setVisibility(View.GONE);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeAssignTask(TASK_UPDATE, doctorId, ipNo);
            }
        });
        executeAssignTask(TASK_GET, doctorId, ipNo);
    }

    private void startEditDischargeEntity(TextView tvName, String category, String name, int groupPosition, int childPosition) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Intent intent=new Intent(getActivity(),EditDischargeEntity.class);
//            intent.putExtra("summaryTitle",category);
//            intent.putExtra("summaryDescription",name);
//            intent.putExtra("groupPosition",groupPosition);
//            intent.putExtra("childPosition",childPosition);
//            ActivityOptions options = ActivityOptions
//                    .makeSceneTransitionAnimation(getActivity(), tvName, getSt(R.string.transition_title));
//            startActivityForResult(intent,REQUEST_EDIT_DETAIL);
//            return;
//        }
        Intent intent = new Intent(getActivity(), EditDischargeEntityActivity.class);
        intent.putExtra("summaryTitle", category);
        intent.putExtra("summaryDescription", name);
        intent.putExtra("groupPosition", groupPosition);
        intent.putExtra("childPosition", childPosition);
        startActivityForResult(intent, REQUEST_EDIT_DETAIL);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent inient) {
        if (requestCode == REQUEST_EDIT_DETAIL) {
            if (resultCode == Activity.RESULT_OK) {
                if (inient != null) {
                    String summaryTitle = inient.getStringExtra("summaryTitle");
                    String summaryDescription = inient.getStringExtra("summaryDescription");
                    int groupPosition = inient.getIntExtra("groupPosition", 0);
                    int childPosition = inient.getIntExtra("childPosition", 0);

                    if (sList != null) {
                        // Get the child from the adapter lists
                        DischargeSummaryModel groupData = sList.get(groupPosition); // Key for _listDataChild
                        ArrayList<DischargeSummaryModel> childDataList = groupData.getChildList(); // List of children
                        DischargeSummaryModel childData = childDataList.get(childPosition);
                        childData.setName(summaryDescription);

                        // Replace the old child list with an updated list (with updated child)
                        childDataList.set(childPosition, childData);
                        groupData.setChildList(childDataList);
                        sList.set(groupPosition, groupData);
                        adapter.notifyDataSetChanged();
                        ((EditDischargeSummaryActivity) getActivity()).setmDataModified(true);
                        btnUpdate.setVisibility(View.VISIBLE);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }


    public void executeAssignTask(int task, String doctorId, String ipNo) {
        if (isOnline()) {
            if (task == TASK_GET) {
                assignTask = new AssignTask(getActivity(), doctorId, ipNo, TASK_GET); //@test
                assignTask.execute(AppSettings.GETIPDDischargeDetails);
            } else if (task == TASK_UPDATE) {
                assignTask = new AssignTask(getActivity(), doctorId, ipNo, TASK_UPDATE); //@test
                assignTask.execute(AppSettings.UpdateDischargeSummary);
            }
        } else {
            showInternetAlertBox(getActivity());
        }
    }


    private class AssignTask extends AsyncTask<String, Void, DischargeSummaryModel> {
        int task;
        Context activity;
        String drId, ipNo;

        public AssignTask(Activity activity, String drId, String ipNo, int task) {
            onAttach(activity);
            this.task = task;
            this.drId = drId;
            this.ipNo = ipNo;
        }

        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == TASK_GET) {
                showProgressDialog(activity, "Getting Data, Keep patience...", Gravity.CENTER);
            } else {
                showProgressDialog(activity, "Updating Discharge detail, Keep patience...", Gravity.CENTER);
            }

        }

        @Override
        protected DischargeSummaryModel doInBackground(String... urls) {
            String response = "error";
            JsonParser jsonParser = new JsonParser(getActivity());
            if (task == TASK_GET) {
                response = ApiCall.POST(urls[0], RequestBuilder.GETDD(drId, ipNo));
                return jsonParser.GETIPDDischargeDetails(response);
            } else if (task == TASK_UPDATE) {
                String temp[];
                if (sList == null) {
                    return null;
                } else {
                    temp = new String[20];
                    for (int i = 0; i < sList.size(); i++) {
                        ArrayList<DischargeSummaryModel> child = sList.get(i).getChildList();
                        temp[i] = child.get(0).getName();
                    }

                    response = ApiCall.POST(urls[0], RequestBuilder.UpdateDD(drId, ipNo, getMACAddress(activity), temp));
                    return jsonParser.updateDischargeSummary(response);
                }
            } else {
                return null;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(DischargeSummaryModel result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == TASK_GET) {
                            sList = result.getGroupList();
                            if (sList != null) {
                                adapter = new DischargeSummaryAdapter(getActivity(), sList, lvMain);
                                lvMain.setAdapter(adapter);
                            }
                        } else if (task == TASK_UPDATE) {
                            ((EditDischargeSummaryActivity) getActivity()).setmDataModified(false);
                            btnUpdate.setVisibility(View.GONE);
                            Toast.makeText(activity, result.getOutputMsg(), Toast.LENGTH_LONG).show();
                            getActivity().finish();
                        }
                    } else if (result.getOutput().equals("failure")) {
                        if (task == TASK_GET) {
                            tvError.setText(result.getOutputMsg());
                            tvError.setVisibility(View.VISIBLE);
                        } else if (task == TASK_UPDATE) {
                            showPopUpDialog(getActivity(), result.getOutputMsg(), PopUpDialog.DEFAULT);
//                            Toast.makeText(activity,result.getOutputMsg(),Toast.LENGTH_LONG).show();
                        }
                    } else {
                        showPopUpDialog(getActivity(), result.getOutput(), PopUpDialog.DEFAULT);
                    }

                } else {
                    if (task == TASK_GET) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                executeAssignTask(TASK_GET, null, null);
                            }
                        });
                    }

                }
            }

        }
    }

    @Override
    public void onOkClick(DialogFragment dialog) {
        btnUpdate.performClick();
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {
        getActivity().finish();
    }

    public void onActionBack() {
        PopupDialog.newInstance("Are you sure to exit?", "Changes could not to be updated to the server. Do you want to update changes?",
                "Update", "Exit", EditDischargeSummary.this).show(getFragmentManager(), "popupDialog");
    }
}
