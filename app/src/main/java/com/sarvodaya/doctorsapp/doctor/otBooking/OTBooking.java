package com.sarvodaya.doctorsapp.doctor.otBooking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.global.DatePickerDialog;
import com.sarvodaya.doctorsapp.global.TimePickerDialog;
import com.sarvodaya.doctorsapp.model.OTBookingModel;
import com.sarvodaya.doctorsapp.model.adapter.OTListAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppAnimation;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.DateTimeUtility;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;
import com.sarvodaya.doctorsapp.util.Validation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.sarvodaya.doctorsapp.model.OTBookingModel.LIST_OF_OT;
import static com.sarvodaya.doctorsapp.model.OTBookingModel.REQUEST_OT_BOOKING;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class OTBooking extends CompactFragment implements View.OnClickListener
        ,DatePickerDialog.DateSelectListener ,TimePickerDialog.TimeSelectTypeListener{


    AssignTask assignTask;
    private TextView tvPatient;

    TextView tvViewDate, OTview, tvSTime, tvETime;
    LinearLayout SpinnerOTId, Date, StartTime, EndTime;
    EditText etReason;
    String OTId = "", OTDate = "", OTtvSTime = "", OTtvETime = "";
    int day, month, year, hours, min;
    Button Submit;
    String[] listContent = {"Operation Theater 1", "Operation Theater 2", "Operation Theater 3", "Operation Theater 4", "Operation Theater 5"};

    int tFlag = 0;
    String timeFlag = "10:10", endTime = "21:00";
    ArrayList<OTBookingModel> sList;
    ArrayAdapter<OTBookingModel> otListAdapter;
    String storeOTId ;

    int currentDate;
    long startTimeStamp, endTimeStamp;
    int sday, smonth, syear, shours, smin;
    int ehours, emin;
    LinearLayout llContainer;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static Fragment newInstance(Activity context) {
        OTBooking otBooking=new OTBooking();
        AppAnimation.setSlideAnimation(context,otBooking,Gravity.BOTTOM);
        return otBooking;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_ot_booking, container, false);
        onActionBack(v, "OT Booking");
        InitUI(v);

        return v;
    }

    private void InitUI(View v) {

        llContainer = (LinearLayout) v.findViewById(R.id.container_main);
        OTview = (TextView) v.findViewById(R.id.tv_OT_selectOT);
        tvViewDate = (TextView) v.findViewById(R.id.tv_OT_view_date);
        tvSTime = (TextView) v.findViewById(R.id.tv_OT_View_sTime);
        tvETime = (TextView) v.findViewById(R.id.tv_OT_View_eTime);
        etReason = (EditText) v.findViewById(R.id.et_OT_View_reason);

        SpinnerOTId = (LinearLayout) v.findViewById(R.id.ll_ot_select_OT);
        StartTime = (LinearLayout) v.findViewById(R.id.ll_ot_selectStartTime);
        EndTime = (LinearLayout) v.findViewById(R.id.ll_ot_selectEndTime);
        Date = (LinearLayout) v.findViewById(R.id.ll_ot_select_date);
        Submit = (Button) v.findViewById(R.id.btn_submit_button);

        Submit.setOnClickListener(this);
        SpinnerOTId.setOnClickListener(this);
        Date.setOnClickListener(this);
        StartTime.setOnClickListener(this);
        EndTime.setOnClickListener(this);

        getDattvETime();
        executeAssignTask(LIST_OF_OT);
    }


    public void getDattvETime() {
        String date, time;
        final Calendar cal = Calendar.getInstance();
        syear = cal.get(Calendar.YEAR);
        smonth = cal.get(Calendar.MONTH);
        sday = cal.get(Calendar.DAY_OF_MONTH);

        shours = cal.get(Calendar.HOUR_OF_DAY);
        smin = cal.get(Calendar.MINUTE);
        ehours = cal.get(Calendar.HOUR_OF_DAY);
        emin = cal.get(Calendar.MINUTE);

        date = DateTimeUtility.convertDate(sday, smonth, syear);
        currentDate = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));

        startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
        endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
        tvViewDate.setText(date);

        time = new SimpleDateFormat("h:mm a").format(cal.getTime());

        tvSTime.setText(time);
        tvETime.setText(time);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_button:
                if (checkValidation()) {
                    hideKeybord(getActivity());
                    executeAssignTask(REQUEST_OT_BOOKING);
                }
                break;
            case R.id.ll_ot_select_OT:
                showOTBookingPopup();
                break;
            case R.id.ll_ot_selectStartTime:
                TimePickerDialog.newInstance(OTBooking.this,false,TimePickerDialog.TIME_START,shours,smin)
                        .show(getFragmentManager(),"timePickerDialog");
                break;
            case R.id.ll_ot_selectEndTime:
                TimePickerDialog.newInstance(OTBooking.this,false,TimePickerDialog.TIME_END,ehours,emin)
                        .show(getFragmentManager(),"timePickerDialog");
                break;
            case R.id.ll_ot_select_date:
                DatePickerDialog.newInstance(OTBooking.this,true,sday,smonth,syear)
                        .show(getFragmentManager(),"datePickerDialog");
                break;

        }
    }


    public void executeAssignTask(int task) {
        if (isOnline()) {
            switch (task) {
                case LIST_OF_OT:
                    assignTask = new AssignTask(getActivity(), task, etReason.getText().toString());
                    assignTask.execute(AppSettings.ListofOT);
                    break;
                case REQUEST_OT_BOOKING:
                    assignTask = new AssignTask(getActivity(), task, etReason.getText().toString());
                    assignTask.execute(AppSettings.RequestOTBooking);
                    break;
            }
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (storeOTId==null) {
            Toast.makeText(getActivity(), "Please select OT", Toast.LENGTH_SHORT)
                    .show();
            showOTBookingPopup();
            return false;

        }
        if (startTimeStamp > endTimeStamp) {
            Toast.makeText(getActivity(), "Booking start time must be less than end time", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }
        if (endTimeStamp < startTimeStamp) {
            Toast.makeText(getActivity(), "Booking end time must be grater than start time", Toast.LENGTH_SHORT)
                    .show();

            return false;
        }
        if (!Validation.isEmpty(etReason, true)) {
            return false;
        }

        return ret;
    }


    private class AssignTask extends AsyncTask<String, Void, OTBookingModel> {

        private final String reason;
        Context activity;
        int task;

        public AssignTask(Activity activity, int task, String reason) {
            onAttach(activity);
            this.task = task;
            this.reason = reason;
        }

        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            switch (task) {
                case LIST_OF_OT:
                    showProgressDialog(activity, "Getting Data, Keep patience...", Gravity.CENTER);
                    break;
                case REQUEST_OT_BOOKING:
                    showProgressDialog(activity, "Booking Request Processing...", Gravity.CENTER);
                    break;
            }
        }

        @Override
        protected OTBookingModel doInBackground(String... urls) {
            String response = "error";
            OTBookingModel result = null;
//            List<BasicNameValuePair> params = new ArrayList<>();
            JsonParser jsonParser = new JsonParser(getActivity());
            switch (task) {
                case LIST_OF_OT:
                    if (activity != null) {
//                        params.add(new BasicNameValuePair("Date", DateTimeUtility.viewFormatDate("yyyy-MM-dd"))); //yyyy-MM-dd
                        response= ApiCall.POST(urls[0], RequestBuilder.setDate(DateTimeUtility.viewFormatDate("yyyy-MM-dd")));
                        result = jsonParser.getOTListDetail(response);
                    }
                    break;
                case REQUEST_OT_BOOKING:
                    if (activity != null) {
//                        params.add(new BasicNameValuePair("OTId", convert(storeOTId)));
//                        params.add(new BasicNameValuePair("DoctorId", convert(Doctor.getDoctorId(activity))));
//                        params.add(new BasicNameValuePair("FromDate", convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear))));
//                        params.add(new BasicNameValuePair("ToDate", convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear))));
//                        params.add(new BasicNameValuePair("FromTime", convert(DateTimeUtility.convertTime(shours, smin))));
//                        params.add(new BasicNameValuePair("ToTime", convert(DateTimeUtility.convertTime(ehours, emin))));
//                        params.add(new BasicNameValuePair("Reason", convert(reason)));
                        response= ApiCall.POST(urls[0], RequestBuilder.setOtBooking(convert(storeOTId),
                                convert(Doctor.getDoctorId(activity)),
                                convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear)),
                                convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear)),
                                convert(DateTimeUtility.convertTime(shours, smin)),
                                convert(DateTimeUtility.convertTime(ehours, emin)),
                                convert(reason)));
                        result = jsonParser.getOTBookingRequestDetail(response);
                    }
                    break;
            }



//            response = ob.makeConnection(urls[0], HttpReq.GET, params);
//            switch (task) {
//                case LIST_OF_OT:
//                    result = jsonParser.getOTListDetail(response);
//                    break;
//                case REQUEST_OT_BOOKING:
//                    result = jsonParser.getOTBookingRequestDetail(response);
//                    break;
//            }
            return result;
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(OTBookingModel result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    switch (task) {
                        case LIST_OF_OT:
                            if (result.getOutput().equals("success")) {
                                sList = result.getList();
                                otListAdapter = new OTListAdapter(getActivity(), R.layout.slot_opd_history, sList);
                                showFadInAnimation(llContainer);
//                                llContainer.setVisibility(View.VISIBLE);
                            } else if (result.getOutput().equals("failure")) {
                                showPopUpDialog(getActivity(), result.getOutputError(), PopUpDialog.DEFAULT);

                            } else {
                                showPopUpDialog(getActivity(), result.getOutput(), PopUpDialog.DEFAULT);
                            }
                            break;
                        case REQUEST_OT_BOOKING:
                            if (result.getOutput().equals("success")) {
                                showPopUpDialog(getActivity(), result.getOutputError(), PopUpDialog.DEFAULT);
                            } else if (result.getOutput().equals("failure")) {
                                showPopUpDialog(getActivity(), result.getOutputError(), PopUpDialog.DEFAULT);
                            } else {
                                showPopUpDialog(getActivity(), result.getOutput(), PopUpDialog.DEFAULT);
                            }
                            break;
                    }


                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            switch (task) {
                                case LIST_OF_OT:
                                    executeAssignTask(LIST_OF_OT);
                                    break;
                                case REQUEST_OT_BOOKING:
                                    if (checkValidation()) {
                                        executeAssignTask(REQUEST_OT_BOOKING);
                                    }
                                    break;
                            }
                        }
                    });
                }
            }

        }
    }

    public void showOTBookingPopup() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alertbox_otlist, null);
        dialogBuilder.setView(v);
        ListView lvMain = (ListView) v.findViewById(R.id.dialoglist);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (sList != null) {
                    dialog.dismiss();
                    storeOTId = sList.get(position).getOTId();
                    OTview.setText(sList.get(position).getOTName());
                }
            }
        });
        if (otListAdapter != null)
            lvMain.setAdapter(otListAdapter);
        dialog = dialogBuilder.create();
        dialog.show();
    }


    @Override
    public void onSelectDateClick(DialogFragment dialog, int day, int month, int year) {
        String date = DateTimeUtility.convertDate(day, month, year);
        tvViewDate.setText(date);
        syear = year;
        smonth = month;
        sday = day;
    }

//    @SuppressLint("ValidFragment")
//    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Calendar calendar = Calendar.getInstance();
//            DatePickerDialog dpd;
//            dpd = new DatePickerDialog(getActivity(), this, syear, smonth, sday);
//
//
//            // Set the DatePicker minimum date selection to current date
//            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
//            // dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way
//            // to get the current day
//
//            // //Add 6 days with current date
//            // calendar.add(Calendar.DAY_OF_MONTH,6);
//            //
//            // //Set the maximum date to select from DatePickerDialog
//            // dp.setMaxDate(calendar.getTimeInMillis());
//
//            return dpd;
//        }
//
//        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
//            populateSetDate(yy, mm, dd);
////			storeTimeStamp(yy,mm,dd);
//
//
//        }
//
//
//        public void populateSetDate(int year, int month, int day) {
//            String date = DateTimeUtility.convertDate(day, month, year);
//            tvViewDate.setText(date);
//            syear = year;
//            smonth = month;
//            sday = day;
//        }
//
//    }

    @Override
    public void onSelectDateFlagClick(DialogFragment dialog, int timeType, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);

        String format = new SimpleDateFormat("h:mm a").format(c.getTime());

        if (timeType == TimePickerDialog.TIME_START) {
            tvSTime.setText(format);
            shours = hourOfDay;
            smin = minute;
            startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));

        } else {
            tvETime.setText(format);
            ehours = hourOfDay;
            emin = minute;
            endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, ehours, emin));

        }
    }



//    @SuppressLint("ValidFragment")
//    public class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            //final Calendar calendar = Calendar.getInstance();
//            TimePickerDialog tp;
//            if (status == START_TIME) {
//                tp = new TimePickerDialog(getActivity(), this, shours, smin, true);
//            } else {
//                tp = new TimePickerDialog(getActivity(), this, ehours, emin, true);
//            }
//            return tp;
//        }
//
//        @Override
//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            Calendar c = Calendar.getInstance();
//            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            c.set(Calendar.MINUTE, minute);
//
//            String format = new SimpleDateFormat("h:mm a").format(c.getTime());
//
//            if (status == START_TIME) {
//                tvSTime.setText(format);
//                shours = hourOfDay;
//                smin = minute;
//                startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
//
//            } else {
//                tvETime.setText(format);
//                ehours = hourOfDay;
//                emin = minute;
//                endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, ehours, emin));
//
//            }
//        }
//    }
}