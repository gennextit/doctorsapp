package com.sarvodaya.doctorsapp.doctor.ipd;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.AppWebView;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.model.IPDModel;
import com.sarvodaya.doctorsapp.model.adapter.IPDHistoryAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class IPDPatientHistory extends CompactFragment {
    ListView lvMain;
    String doctorId,doctorName,patientName,IPNo;
    AssignTask assignTask;
    private ArrayList<IPDModel> listMain;
    private TextView tvPatientName;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public void setDoctorDetail(String doctorName, String doctorId, String patientName, String ipNo) {
        this.doctorId=doctorId;
        this.doctorName=doctorName;
        this.patientName=patientName;
        this.IPNo=ipNo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_ipd_history, container, false);
        onActionBack(v,"IPD Patient History" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvPatientName = (TextView) v.findViewById(R.id.tv_ipd_dr_Name);
        lvMain = (ListView) v.findViewById(R.id.lv_school);
        if(patientName!=null)
            tvPatientName.setText(patientName);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(listMain!=null){
                    String url=listMain.get(position).getUrl();
                    if(!url.equals("")){
                        String finalUrl= null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            finalUrl = Html.fromHtml(url,Html.FROM_HTML_MODE_LEGACY).toString();
                        }else{
                            finalUrl = Html.fromHtml(url).toString();
                        }
                        AppWebView appWebView =AppWebView.newInstance(listMain.get(position).getTestName() ,finalUrl , AppWebView.URL);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.add(android.R.id.content, appWebView, "appWebView");
                        transaction.addToBackStack("appWebView");
                        transaction.commit();

//                        String filename =  "Some Notes.pdf";
//                        final ProgressDialog dialog = ProgressDialog.show(getActivity(),
//                                "Downloading PDF",
//                                "Donwloading " + filename, true, true);
//
//                        HttpReq.downloadPdf(getActivity(),
//                                finalUrl,
//                                filename,
//                                new HttpReq.OnTaskCompleted() {
//                                    @Override
//                                    public void onSuccess() {
//                                        dialog.dismiss();
//                                    }
//
//                                    @Override
//                                    public void onError(String msg) {
//                                        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
//                                        dialog.dismiss();
//                                    }
//                                });
//                        new HttpTask().execute(finalUrl);
//                        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(url.replace("&amp;", "&")));
//                        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(Html.fromHtml(url).toString()));
//                        startActivity(viewIntent);
                    }else {
                        showPopUpDialog(getActivity(),"Lab report not available for test\n"+listMain.get(position).getTestName(),PopUpDialog.DEFAULT);
                    }
                }
            }
        });
        if(listMain!=null){
            IPDHistoryAdapter ipdHistoryAdapter = new IPDHistoryAdapter(getActivity(), R.layout.slot_ipd, listMain);
            lvMain.setAdapter(ipdHistoryAdapter);
        }else{
            executeAssignTask();
        }
    }


    public void executeAssignTask() {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(), IPNo);
            assignTask.execute(AppSettings.GetIPDPatientDetails);
        } else {
            showInternetAlertBox(getActivity());
        }


    }

    public void setPatientList(ArrayList<IPDModel> listMain) {
        this.listMain=listMain;
    }


    private class AssignTask extends AsyncTask<String, Void, IPDModel> {

        Activity activity;
        String ipNo;

        public AssignTask(Activity activity, String ipNo) {
            onAttach(activity);
            this.ipNo = ipNo;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Getting Data, Keep patience...", Gravity.CENTER);
        }

        @Override
        protected IPDModel doInBackground(String... urls) {
            String response = "error";

            JsonParser jsonParser = new JsonParser(getActivity());
            response=ApiCall.POST(urls[0], RequestBuilder.setIpNo(ipNo));
            return jsonParser.getIPDPatientDetail(response);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(IPDModel result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        listMain=result.getList();
                        IPDHistoryAdapter ipdHistoryAdapter = new IPDHistoryAdapter(getActivity(), R.layout.slot_ipd, listMain);
                        lvMain.setAdapter(ipdHistoryAdapter);

                    } else if (result.getOutput().equals("failure")) {
                        ArrayList<String> errorList = new ArrayList<>();
                        errorList.add("No patient history available");
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_error,
                                R.id.tv_message, errorList);
                        lvMain.setAdapter(adapter);
                    } else {
                        showPopUpDialog(activity, result.getOutput(), PopUpDialog.DEFAULT);
                    }

                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask();
                        }
                    });
                }
            }

        }
    }
}