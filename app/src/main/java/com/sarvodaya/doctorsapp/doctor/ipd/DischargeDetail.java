package com.sarvodaya.doctorsapp.doctor.ipd;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;
import com.sarvodaya.doctorsapp.util.Utility;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by Abhijit on 01-Oct-16.
 */

public class DischargeDetail extends CompactFragment implements View.OnClickListener {
    private static final int TASK_GETIPDD = 1, TASK_APPROVE_REJECT = 2;
    Button btnApprove, btnReject, btnEditDischargeSummary;
    String patientName, ipNo;
    AssignTask assignTask;
    private String pdfUrl;
    WebView webView;
    ImageView ivDischargeSummary;
    private DownloadManager mgr = null;
    public String fileName;
    private String finalUrl;
    private String doctorId;
    private TextView tvDischargeSummary;
    private String summaryStatus;
    private PermissionListener onPermissionListener;


    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static DischargeDetail newInstance(String doctorId, String patientName, String ipNo, String summaryStatus) {
        DischargeDetail fragment = new DischargeDetail();
        fragment.patientName = patientName;
        fragment.summaryStatus = summaryStatus;
        fragment.doctorId = doctorId;
        fragment.ipNo = ipNo;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_discharge_detail, container, false);
        onActionBack(v, patientName);
        InitUI(v);
        setCallPermission();
        return v;
    }

    private void InitUI(View v) {
        tvDischargeSummary = (TextView) v.findViewById(R.id.tv_discharge_summary);
//        LinearLayout llDS = (LinearLayout) v.findViewById(R.id.ll_discharge_option);
        btnApprove = (Button) v.findViewById(R.id.btn_approve);
        btnReject = (Button) v.findViewById(R.id.btn_reject);
        btnEditDischargeSummary = (Button) v.findViewById(R.id.btn_edit_summary);
        LinearLayout llDischargeSummary = (LinearLayout) v.findViewById(R.id.ll_discharge_detail);
        webView = (WebView) v.findViewById(R.id.webView_browser);
        mgr = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
        getActivity().registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        getActivity().registerReceiver(onNotificationClick,
                new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));

        btnApprove.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        btnEditDischargeSummary.setOnClickListener(this);
        llDischargeSummary.setOnClickListener(this);

//        if (summaryStatus != null && summaryStatus.equalsIgnoreCase("Approved")) {
//            llDS.setVisibility(View.GONE);
//            executeAssignTask(TASK_GETIPDD, null, null);
//        } else {
//            llDS.setVisibility(View.VISIBLE);
//        }

    }

    private void setCallPermission() {
        onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                executeAssignTask(TASK_GETIPDD, null, null);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_discharge_detail:
//                showPDF(fileName);
                new TedPermission(getActivity())
                        .setPermissionListener(onPermissionListener)
                        .setDeniedMessage(getString(R.string.permission_denied_explanation))
                        .setRationaleMessage(getString(R.string.permission_rationale_storage))
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
                        .check();
                break;
            case R.id.btn_approve:
                executeAssignTask(TASK_APPROVE_REJECT, "A", "");
                break;
            case R.id.btn_reject:
                showMessageAlert(getActivity());
                break;
            case R.id.btn_edit_summary:
                Intent intent = new Intent(getActivity(), EditDischargeSummaryActivity.class);
                intent.putExtra("doctorId", doctorId);
                intent.putExtra("ipNo", ipNo);
                startActivity(intent);
//                addFragment(EditDischargeSummary.newInstance(),"editDischargeSummary");
                break;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(onComplete);
        getActivity().unregisterReceiver(onNotificationClick);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
//            findViewById(R.id.start).setEnabled(true);
            hideProgressDialog();

            showPDF(fileName);
//            Toast.makeText(ctxt, "Completed!", Toast.LENGTH_LONG).show();
        }
    };

    BroadcastReceiver onNotificationClick = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
//            Toast.makeText(ctxt, "Ummmm...hi!", Toast.LENGTH_LONG).show();
        }
    };

    private void showPDF(String fileName) {
        if (fileName != null) {
            File documentOutput = getDocumentOutput(fileName);
            if (documentOutput.exists()) {
                Uri path;
                Intent pdfIntent = new Intent(Intent.ACTION_VIEW);

                path = getUriFromFile(documentOutput);

                pdfIntent.setDataAndType(path, "application/pdf");
                pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    startActivity(pdfIntent);
                } catch (ActivityNotFoundException e) {
                    showPopUpDialog(getActivity(), "No Application available to view PDF", PopUpDialog.PDF);
                }
            } else {
                if (finalUrl != null) {
                    showProgressDialog(getActivity(), "Processing Request please wait...", Gravity.CENTER);
                    startWebView(finalUrl);
                }
            }
        } else {
            showPopUpDialog(getActivity(), "Discharge summary pdf not available", PopUpDialog.DEFAULT);
        }
    }

    private Uri getUriFromFile(File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }

    public File getDocumentOutput(String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            Toast.makeText(getActivity(), "Invalid file name", Toast.LENGTH_SHORT).show();
        }

        File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        return new File(dir, fileName);
    }

    public void executeAssignTask(int task, String type, String remarks) {
        if (isOnline()) {
            if (task == TASK_GETIPDD) {
                assignTask = new AssignTask(getActivity(), doctorId, ipNo, null, null, TASK_GETIPDD);
                assignTask.execute(AppSettings.GETIPDDischargeDetails);
            } else {
                assignTask = new AssignTask(getActivity(), doctorId, ipNo, type, remarks, TASK_APPROVE_REJECT);
                assignTask.execute(AppSettings.ApproveRejectDischargeSummary);
            }
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    private class AssignTask extends AsyncTask<String, Void, String[]> {
        private int task;
        private final String DRID;
        private final String IPNO;
        Context activity;
        private String type, remarks;

        public AssignTask(Activity activity, String DRID, String IPNO, String type, String remarks, int task) {
            onAttach(activity);
            this.DRID = DRID;
            this.task = task;
            this.IPNO = IPNO;
            this.type = type;
            this.remarks = remarks;

        }

        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing Request please wait...", Gravity.CENTER);
        }

        @Override
        protected String[] doInBackground(String... urls) {
            String response = "error";
            JsonParser jsonParser = new JsonParser(activity);
            if (activity != null) {
                if (task == TASK_GETIPDD) {
//                    params.add(new BasicNameValuePair("Doctor_ID", convert(DRID)));
//                    params.add(new BasicNameValuePair("IPNO", convert(IPNO)));
//                    response = ob.makeConnection(urls[0], HttpReq.GET, params);
                    response = ApiCall.POST(urls[0], RequestBuilder.GETDD(convert(DRID), convert(IPNO)));
                    return jsonParser.getDischargeDetail(response);
                } else {
//                    params.add(new BasicNameValuePair("DoctorId", convert(DRID)));
//                    params.add(new BasicNameValuePair("IPNo", convert(IPNO)));
//                    params.add(new BasicNameValuePair("Type", convert(type)));
//                    params.add(new BasicNameValuePair("Remark", convert(remarks)));
//                    response = ob.makeConnection(urls[0], HttpReq.GET, params);
                    response = ApiCall.POST(urls[0], RequestBuilder.dischargeDetail(convert(DRID), convert(IPNO), convert(type), convert(remarks)));
                    return jsonParser.getApproveRejectInfo(response);
                }
            } else {
                return null;
            }

//            if (task == TASK_GETIPDD) {
//                response = ob.makeConnection(urls[0], HttpReq.GET, params);
//                return jsonParser.getDischargeDetail(response);
//            } else {
//                response = ob.makeConnection(urls[0], HttpReq.GET, params);
//                return jsonParser.getApproveRejectInfo(response);
//            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String[] result) {
            if (activity != null) {
                if (result != null) {
                    if (result[0].equals("success")) {
                        if (task == TASK_GETIPDD) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                finalUrl = Html.fromHtml(result[1], Html.FROM_HTML_MODE_COMPACT).toString();
                            } else {
                                finalUrl = Html.fromHtml(result[1]).toString();
                            }
                            startWebView(finalUrl);
                            tvDischargeSummary.setText(getSt(R.string.tap_to_view_discharge_summary));
                        } else {
                            hideProgressDialog();
                            showPopUpDialog(getActivity(), result[1], PopUpDialog.DEFAULT);
                        }
                    } else if (result[0].equals("failure")) {
                        hideProgressDialog();
                        if (task == TASK_GETIPDD) {
                            tvDischargeSummary.setText(getSt(R.string.discharge_summary_not_available));
                        }
                        showPopUpDialog(getActivity(), result[1], PopUpDialog.DEFAULT);
                    } else {
                        hideProgressDialog();
                        if (task == TASK_GETIPDD) {
                            tvDischargeSummary.setText(getSt(R.string.discharge_summary_not_available));
                        }
                        showPopUpDialog(getActivity(), result[0], PopUpDialog.DEFAULT);
                    }

                } else {
                    hideProgressDialog();
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            if (task == TASK_GETIPDD) {
                                executeAssignTask(TASK_GETIPDD, null, null);
                            } else {
                                executeAssignTask(TASK_APPROVE_REJECT, type, remarks);
                            }

                        }
                    });
                }
            }

        }
    }


//    private void startWebView(String url) {
//        webView.setWebViewClient(new WebViewClient() {
//
//            // If you will not use this method url links are opeen in new brower
//            // not in webview
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//
//                view.loadUrl(url);
//
//                return true;
//            }
//
//            // Show loader on url load
//            public void onLoadResource(WebView view, String url) {
//
//            }
//
//            public void onPageFinished(WebView view, String url) {
//
//            }
//
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                super.onReceivedError(view, errorCode, description, failingUrl);
////				Toast.makeText(getActivity(), "Your Internet Connection May not be active Or " + errorCode , Toast.LENGTH_LONG).show();
//                hideProgressDialog();
//                Button retry = showBaseServerErrorAlertBox(description + "\n" + errorCode);
//                retry.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        hideBaseServerErrorAlertBox();
//                        if (finalUrl != null) {
//                            startWebView(finalUrl);
//                        } else {
//                            executeAssignTask(TASK_GETIPDD, null, null);
//                        }
//                    }
//                });
//            }
//
//        });
//
//        // Javascript inabled on webview
//        webView.getSettings().setJavaScriptEnabled(true);
//
//        // Other webview options
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//        //webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        //webView.setScrollbarFadingEnabled(false);
//        webView.loadUrl(url);
//
//        webView.setDownloadListener(new DownloadListener() {
//            public void onDownloadStart(String url, String userAgent,
//                                        String contentDisposition, String mimetype,
//                                        long contentLength) {
//                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//
//                request.allowScanningByMediaScanner();
//                fileName = Utility.getNameWithTimeStamp(patientName.replaceAll("[^A-Za-z0-9]", ""));
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
//                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
//                if (activity != null) {
//                    DownloadManager dm = (DownloadManager) activity.getSystemService(DOWNLOAD_SERVICE);
//                    dm.enqueue(request);// check nullpointer
//                }
//
////				Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT); //This is important!
////				intent.addCategory(Intent.CATEGORY_OPENABLE); //CATEGORY.OPENABLE
////				intent.setType("*/*");//any application,any extension
////                Toast.makeText(getActivity(), "Downloading File", //To notify the Client that the file is being downloaded
////                        Toast.LENGTH_LONG).show();
//
//            }
//        });
//    }

    private void startWebView(final String url) {
        showProgressDialog(getActivity(), "Processing please wait...", Gravity.CENTER);
        webView.setWebViewClient(new WebViewClient() {

            // If you will not use this method url links are opeen in new brower
            // not in webview

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return true;
            }

            // Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                hideProgressDialog();
                Button retry = showBaseServerErrorAlertBox(error.getDescription() + "\n" + error.getErrorCode());
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideBaseServerErrorAlertBox();
                        startWebView(url);
                    }
                });
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                hideProgressDialog();
                Button retry = showBaseServerErrorAlertBox(description + "\n" + errorCode);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideBaseServerErrorAlertBox();
                        startWebView(url);
                    }
                });
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        //webView.setScrollbarFadingEnabled(false);

        webView.loadUrl(url);

        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                request.allowScanningByMediaScanner();
                fileName = Utility.getNameWithTimeStamp(patientName.replaceAll("[^A-Za-z0-9]", ""));
//				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN); //Notify client once download is completed!

//				File pdfFile = Utility.getExternalDirectory(fileName);
//				if (cacheFileDoesNotExist()) {
//					createCacheFile();
//				}
//				Uri path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName(), pdfFile);
//				request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                File cameraOutput = getDocumentOutput(fileName);
//				Uri path = getUriFromFile(cameraOutput);
                request.setDestinationUri(Uri.fromFile(cameraOutput));
                DownloadManager dm = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);// check nullpointer
            }
        });

    }

    public void showMessageAlert(Activity Act) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = Act.getLayoutInflater();

        View v = inflater.inflate(R.layout.custorm_reject, null);
        dialogBuilder.setView(v);
        Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
        final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                hideKeybord(getActivity());
                executeAssignTask(TASK_APPROVE_REJECT, "R", etReview.getText().toString());

            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }


}
