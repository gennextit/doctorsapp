package com.sarvodaya.doctorsapp.doctor.ipd;

import android.content.Intent;
import android.os.Bundle;

import com.sarvodaya.doctorsapp.BaseActivity;
import com.sarvodaya.doctorsapp.R;

/**
 * Created by Admin on 7/11/2017.
 */

public class EditDischargeSummaryActivity extends BaseActivity {


    private String ipNo;
    private String doctorId;
    private EditDischargeSummary editDischargeSummary;
    private boolean mDataModified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discharge_summary);
        Intent intent=getIntent();
        if(intent!=null){
            doctorId=intent.getStringExtra("doctorId");
            ipNo=intent.getStringExtra("ipNo");
        }
        editDischargeSummary=EditDischargeSummary.newInstance(doctorId,ipNo);
        addFragmentWithoutBackstack(editDischargeSummary,R.id.container,"editDischargeSummary");
    }

    public void setmDataModified(boolean mDataModified) {
        this.mDataModified = mDataModified;
    }

    public boolean getmDataModified() {
       return mDataModified;
    }



    @Override
    public void onBackPressed() {
        if(mDataModified){
            editDischargeSummary.onActionBack();
        }else{
            super.onBackPressed();
        }
    }
}
