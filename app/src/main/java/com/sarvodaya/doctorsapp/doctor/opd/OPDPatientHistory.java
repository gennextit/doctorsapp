package com.sarvodaya.doctorsapp.doctor.opd;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.dialog.RecommendDialog;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.model.OPDModel;
import com.sarvodaya.doctorsapp.model.RecommendModel;
import com.sarvodaya.doctorsapp.model.adapter.OPDHistoryAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;


import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class OPDPatientHistory extends CompactFragment implements RecommendDialog.RecommendListener{
    String appNo = null, patientName = null, appId = null;
    ListView lvMain;

    AssignTask assignTask;
    private ArrayList<OPDModel> opdList;
    private TextView tvPatient;
    private ArrayList<RecommendModel> rcList,rcSelectList;
    private String sltRecIds;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void setBasicDetail(String appNo, String patientName, String appId) {
        this.appNo = appNo;
        this.appId = appId;
        this.patientName = patientName;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_opd_detail, container, false);
        onActionBack(v,"OPD Patient History" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvPatient = (TextView) v.findViewById(R.id.tv_opdHistory_App_Name);
        Button btnRecommend = (Button) v.findViewById(R.id.btn_recommend);
        lvMain = (ListView) v.findViewById(R.id.lv_school);
        if(patientName!=null)
        tvPatient.setText(patientName);
        executeAssignTask();

        btnRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(RecommendFragment.newInstance(),"recommendFragment");
            }
        });
    }

    public void executeAssignTask() {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(), appId);
            assignTask.execute(AppSettings.GetOPDPatientDetails);
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    @Override
    public void onSubmitClick(RecommendDialog modeOfPaymentDialog, ArrayList<RecommendModel> checkedList, ArrayList<RecommendModel> originalList) {
        this.rcList=originalList;
        this.rcSelectList = checkedList;
        String sltDay = "",sltDayIds = "";
        if (rcSelectList != null && rcSelectList.size() != 0) {
            for (RecommendModel model : rcSelectList) {
                sltDay += model.getRecName() + ",";
                sltDayIds+=model.getRecName() +",";
            }
            sltRecIds= sltDay.substring(0, sltDay.length() - 1);
        }else{
            this.sltRecIds=null;
        }
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, OPDModel> {

        Activity activity;
        String appId;

        public AssignTask(Activity activity, String appId) {
            onAttach(activity);
            this.appId = appId;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Getting Data, Keep patience...", Gravity.CENTER);
        }

        @Override
        protected OPDModel doInBackground(String... urls) {
            String response = "error";
//            if (activity != null) {
//                params.add(new BasicNameValuePair("App_ID", appId));
//            }

            JsonParser jsonParser = new JsonParser(getActivity());

//            response = ob.makeConnection(urls[0], HttpReq.GET, params);
            response= ApiCall.POST(urls[0], RequestBuilder.setAppId(appId));
            return  jsonParser.getOPDPatientHistory(response);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(OPDModel result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        OPDHistoryAdapter opdHistoryAdapter = new OPDHistoryAdapter(getActivity(), R.layout.slot_opd_history, result.getList());
                        lvMain.setAdapter(opdHistoryAdapter);

                    } else if (result.getOutput().equals("failure")) {
                        ArrayList<String> errorList = new ArrayList<>();
                        errorList.add("No patient history available");
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_error,
                                R.id.tv_message, errorList);
                        lvMain.setAdapter(adapter);
                    } else {
                        showPopUpDialog(activity, result.getOutput(), PopUpDialog.DEFAULT);
                    }

                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask();
                        }
                    });
                }
            }

        }
    }
}