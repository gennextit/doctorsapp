package com.sarvodaya.doctorsapp.doctor;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.Utility;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static android.content.Context.DOWNLOAD_SERVICE;


public class AppWebView extends CompactFragment {
	String BrowserSetting = "";
	// common function
	WebView webView;
	ProgressBar progressBar;
	FragmentManager mannager;
	String webUrl;
	int URLTYPE=0;
	String browserName;
	public static int URL=0, PDF=1;
	public String fileName;
	private PermissionListener onPermissionListener;

	private static final String ASSET_NAME = "pdf-sample.pdf";
	public static final int BUFFER_SIZE = 1024;
	private File cacheFile = null;

//	public void setUrl(String browserName,String url,int urlType) {
//		this.browserName=browserName;
//		this.webUrl=url;
//		this.URLTYPE=urlType;
//	}


	public static AppWebView newInstance(String browserName,String url,int urlType) {
		AppWebView appWebView=new AppWebView();
		appWebView.browserName=browserName;
		appWebView.webUrl=url;
		appWebView.URLTYPE=urlType;
		return appWebView;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.app_web_view_transparent, container, false);
		mannager = getFragmentManager();
//		setActionBarOption(v);
		initUi(v);
		deleteFiles();
		return v;
	}


	private void initUi(View v) {
		// TODO Auto-generated method stub
		webView = (WebView) v.findViewById(R.id.webView_browser);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		Button ViewDoc = (Button) v.findViewById(R.id.btn_submit_button);

		ViewDoc.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(fileName!=null){
					File pdfFile = Utility.getExternalDirectory(fileName);
					if(pdfFile.exists()){
						Uri path = Uri.fromFile(pdfFile);
						Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
						pdfIntent.setDataAndType(path, "application/pdf");
						pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						try {
							startActivity(pdfIntent);
						} catch (ActivityNotFoundException e) {
							Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
						}
					}else{
						Toast.makeText(getActivity(), "Please wait while downloading is completed", Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(getActivity(), "Please wait...", Toast.LENGTH_SHORT).show();
				}
				mannager.popBackStack();
			}
		});

		webView.setInitialScale(100);
		webView.getSettings().setDisplayZoomControls(true);
		webView.getSettings().setSupportZoom(true);

		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				progressBar.setProgress(progress);
				if (progress == 100) {
					progressBar.setVisibility(View.GONE);
				} else {
					progressBar.setVisibility(View.VISIBLE);
				}
			}
		});
		// Javascript inabled on webview

		setCallPermission();

		new TedPermission(getActivity())
				.setPermissionListener(onPermissionListener)
				.setDeniedMessage(getString(R.string.permission_denied_explanation))
				.setRationaleMessage(getString(R.string.permission_rationale_storage))
				.setGotoSettingButtonText("setting")
				.setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
				.check();

		getActivity().registerReceiver(onComplete,
				new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		getActivity().registerReceiver(onNotificationClick,
				new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));

	}

	private void setCallPermission() {
		onPermissionListener = new PermissionListener() {
			@Override
			public void onPermissionGranted() {
				if (isOnline() == true) {
					if(URLTYPE==URL){
						startWebView(webUrl);
					}else{
						//webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf);
						String Doc="<iframe src='http://docs.google.com/gview?embedded=true&url="+webUrl+"' width='100%' height='100%' style='border: none;'></iframe>";
						startWebView(Doc);
					}
				} else {
					showInternetAlertBox(getActivity());
				}
			}

			@Override
			public void onPermissionDenied(ArrayList<String> deniedPermissions) {
				Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
			}
		};
	}

	BroadcastReceiver onComplete=new BroadcastReceiver() {
		public void onReceive(Context ctxt, Intent intent) {
//            findViewById(R.id.start).setEnabled(true);
			hideProgressDialog();

			showPDF(fileName);
//            Toast.makeText(ctxt, "Completed!", Toast.LENGTH_LONG).show();
		}
	};
	BroadcastReceiver onNotificationClick=new BroadcastReceiver() {
		public void onReceive(Context ctxt, Intent intent) {
//            Toast.makeText(ctxt, "Ummmm...hi!", Toast.LENGTH_LONG).show();
		}
	};

	private void showPDF(String fileName) {
//		File pdfFile = Utility.getExternalDirectory(fileName);
		File documentOutput = getDocumentOutput(fileName);

		if(documentOutput.exists()){
			Uri path ;
			Intent pdfIntent = new Intent(Intent.ACTION_VIEW);

//			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//				String packageNa = getActivity().getPackageName();
////				path = FileProvider.getUriForFile(getActivity(), packageNa + ".provider", new File(fileName));
////
////				List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(pdfIntent, PackageManager.MATCH_DEFAULT_ONLY);
////				for (ResolveInfo resolveInfo : resInfoList) {
////					String packageName = resolveInfo.activityInfo.packageName;
////					getActivity().grantUriPermission(packageName, path, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
////				}
//				if (cacheFileDoesNotExist()) {
//					createCacheFile();
//				}
//				path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName(), pdfFile);
//				pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//			}else {
//				path = Uri.fromFile(pdfFile);
//			}
			path = getUriFromFile(documentOutput);

			pdfIntent.setDataAndType(path, "application/pdf");
			pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			try {
				startActivity(pdfIntent);
				mannager.popBackStack();
			} catch (ActivityNotFoundException e) {
//                Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
				showPopUpDialog(getActivity(), "No Application available to view PDF", PopUpDialog.PDF);
			}
		}else{
			mannager.popBackStack();
			Toast.makeText(getActivity(), "File not found in download folder", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean cacheFileDoesNotExist() {
		if (cacheFile == null) {
			cacheFile = new File(getActivity().getCacheDir(), fileName);
		}
		return !cacheFile.exists();
	}

	private void createCacheFile() {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = getActivity().getAssets().open(fileName);
			outputStream = new FileOutputStream(cacheFile);
			byte[] buf = new byte[BUFFER_SIZE];
			int len;
			while ((len = inputStream.read(buf)) > 0) {
				outputStream.write(buf, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(inputStream);
			close(outputStream);
		}
	}

	private void close(@Nullable Closeable closeable) {
		if (closeable == null) {
			return;
		}
		try {
			closeable.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	public class myWebclient extends WebViewClient {
//		@Override
//		public void onPageStarted(WebView view, String url, Bitmap favicon) {
//			super.onPageStarted(view, url, favicon);
//		}
//
//		@Override
//		public boolean shouldOverrideUrlLoading(WebView view, String url) {
//			view.loadUrl(url);
//			return true;
//		}
//
//	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		getActivity().unregisterReceiver(onComplete);
		getActivity().unregisterReceiver(onNotificationClick);
	}


	private void startWebView(String url) {
		showProgressDialog(getActivity(),"Processing please wait...", Gravity.CENTER);
		webView.setWebViewClient(new WebViewClient() {
			
			// If you will not use this method url links are opeen in new brower
			// not in webview

			@SuppressWarnings("deprecation")
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if(URLTYPE==URL){
					view.loadUrl(url);
				}else{
					view.loadData(url, "text/html", "UTF-8");
				}
				return true;
			}

			@TargetApi(Build.VERSION_CODES.N)
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
				if(URLTYPE==URL){
					view.loadUrl(request.getUrl().toString());
				}else{
					view.loadData(request.getUrl().toString(), "text/html", "UTF-8");
				}
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {

			}

			public void onPageFinished(WebView view, String url) {

			}

			@TargetApi(Build.VERSION_CODES.N)
			@Override
			public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
				super.onReceivedError(view, request, error);
				hideProgressDialog();
				Button retry=showBaseServerErrorAlertBox(error.getDescription()+"\n"+error.getErrorCode());
				retry.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						hideBaseServerErrorAlertBox();
						startWebView(webUrl);
					}
				});
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				hideProgressDialog();
				Button retry=showBaseServerErrorAlertBox(description+"\n"+errorCode);
				retry.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						hideBaseServerErrorAlertBox();
						startWebView(webUrl);
					}
				});
			}

		});

		// Javascript inabled on webview
		webView.getSettings().setJavaScriptEnabled(true);

		// Other webview options 
		webView.getSettings().setAllowFileAccess(true);
	    webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		//webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		//webView.setScrollbarFadingEnabled(false); 
		if(URLTYPE==URL){
			webView.loadUrl(url);
        }else{
    		webView.loadData( url , "text/html", "UTF-8");
		}
		webView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent,
										String contentDisposition, String mimetype,
										long contentLength) {
				DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

				request.allowScanningByMediaScanner();
				fileName=Utility.getNameWithTimeStamp(browserName.replaceAll("[^A-Za-z0-9]",""));
//				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN); //Notify client once download is completed!

//				File pdfFile = Utility.getExternalDirectory(fileName);
//				if (cacheFileDoesNotExist()) {
//					createCacheFile();
//				}
//				Uri path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName(), pdfFile);
//				request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
				File cameraOutput = getDocumentOutput(fileName);
//				Uri path = getUriFromFile(cameraOutput);
				request.setDestinationUri(Uri.fromFile(cameraOutput));
				DownloadManager dm = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
				dm.enqueue(request);// check nullpointer
			}
		});

	}
	private Uri getUriFromFile(File docOutput) {
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
			return FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", docOutput);
		} else {
			return Uri.fromFile(docOutput);
		}
	}
	public void deleteFiles() {
		File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);

//		Filedir file = new File(dir);

		if (dir.exists()) {
			String deleteCmd = "rm -r " + dir;
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec(deleteCmd);
			} catch (IOException e) { }
		}
	}

	public File getDocumentOutput(String fileName) {
		if (TextUtils.isEmpty(fileName)) {
			Toast.makeText(getActivity(), "Invalid file name", Toast.LENGTH_SHORT).show();
		}

		File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
		return new File(dir, fileName);
	}
	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		actionbarTitle.setText(browserName);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (webView.canGoBack()) {
					webView.goBack();
				} else {
					// Let the system handle the back button
					mannager.popBackStack();
				}
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}
	
	// Detect when the back button is pressed
	public void onBackPressed() {

		if (webView.canGoBack()) {
			webView.goBack();
		} else {
			// Let the system handle the back button
			mannager.popBackStack();
		}
	}

	public void AlertInternet() {

		new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Not Connected!").setMessage("Please Check Your Internet Connection.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}

				}).show();
		// Toast.makeText(getApplicationContext(),"No Internet Connection
		// found...",Toast.LENGTH_LONG).show();

	}

}

