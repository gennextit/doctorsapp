package com.sarvodaya.doctorsapp.doctor.selfHelp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.DateTimeUtility;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class PostponeAppointments extends CompactFragment {
    TextView tvViewdate;
    Button confirmButton;

    RadioGroup radioOptionGroup;
    RadioButton radioOptionButton;
    RadioButton option1, option2,option3;
    AssignTask assignTask;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_postpone_appointment, container, false);
        onActionBack(v,"Postpone Appointments" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvViewdate = (TextView) v.findViewById(R.id.tv_postpone_viewDate);
        radioOptionGroup = (RadioGroup) v.findViewById(R.id.rg_postpone_radioOption);
        confirmButton = (Button) v.findViewById(R.id.btn_submit_button);
        option1 = (RadioButton) v.findViewById(R.id.rb_postpone_option1);
        option2 = (RadioButton) v.findViewById(R.id.rb_postpone_option2);
        option3 = (RadioButton) v.findViewById(R.id.rb_postpone_option3);
        option1.setChecked(true);

        tvViewdate.setText(DateTimeUtility.viewFormatDate("d MMMM yyyy, EEEE"));

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int option=0;
                // TODO Auto-generated method stub
                int selectedId = radioOptionGroup.getCheckedRadioButtonId();

                radioOptionButton = (RadioButton)v.findViewById(selectedId);
                switch(selectedId){
                    case R.id.rb_postpone_option1 :
                        option=1;
                        break;
                    case R.id.rb_postpone_option2 :
                        option=2;
                        break;
                    case R.id.rb_postpone_option3 :
                        option=3;
                        break;
                }

                executeAssignTask(option);

            }
        });
    }

    public void executeAssignTask(int option) {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(),option);
            assignTask.execute(AppSettings.InformDelayToPatient);
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    private class AssignTask extends AsyncTask<String, Void, String[]> {

        private final int option;
        Activity activity;

        public AssignTask(Activity activity, int option) {
            onAttach(activity);
            this.option = option;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Postpone Request Processing...", Gravity.CENTER);
        }

        @Override
        protected String[] doInBackground(String... urls) {
            String response = "error";
            JsonParser jsonParser = new JsonParser(getActivity());
            if (activity != null) {
//                params.add(new BasicNameValuePair("DoctorId", convert(Doctor.getDoctorId(activity))));
//                params.add(new BasicNameValuePair("option", convert(String.valueOf(option))));
//                response = ob.makeConnection(urls[0], HttpReq.GET, params);
                response= ApiCall.POST(urls[0], RequestBuilder.postponeAppointment(convert(Doctor.getDoctorId(activity)),convert(String.valueOf(option))));
                return jsonParser.getPostponeReqDetail(response);
            }else {
                return null;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String[] result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result[0].equals("success")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else if (result[0].equals("failure")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else {
                        showPopUpDialog(activity, result[0], PopUpDialog.DEFAULT);
                    }

                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask(option);
                        }
                    });
                }
            }

        }
    }
}