package com.sarvodaya.doctorsapp.doctor.ipd;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

/**
 * Created by Abhijit on 01-Oct-16.
 */

public class DischargeSummary extends CompactFragment {
    Button confirmButton;
    String patientName,ipNo;
    TextView tvPatientName;
    EditText etRemarks;

    RadioGroup radioOptionGroup;
    RadioButton radioOptionButton;
    RadioButton option1, option2;

    AssignTask assignTask;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void setDetail(String patientName, String ipNo) {
        this.patientName=patientName;
        this.ipNo=ipNo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_discharge_summary, container, false);
        onActionBack(v,"Discharge Summary" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        tvPatientName = (TextView) v.findViewById(R.id.tv_discharge_patient_name);
        etRemarks = (EditText) v.findViewById(R.id.et_discharge_remarks);
        radioOptionGroup = (RadioGroup) v.findViewById(R.id.rg_discharge_option);
        confirmButton = (Button) v.findViewById(R.id.btn_submit_button);
        option1 = (RadioButton) v.findViewById(R.id.rb_discharge_approve);
        option2 = (RadioButton) v.findViewById(R.id.rb_discharge_reject);
        option1.setChecked(true);

        tvPatientName.setText(patientName);

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String option = null;
                // TODO Auto-generated method stub
                int selectedId = radioOptionGroup.getCheckedRadioButtonId();

                radioOptionButton = (RadioButton)v.findViewById(selectedId);
                switch(selectedId){
                    case R.id.rb_discharge_approve :
                        option="A";
                        break;
                    case R.id.rb_discharge_reject :
                        option="R";
                        break;
                }

                executeAssignTask(option);

            }
        });
    }

    public void executeAssignTask(String option) {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(),option,etRemarks.getText().toString());
            assignTask.execute(AppSettings.ApproveRejectDischargeSummary);
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    private class AssignTask extends AsyncTask<String, Void, String[]> {
        int task;
        private final String option;
        private final String remarks;
        Activity activity;

        public AssignTask(Activity activity, String option, String remarks) {
            onAttach(activity);
            this.remarks=remarks;
            this.option = option;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing Request please wait...", Gravity.CENTER);
        }

        @Override
        protected String[] doInBackground(String... urls) {
            String response = "error";
//            List<BasicNameValuePair> params = new ArrayList<>();
            if (activity != null) {
//                params.add(new BasicNameValuePair("DoctorId", convert(Doctor.getDoctorId(activity))));
//                params.add(new BasicNameValuePair("Type", convert(option)));
//                params.add(new BasicNameValuePair("IPNo", convert(ipNo)));
//                params.add(new BasicNameValuePair("Remark", convert(remarks)));
//                HttpReq ob = new HttpReq();
//                response = ob.makeConnection(urls[0], HttpReq.GET, params);
                response =ApiCall.POST(urls[0], RequestBuilder.dischargeDetail(convert(Doctor.getDoctorId(activity)),convert(ipNo),convert(option),convert(remarks)));
                JsonParser parser=new JsonParser(activity);
                return parser.getDefaultTask(response);
//                return ob.getDefaultTask(response);
            }else {
                return null;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String[] result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result[0].equals("success")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else if (result[0].equals("failure")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else {
                        showPopUpDialog(activity, result[0], PopUpDialog.DEFAULT);
                    }

                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask(option);
                        }
                    });
                }
            }

        }
    }

}
