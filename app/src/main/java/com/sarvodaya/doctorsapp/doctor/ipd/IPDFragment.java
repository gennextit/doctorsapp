package com.sarvodaya.doctorsapp.doctor.ipd;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.model.IPDModel;
import com.sarvodaya.doctorsapp.model.adapter.IPDAdapter;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppAnimation;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;


import java.util.ArrayList;

import static com.sarvodaya.doctorsapp.util.AppTokens.APP_UNDER_TESTING;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class IPDFragment extends CompactFragment {
    private static final int TASK_GETIPDPATIENTLIST = 1, TASK_GETIPDPATIENTLIST_DETAIL = 2;
    ListView lvMain;
    String doctorId, doctorName;
    AssignTask assignTask;
    private ArrayList<IPDModel> listMain;
    private TextView tvDoctorName, tvDate;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Context context,String doctorName, String doctorId) {
        IPDFragment fragment=new IPDFragment();
        AppAnimation.setSlideAnimation(context,fragment,Gravity.BOTTOM);
        fragment.doctorId = doctorId;
        fragment.doctorName = doctorName;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_ipd, container, false);
        onActionBack(v, "IPD");
        InitUI(v);
        return v;
    }


    private void InitUI(View v) {
        tvDoctorName = (TextView) v.findViewById(R.id.tv_ipd_dr_Name);
        tvDate = (TextView) v.findViewById(R.id.tv_ipd_date);
        lvMain = (ListView) v.findViewById(R.id.lv_school);
        if (doctorName != null)
            tvDoctorName.setText(doctorName);
        tvDate.setText(viewDateDDMMMYYY());

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (listMain != null) {
                    TextView tvTitle = (TextView) view.findViewById(R.id.tv_ipd_custom_list_PatientName);
                    executeAssignTask(TASK_GETIPDPATIENTLIST_DETAIL, listMain.get(position).getIPNo(),
                            listMain.get(position).getPatientName(),tvTitle);
                }
            }
        });


        executeAssignTask(TASK_GETIPDPATIENTLIST, null, null,null);
    }

    public void executeAssignTask(int task, String ipNo, String patientName,TextView tvHeader) {
        if (isOnline()) {
            if (task == TASK_GETIPDPATIENTLIST) {
                assignTask = new AssignTask(getActivity(), doctorId,null,null,TASK_GETIPDPATIENTLIST,tvHeader);
                assignTask.execute(AppSettings.GetIPDPatientList);
            } else {
                assignTask = new AssignTask(getActivity(), doctorId, ipNo, patientName, TASK_GETIPDPATIENTLIST_DETAIL,tvHeader);
                assignTask.execute(AppSettings.GetIPDPatientDetails);
            }

        } else {
            showInternetAlertBox(getActivity());
        }


    }


    private class AssignTask extends AsyncTask<String, Void, IPDModel> {
        private final TextView tvHeader;
        int task;
        Context activity;
        String drId, ipNo, patientName;

        public AssignTask(Activity activity, String drId, String ipNo, String patientName, int task,TextView tvHeader) {
            onAttach(activity);
            this.task = task;
            this.drId = drId;
            this.ipNo = ipNo;
            this.tvHeader = tvHeader;
            this.patientName = patientName;
        }



        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == TASK_GETIPDPATIENTLIST) {
                showProgressDialog(activity, "Getting Data, Keep patience...", Gravity.CENTER);
            } else {
                showProgressDialog(activity, "Loading patient history, Keep patience...", Gravity.CENTER);
            }

        }

        @Override
        protected IPDModel doInBackground(String... urls) {
            String response = "error";
            IPDModel result;
            JsonParser jsonParser = new JsonParser(getActivity());
            if (activity != null) {
                if (task == TASK_GETIPDPATIENTLIST) {
                    response= ApiCall.POST(urls[0], RequestBuilder.setDoctorId(drId));
                    return jsonParser.getIPDDetail(response);
                } else {
                    response= ApiCall.POST(urls[0], RequestBuilder.setIpNo(ipNo));
                    return jsonParser.getIPDPatientDetail(response);
                }
            }else{
                return null;
            }

        }

        @Override
        protected void onPostExecute(IPDModel result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == TASK_GETIPDPATIENTLIST) {
                            listMain = result.getList();
                            IPDAdapter ipdAdapter = new IPDAdapter(getActivity(), R.layout.slot_ipd, listMain, getFragmentManager(), doctorId);
                            lvMain.setAdapter(ipdAdapter);
                        } else {
                            if (result.getList() != null) {
                                IPDPatientHistory ipdPatientHistory = new IPDPatientHistory();
                                ipdPatientHistory.setPatientList(result.getList());
                                ipdPatientHistory.setDoctorDetail(doctorName, doctorId, patientName, ipNo);
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.add(android.R.id.content, ipdPatientHistory, "ipdPatientHistory");
                                transaction.addToBackStack("ipdPatientHistory");
                                transaction.commit();
                            }
                        }
                    } else if (result.getOutput().equals("failure")) {
                        if (task == TASK_GETIPDPATIENTLIST) {
                            ArrayList<String> errorList = new ArrayList<>();
                            errorList.add("No patient available");
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_error,
                                    R.id.tv_message, errorList);
                            lvMain.setAdapter(adapter);
                        } else {
                            showPopUpDialog(getActivity(), "No history available for patient " + patientName, PopUpDialog.DEFAULT);
                        }

                    } else {
                        showPopUpDialog(getActivity(), result.getOutput(), PopUpDialog.DEFAULT);
                    }

                } else {
                    if (task == TASK_GETIPDPATIENTLIST) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                executeAssignTask(TASK_GETIPDPATIENTLIST, null, null,null);
                            }
                        });
                    } else {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                executeAssignTask(TASK_GETIPDPATIENTLIST_DETAIL, ipNo, patientName,tvHeader);
                            }
                        });
                    }

                }
            }

        }
    }
}