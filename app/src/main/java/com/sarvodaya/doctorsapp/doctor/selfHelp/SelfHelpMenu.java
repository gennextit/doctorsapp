package com.sarvodaya.doctorsapp.doctor.selfHelp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.model.SelfHelpModel;
import com.sarvodaya.doctorsapp.model.adapter.SelfHelpMenuAdapter;
import com.sarvodaya.doctorsapp.util.AppAnimation;

import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class SelfHelpMenu extends CompactFragment {
    ListView lvMain;

    private ArrayList<SelfHelpModel> listMain;
    private TextView tvPatient;


    public static Fragment newInstance(Activity context) {
        SelfHelpMenu selfHelpMenu=new SelfHelpMenu();
        AppAnimation.setSlideAnimation(context,selfHelpMenu, Gravity.BOTTOM);
        return selfHelpMenu;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_self_help_menu, container, false);
        onActionBack(v, "Self Help");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.lv_school);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (listMain != null) {
                    FragmentTransaction transaction;
                    switch (listMain.get(position).getMenuName()) {
                        case "Book a Leave":
                            BookALeave bookALeave = new BookALeave();
                            transaction = getFragmentManager().beginTransaction();
                            transaction.add(android.R.id.content, bookALeave, "bookALeave");
                            transaction.addToBackStack("bookALeave");
                            transaction.commit();
                            break;
                        case "Postpone Appointments":
                            PostponeAppointments postponeAppointments = new PostponeAppointments();
                            transaction = getFragmentManager().beginTransaction();
                            transaction.add(android.R.id.content, postponeAppointments, "postponeAppointments");
                            transaction.addToBackStack("postponeAppointments");
                            transaction.commit();
                            break;

                    }
                }
            }
        });

        executeAssignTask();
    }

    public void executeAssignTask() {
        listMain = new ArrayList<>();
        SelfHelpModel selfHelpModel;
        selfHelpModel = new SelfHelpModel();
        selfHelpModel.setMenuName("Book a Leave");
        selfHelpModel.setImage(R.drawable.boo_a_leave);
        listMain.add(selfHelpModel);
        selfHelpModel = new SelfHelpModel();
        selfHelpModel.setMenuName("Postpone Appointments");
        selfHelpModel.setImage(R.drawable.postponed_appointment);
        listMain.add(selfHelpModel);

        SelfHelpMenuAdapter selfHelpMenuAdapter = new SelfHelpMenuAdapter(getActivity(), R.layout.slot_selfhelp, listMain);
        lvMain.setAdapter(selfHelpMenuAdapter);

    }
}