package com.sarvodaya.doctorsapp.doctor.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.BuildConfig;
import com.sarvodaya.doctorsapp.MainActivity;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;
import com.sarvodaya.doctorsapp.util.Validation;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class LoginMobile extends CompactFragment {

    EditText etMobile;
    Button btnSignIn;
    private ProgressBar progressBar;
    AssignTask assignTask;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login_mobile, container, false);
//        onActionBack(v,"" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        TextView tvAppVersion = (TextView) v.findViewById(R.id.tv_app_version);
        etMobile = (EditText) v.findViewById(R.id.et_login_Mobile);
        btnSignIn = (Button) v.findViewById(R.id.btn_login_button);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                if (isOnline()) {
                    if(checkValidation()) {
                        assignTask = new AssignTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString(), AssignTask.VALIDATE_DOCTOR_TASK);
                        assignTask.execute(AppSettings.VALIDATE_DOCTOR);
                    }
                } else {
                    showInternetAlertBox(getActivity());
                }
            }
        });
        etMobile.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    btnSignIn.performClick();
                    return true;
                }
                return false;
            }
        });
        String appVersion = "App version "+ BuildConfig.VERSION_NAME;
        tvAppVersion.setText(appVersion);
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.isEmpty(etMobile, true)) {
            return false;
        }
        return ret;
    }


    private class AssignTask extends AsyncTask<String, Void, String> {
        Button btn;
        ProgressBar pBar;
        Context activity;
        String name, mobile;
        int task;
        public static final int VALIDATE_DOCTOR_TASK = 1, LOAD_DOCTOR_TASK = 2;
        private String macAddr;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public AssignTask(Activity activity, Button btn, ProgressBar pBar, String mobile, int task) {
            this.activity = activity;
            this.btn = btn;
            this.pBar = pBar;
            this.mobile = mobile;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//            pBar.setVisibility(View.VISIBLE);
//            btn.setVisibility(View.GONE);
            showProgressDialog(activity, "Authentication, Keep patience...", Gravity.CENTER);
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            macAddr=getMACAddress(getActivity());

//            if (activity != null) {
//                params.add(new BasicNameValuePair("mobile", mobile));
//                params.add(new BasicNameValuePair("mac_addr", getMACAddress(getActivity())));
//            }
            JsonParser jsonParser = new JsonParser(getActivity());
            if (task == VALIDATE_DOCTOR_TASK) {
//                response = ob.makeConnection(urls[0], HttpReq.GET, params);
                response=ApiCall.GET(urls[0], RequestBuilder.setMobilenMack(mobile,macAddr));
                return jsonParser.parseValidateDoctor(getActivity(), response);

            } else {
//                response = ob.makeConnection(urls[0], HttpReq.GET, null);
                response= ApiCall.GET(urls[0]);
                return jsonParser.parseLoadDoctor(getActivity(), response, Doctor.getDoctorId(activity));
            }

        }

        @Override
        protected void onPostExecute(String result) {
            if (activity != null) {
                if (result != null) {
                    if (result.equalsIgnoreCase("success")) {
                        if (task == VALIDATE_DOCTOR_TASK) {
                            Doctor.setDoctorMobile(activity, mobile);
                            assignTask = new AssignTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString()
                                    , AssignTask.LOAD_DOCTOR_TASK);
                            assignTask.execute(AppSettings.LOADDOCTOR);
                        } else {
                            hideProgressDialog();
                            Intent ob = new Intent(getActivity(), MainActivity.class);
                            startActivity(ob);
                            getActivity().finish();
                        }
                    } else if (result.equalsIgnoreCase("failure")) {
                        hideProgressDialog();
                        showPopUpDialog(getActivity(),JsonParser.ERRORMESSAGE+"\nMAC:"+macAddr, PopUpDialog.LOGIN);
                    }else if (result.equalsIgnoreCase("not available")){
                        hideProgressDialog();
                        Intent ob = new Intent(getActivity(), MainActivity.class);
                        startActivity(ob);
                        getActivity().finish();
                    }
                } else {
                    hideProgressDialog();
                    if (task == VALIDATE_DOCTOR_TASK) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                hideBaseServerErrorAlertBox();
                                if (isOnline()) {
                                    assignTask = new AssignTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString()
                                            , AssignTask.VALIDATE_DOCTOR_TASK);
                                    assignTask.execute(AppSettings.VALIDATE_DOCTOR);
                                } else {
                                    showInternetAlertBox(getActivity());
                                }
                            }
                        });
                    } else {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                hideBaseServerErrorAlertBox();
                                if (isOnline()) {
                                    if(checkValidation()) {
                                        assignTask = new AssignTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString()
                                                , AssignTask.LOAD_DOCTOR_TASK);
                                        assignTask.execute(AppSettings.LOADDOCTOR);
                                    }
                                } else {
                                    showInternetAlertBox(getActivity());
                                }
                            }
                        });
                    }

                }
            }
        }
    }
}