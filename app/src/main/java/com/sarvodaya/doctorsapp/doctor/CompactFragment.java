package com.sarvodaya.doctorsapp.doctor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.model.SideMenu;
import com.sarvodaya.doctorsapp.model.SideMenuAdapter;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.L;
import com.sarvodaya.doctorsapp.util.TypefaceSpan;

import java.io.UnsupportedEncodingException;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.sarvodaya.doctorsapp.util.PopUpDialog.DEFAULT;
import static com.sarvodaya.doctorsapp.util.PopUpDialog.LOGIN;
import static com.sarvodaya.doctorsapp.util.PopUpDialog.PDF;

public class CompactFragment extends Fragment {
    ProgressDialog progressDialog;
    boolean conn = false;
    public AlertDialog dialog = null;
    public static String ErrorMessage = "not available";
    public static int OPEN_TO_ALL = 1, INVITE_ONLY = 2, PAID_ENTRY = 3;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;
    LinearLayout llProgress, llButton;

    public CompactFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View v = inflater.inflate(R.layout.compact_fragment, container, false);
//        SetDrawer(getActivity(),v);
//        return v;
//    }


//    protected LinearLayout setProgressBar(View v) {
//        llProgress=(LinearLayout)v.findViewById(R.id.ll_progress);
//        llButton=(LinearLayout)v.findViewById(R.id.ll_button);
//        return llButton;
//    }
//    protected LinearLayout setProgressBarWithButton(View v) {
//        llProgress=(LinearLayout)v.findViewById(R.id.ll_progress);
//        llButton=(LinearLayout)v.findViewById(R.id.ll_button);
//        return llButton;
//    }
//    protected LinearLayout setProgressBarWithoutButton(View v) {
//        llProgress=(LinearLayout)v.findViewById(R.id.ll_progress);
//        return llProgress;
//    }

    protected void showProgressBar() {
//        Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_in);
        Animation animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        if (llButton != null) {
            llButton.startAnimation(animFadeOut);
            llButton.setVisibility(View.GONE);
        }
        if (llProgress != null) {
//            llProgress.startAnimation(animFadeIn);
            llProgress.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProgressBar() {
        Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        Animation animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        if (llButton != null) {
            llButton.startAnimation(animFadeIn);
            llButton.setVisibility(View.VISIBLE);
        }
        if (llProgress != null) {
            llProgress.startAnimation(animFadeOut);
            llProgress.setVisibility(View.GONE);
        }
    }


    public void showProgressDialog(Context context, String Msg, int gravity) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(Msg);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
            if (gravity == Gravity.BOTTOM) {
                progressDialog.getWindow().setGravity(Gravity.BOTTOM);
            }
            progressDialog.show();

        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showProgressWithAnimation(LinearLayout llprogress) {
        Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        llprogress.startAnimation(animZoomIn);
        llprogress.setVisibility(View.VISIBLE);

    }

    public void showFadInAnimation(LinearLayout layout) {
        Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        layout.startAnimation(animZoomIn);
        layout.setVisibility(View.VISIBLE);
    }

    public void hideProgressWithAnimation(LinearLayout llprogress) {
        Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        llprogress.startAnimation(animZoomIn);
        llprogress.setVisibility(View.GONE);
    }

    public void onActionBack(View v, String title) {
        LinearLayout backButton = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        TextView actionBarTitle = (TextView) v.findViewById(R.id.actionbar_title);
        setHeading(actionBarTitle, title);
//        actionBarTitle.setText(title);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                getFragmentManager().popBackStack();
            }
        });
    }

    public void onActionBack(View v, String title, final Activity activity) {
        LinearLayout backButton = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        TextView actionBarTitle = (TextView) v.findViewById(R.id.actionbar_title);
        actionBarTitle.setText(title);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    protected void setHeading(TextView tv, String title) {
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan(getActivity(), "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv.setText(s);

    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }


//    public void hideKeybord() {
//        InputMethodManager inputManager = (InputMethodManager) getActivity()
//                .getSystemService(getActivity().INPUT_METHOD_SERVICE);
//        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
//                InputMethodManager.HIDE_NOT_ALWAYS);
//    }

    public void hideKeybord(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = activity.getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String viewTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String viewDateDDMMMYYY() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String convertDate(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String outputDateStr = null;
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDate2(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMMM");
        Date date = null;
        String outputDateStr = null;
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDate5(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        String outputDateStr = null;
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }


    public static String convertTime24to12Hours(String inputTime) {
        DateFormat inputFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        String outputDateStr = null;
        try {
            date = inputFormat.parse(inputTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }


    public static String convertDate3(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        String outputDateStr = null;
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String getDD(String inputDate) {
        return inputDate.substring(0, 2);
    }

    public static String getMM(String inputDate) {
        return inputDate.substring(3, 5);
    }

    //	public String setBoldFont(String title) {
//		return setBoldFont(R.color.gray,Typeface.NORMAL,title);
//	}
//	public String setActionBarTitle(String title) {
//		return setBoldFont(R.color.white,Typeface.BOLD,title);
//	}
//
//	public String setBoldFont(int colorId,int typeFaceStyle, String title) {
//		SpannableString s = new SpannableString(title);
//		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
//		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
//		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//		return title;
//	}
//	public static String setBoldFont(Context context,int colorId,int typeface, String title) {
//		SpannableString s = new SpannableString(title);
//		Typeface externalFont=Typeface.createFromAsset(context.getAssets(), "fonts/segoeui.ttf");
//		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new ForegroundColorSpan(context.getResources().getColor(colorId)), 0, s.length(),
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new StyleSpan(typeface), 0, s.length(), 0);
//		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//		return title;
//	}
   /* public void setTypsFace(EditText et, EditText et1, EditText et2, EditText et3, EditText et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et1, AutoCompleteTextView et2, AutoCompleteTextView et3, AutoCompleteTextView et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(EditText et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(CheckBox et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(TextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(Button et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }*/

    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getDateMDY() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getDateDDMMM() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getDateMDYSlash() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getCurrentDD() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public String getSt(int id) {

        return getResources().getString(id);
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

//        ConnectivityManager cm = (ConnectivityManager) getActivity()
//                .getSystemService(getActivity().CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        if (haveConnectedWifi == true || haveConnectedMobile == true) {
//
//            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
//            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
//            conn = true;
//        } else {
//            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
//            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
//            conn = false;
//        }

        return true;
    }

//    public static String getMACAddress(Context activity) {
//        return "34:aa:8b:f0:04:4e";// Amit Gupta Device MAC
//    }
    //get Adress Mac By Interface
    public static String getMACAddress(Context activity) {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString().toLowerCase();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }


//    public String getMACAddress(Context activity) {
////        String address = "74:04:2b:0b:a1:59";
//        String address = "02:00:00:00:00:02";
//        // TODO Auto-generated method stub
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            DeviceAdminReceiver admin = new DeviceAdminReceiver();
//            DevicePolicyManager devicepolicymanager = admin.getManager(activity);
//            ComponentName name1 = admin.getWho(activity);
//            if (devicepolicymanager.isAdminActive(name1)) {
//                address = devicepolicymanager.getWifiMacAddress(name1);
//            }
//            return address;
//        }else {
//            WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
//
//            if (wifiManager.isWifiEnabled()) {
//                // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
//                WifiInfo info = wifiManager.getConnectionInfo();
//                address = info.getMacAddress();
//
//            } else {
//                // ENABLE THE WIFI FIRST
//                wifiManager.setWifiEnabled(true);
//                WifiInfo info = wifiManager.getConnectionInfo();
//                address = info.getMacAddress();
//                wifiManager.setWifiEnabled(false);
//            }
//            if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
//                return address;
//            } else {
//                return Doctor.testMAC;
//            }
//        }
//    }

    public String urlEn(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

//    public void showReviewAlertSuccess(Activity Act, String cat) {
//        showReviewAlertSuccess(Act, cat, null, 0);
//    }
//
//    public void showReviewAlertSuccess(Activity Act, String cat, String sltSbuddy) {
//        showReviewAlertSuccess(Act, cat, sltSbuddy, 0);
//    }
//
//    public void showReviewAlertSuccess(Activity Act, String cat, int btnTask) {
//        showReviewAlertSuccess(Act, cat, null, btnTask);
//    }

    public void showPopUpDialog(final Activity Act, String message, final int alertType) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        final FragmentManager manager = getActivity().getSupportFragmentManager();
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = Act.getLayoutInflater();

        View v = inflater.inflate(R.layout.popup_dialog, null);
        TextView baseTitle = (TextView) v.findViewById(R.id.custom_dialog_baseTitle);
        TextView baseSubTitle = (TextView) v.findViewById(R.id.custom_dialog_baseSubTitle);
        TextView baseText = (TextView) v.findViewById(R.id.custom_dialog_baseText);
        switch (alertType) {
            case DEFAULT:
                baseTitle.setText("Alert!!");
                baseText.setText(message);
                break;
            case LOGIN:
                baseTitle.setText("Alert!!");
                baseText.setText(message);
                break;
            case PDF:
                baseTitle.setText("Required PDF Viewer!!");
                baseText.setText(message);
                break;

        }

        dialogBuilder.setView(v);
        Button sendButton = (Button) v.findViewById(R.id.btn_custom_main_search_coaches_review_rate_goBack);

        switch (alertType) {
            case DEFAULT:
                sendButton.setText("OK");
                break;
            case LOGIN:
                sendButton.setText("OK");
                break;
            case PDF:
                sendButton.setText("DOWNLOAD");
                break;
        }
        sendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                switch (alertType) {
                    case DEFAULT:

                        break;
                    case LOGIN:

                        break;
                    case PDF:
                        try {
                            Uri uri = Uri.parse("market://details?id=" + "com.adobe.reader");
                            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                                    "http://play.google.com/store/apps/details?id=" + "com.adobe.reader")));
                        }
                        break;
//                    case -2:
//                        manager.popBackStack();
//                        break;
//                    case -3:
//                        Act.finish();
//                        break;


                }

            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void showInternetPopUpDialog(final Activity Act, final String message) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        final FragmentManager manager = getActivity().getSupportFragmentManager();
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = Act.getLayoutInflater();

        View v = inflater.inflate(R.layout.popup_internet, null);
        final TextView tvDetail = (TextView) v.findViewById(R.id.tv_alert_internet_detail);
        ImageView ivCloud = (ImageView) v.findViewById(R.id.iv_alert_internet);
        Button btnOk = (Button) v.findViewById(R.id.btn_alert_internet);

        tvDetail.setText(getSt(R.string.internet_slow_response_msg));

        ivCloud.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                tvDetail.setText(message);
            }
        });

        dialogBuilder.setView(v);


        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    // public void showReview_rate_submitToast(Context context){
    // // Inflate the Layout
    // LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
    // Activity.LAYOUT_INFLATER_SERVICE);
    // //LayoutInflater inflater = context.getLayoutInflater();
    //
    // //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup)
    // findViewById(R.id.custom_toast_layout_id));
    // View
    // layout=lInflater.inflate(R.layout.custom_main_search_coaches_review_rate_submit,
    // null);
    //// TextView
    // a=(TextView)layout.findViewById(R.id.tv_custom_toast_ViewMessage);
    //// ImageView b=(ImageView)layout.findViewById(R.id.iv_custom_toast);
    //// layout.setBackgroundResource((status) ? R.drawable.toast_bg :
    // R.drawable.toast_bg_red);
    //// b.setImageResource((status) ? R.drawable.success : R.drawable.fail);
    //// a.setText(txt);
    //// a.setTextColor((status) ? getResources().getColor(R.color.icon_green) :
    // getResources().getColor(R.color.icon_red));
    // // Create Custom Toast
    // Toast toast = new Toast(context);
    // toast.setGravity(Gravity.CENTER, 0, 0);
    // toast.setDuration(Toast.LENGTH_SHORT);
    // toast.setView(layout);
    // toast.show();
    // }
    //


    public void showInternetAlertBox(Activity activity) {
        showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2);
    }

    public void showDefaultAlertBox(Activity activity, String title, String Description) {
        showDefaultAlertBox(activity, title, Description, 1);
    }

    public void showDefaultAlertBox(Activity activity, String title, String Description, int noOfButtons) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog_new, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(title);
        tvDescription.setText(Description);
        if (noOfButtons == 1) {
            button1.setVisibility(View.GONE);
            button2.setText("Done");
        }
        button1.setText("Setting");
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                EnableMobileIntent();
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);
    }

    protected void SetDrawerListener(View v) {
        LinearLayout llHome = (LinearLayout) v.findViewById(R.id.ll_actionbar_home);
        llHome.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                if (!drawerOpen) {
                    dLayout.openDrawer(dList);
                } else {
                    dLayout.closeDrawer(dList);
                }

            }
        });
    }

    protected void SetDrawer(final Activity act, LinearLayout iv, View v) {
        // TODO Auto-generated method stub
//        ImageLoader imageLoader;
//        imageLoader = new ImageLoader(act);

        iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                if (!drawerOpen) {
                    dLayout.openDrawer(dList);
                } else {
                    dLayout.closeDrawer(dList);
                }

            }
        });

        dLayout = (DrawerLayout) v.findViewById(R.id.drawer_layout);
        dList = (ListView) v.findViewById(R.id.left_drawer);
//        LayoutInflater inflater = getLayoutInflater();
//        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
//        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
//        ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
//
//        imageLoader.DisplayImage(KidProfile.getStudentImage(BaseActivity.this), ivProfileToggle, "blank", false);
//        imageLoader.DisplayImage(KidProfile.getStudentImage(BaseActivity.this), ivProfile, "blank", false);
//        tvName.setText(KidProfile.getStudentName(BaseActivity.this));
//
//
//        dList.addHeaderView(listHeaderView);
        SideMenu s1 = new SideMenu("Facebook Feeds", R.mipmap.ic_menu_facebook);
        SideMenu s2 = new SideMenu("Twitter Feeds", R.mipmap.ic_menu_twitter);
        SideMenu s3 = new SideMenu("Website", R.mipmap.ic_menu_website);
        SideMenu s4 = new SideMenu("Share With Friends", R.mipmap.ic_menu_share);

        // SideMenu s7 = new SideMenu("Logout", R.drawable.menu8);

        sideMenuList = new ArrayList<SideMenu>();
        sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        // sideMenuList.add(s6);
        // sideMenuList.add(s7);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(slideMenuAdapter);
        // dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();

                // FragmentManager mannager = getFragmentManager();
                // FragmentTransaction transaction;

                Intent intent;
                FragmentManager mannager;
                FragmentTransaction transaction;
                Intent browserIntent1;
                switch (position) {

                    case 1:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Sarvodayafaridabad"));
                        startActivity(browserIntent1);
//                        ProfileSelection profileSelection = new ProfileSelection();
//                        profileSelection.setCallStatus(ProfileSelection.BASE_ACT);
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, profileSelection, "profileSelection");
//                        transaction.addToBackStack("profileSelection");
//                        transaction.commit();
                        break;
                    case 2:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/sarvodaya_care"));
                        startActivity(browserIntent1);
                           /* CoachRequestCoin coachRequestCoin = new CoachRequestCoin();
                            mannager = getFragmentManager();
                            transaction = mannager.beginTransaction();
                            transaction.replace(android.R.id.content, coachRequestCoin, "coachRequestCoin");
                            transaction.addToBackStack("coachRequestCoin");
                            transaction.commit();*/
                        break;
                    case 3:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://sarvodayahospital.com/"));
                        startActivity(browserIntent1);
//                        Feedback feedback = new Feedback();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, feedback, "feedback");
//                        transaction.addToBackStack("feedback");
//                        transaction.commit();
                        break;
                    case 4:
                        Intent in = new Intent("android.intent.action.SEND");
                        in.setType("text/plain");
                        in.putExtra("android.intent.extra.TEXT", "Check out this app!  https://play.google.com/store/apps");
                        startActivity(Intent.createChooser(in, "Share With Fiends"));
                       /* try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
                            i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) { // e.toString();
                        }*/
                        break;
                    case 5:
                      /*  UserGuide userGuide = new UserGuide();
                        mannager = getFragmentManager();
                        transaction = mannager.beginTransaction();
                        transaction.replace(android.R.id.content, userGuide, "userGuide");
                        transaction.addToBackStack("userGuide");
                        transaction.commit();*/

                        break;

                }

            }

        });
    }

    public void hideBaseServerErrorAlertBox() {
        if (dialog != null)
            dialog.dismiss();
    }

    public Button showBaseServerErrorAlertBox(String errorDetail) {
        return showBaseAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
    }

    public Button showBaseServerErrorAlertBox() {
        return showBaseAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_slow_response_msg), 2, null);
    }

    public Button showBaseAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog_new, null);
        dialogBuilder.setView(v);
//        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvTitle.setText(title);

        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        tvTitle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        //tvTitle.setText(title);
        tvDescription.setText(Description);
        if (noOfButtons == 1) {
            button2.setVisibility(View.GONE);
        }
        button1.setText("Retry");

        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
        return button1;
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    public void addAnimationFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragmentWithoutBackStack(Fragment fragment, String tag) {
        addFragmentWithoutBackStack(fragment, android.R.id.content, tag);
    }

    public void addFragmentWithoutBackStack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

}
