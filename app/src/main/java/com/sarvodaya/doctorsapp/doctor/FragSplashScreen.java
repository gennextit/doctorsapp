package com.sarvodaya.doctorsapp.doctor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.LoginActivity;
import com.sarvodaya.doctorsapp.MainActivity;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.RequestBuilder;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class FragSplashScreen extends CompactFragment {

    private static int SPLASH_TIME_OUT = 500;
    AssignTask assignTask;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_splash_screen, container, false);
//        onActionBack(v,"" );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {


    }


    @Override
    public void onResume() {
        super.onResume();


        if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_TESTING) {
            Doctor.setDoctorMobile(getActivity(), "9876543211");// @test
        }

        if (!Doctor.getDoctorMobile(getActivity()).equals("")) {
//                    AsyncRequest getPosts = new AsyncRequest(SplashScreen.this,1,"BOTTOM", "Authentication, Keep patience...");
//                    getPosts.execute(AppSettings.VALIDATE_DOCTOR+"?mobile="+util.LoadPreferences(AppTokens.UserMOBILE)+"&mac_addr="+util.getMACAddress());

            if (isOnline()) {
                assignTask = new AssignTask(getActivity());
                assignTask.execute(AppSettings.VALIDATE_DOCTOR);
                //new HttpAsyncTaskLogIn().execute(AppSettings.VALIDATE_DOCTOR+"?mobile="+LoadPreferences(DRMOBILE)+"&mac_addr="+getMACAddress());
            } else {
                showInternetAlertBox(getActivity());
            }
        } else {
            startTimer();
        }

    }

    public void startTimer() {
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, SPLASH_TIME_OUT);
    }


    private class AssignTask extends AsyncTask<String, Void, String> {

        Context activity;

        public AssignTask(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Context activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showProgressDialog(getActivity(), "Authentication, Keep patience...", Gravity.BOTTOM);
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            if (activity != null) {
                String drMobile = Doctor.getDoctorMobile(activity);
                String drMAC = getMACAddress(getActivity());

                if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
                    JsonParser jsonParser = new JsonParser(getActivity());
                    response= ApiCall.GET(params[0], RequestBuilder.setMobilenMack(drMobile,drMAC));
                    return jsonParser.setDoctorProfile(getActivity(), response);
                } else {
                    // @test
                    Doctor.setDoctorId(activity, Doctor.testDrId);
                    Doctor.setDoctorName(activity, Doctor.testDrName);
                    Doctor.setDoctorImage(activity, Doctor.testImageURL);
                    Doctor.setDoctorIPDId(activity, Doctor.testDrIdIPD);
                    return "success";
                }
            }else {
                return null;
            }
        }

        
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.equalsIgnoreCase("success")) {
                        Intent ob = new Intent(getActivity(), MainActivity.class);
                        startActivity(ob);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), JsonParser.ERRORMESSAGE, Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }
                } else {
                    Button btn = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();

                            assignTask = new AssignTask(getActivity());
                            assignTask.execute(AppSettings.VALIDATE_DOCTOR);
                        }
                    });
                }
            }
        }
    }
}