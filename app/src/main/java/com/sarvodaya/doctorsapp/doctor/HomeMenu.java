package com.sarvodaya.doctorsapp.doctor;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pubnub.api.Pubnub;
import com.sarvodaya.doctorsapp.MainActivity;
import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.imagecache.ImageLoader;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class HomeMenu extends CompactFragment implements View.OnClickListener{

    ImageView drImage,btnOpd,btnIpd,btnHelp,btnOtBooking,btnSOS,btnChat;
    TextView viewDrName;
    Pubnub pubnub;
    ImageLoader imageLoader;
    private TextView tvIpd;

    public static Fragment newInstance() {
        HomeMenu homeMenu=new HomeMenu();
        return homeMenu;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_home_menu, container, false);
        imageLoader=new ImageLoader(getActivity());
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        viewDrName = (TextView)v.findViewById(R.id.tv_main_viewDrName);
//        viewDrName.setText(util.LoadPreferences(AppTokens.DOCTORNAME));

        btnOpd=(ImageView)v.findViewById(R.id.iv_btn_opd);
        tvIpd=(TextView)v.findViewById(R.id.tv_menu_ipd);
        btnIpd=(ImageView)v.findViewById(R.id.iv_btn_ipd);
        btnHelp=(ImageView)v.findViewById(R.id.iv_btn_selfhelp);
        btnOtBooking=(ImageView)v.findViewById(R.id.iv_btn_otbooking);
        btnSOS=(ImageView)v.findViewById(R.id.iv_btn_sos);
        btnChat=(ImageView)v.findViewById(R.id.iv_btn_chat);
        btnOpd.setOnClickListener(this);
        btnIpd.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        btnOtBooking.setOnClickListener(this);
        btnSOS.setOnClickListener(this);
        btnChat.setOnClickListener(this);


        drImage=(ImageView)v.findViewById(R.id.iv_main_viewprofile);

        imageLoader.DisplayImage(Doctor.getDoctorImage(getActivity()),drImage,"profile",false);
        viewDrName.setText(Doctor.getDoctorName(getActivity()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_btn_opd:
                ((MainActivity)getActivity()).setOPD();

                break;
            case R.id.iv_btn_ipd:
                ((MainActivity)getActivity()).setIPD();
//                startIPDActivity(tvIpd);
                break;
            case R.id.iv_btn_selfhelp:
                ((MainActivity)getActivity()).setSelfHelp();
                break;
            case R.id.iv_btn_otbooking:
                ((MainActivity)getActivity()).setOTBooking();
                break;
            case R.id.iv_btn_sos:
                ((MainActivity)getActivity()).setSOSOption();
                break;
            case R.id.iv_btn_chat:
                ((MainActivity)getActivity()).setChatOption();
                break;
        }
    }

//    private void startIPDActivity(TextView tvName) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Intent intent=new Intent(getActivity(),IPDActivity.class);
//            ActivityOptions options = ActivityOptions
//                    .makeSceneTransitionAnimation(getActivity(), tvName, getSt(R.string.transition_title));
//            startActivity(intent,options.toBundle());
//            return;
//        }
//        Intent intent=new Intent(getActivity(),IPDActivity.class);
//        startActivity(intent);
//    }
}