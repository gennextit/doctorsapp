package com.sarvodaya.doctorsapp.doctor.selfHelp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.R;
import com.sarvodaya.doctorsapp.doctor.CompactFragment;
import com.sarvodaya.doctorsapp.global.DatePickerDialog;
import com.sarvodaya.doctorsapp.global.TimePickerDialog;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.DateTimeUtility;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.JsonParser;
import com.sarvodaya.doctorsapp.util.PopUpDialog;
import com.sarvodaya.doctorsapp.util.RequestBuilder;
import com.sarvodaya.doctorsapp.util.Validation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Abhijit on 27-Sep-16.
 */

public class BookALeave extends CompactFragment implements View.OnClickListener
        ,DatePickerDialog.DateSelectFlagListener,TimePickerDialog.TimeSelectTypeListener{
    TextView tvFromDate, tvToDate, tvFromTime, tvToTime;
    Button confirmButton;
    EditText reason;
    LinearLayout FDate, TDate, FTime, TTime;
    static String startDay, endDay, sTime, eTime;
    int startDateStamp, endDateStamp, currentDate;
    long startTimeStamp, endTimeStamp;
    int sday, smonth, syear, shours, smin;
    int eday, emonth, eyear, ehours, emin;

    AssignTask assignTask;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_book_aleave, container, false);
        onActionBack(v, "Book a Leave");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        FDate = (LinearLayout) v.findViewById(R.id.ll_book_leave_selectFromDate);
        TDate = (LinearLayout) v.findViewById(R.id.ll_book_leave_selectToDate);
        FTime = (LinearLayout) v.findViewById(R.id.ll_book_leave_selectFromTime);
        TTime = (LinearLayout) v.findViewById(R.id.ll_book_leave_selectToTime);
        confirmButton = (Button) v.findViewById(R.id.btn_submit_button);
        confirmButton.setOnClickListener(this);
        FDate.setOnClickListener(this);
        TDate.setOnClickListener(this);
        FTime.setOnClickListener(this);
        TTime.setOnClickListener(this);

        tvFromDate = (TextView) v.findViewById(R.id.tv_book_leave_FromDate);
        tvToDate = (TextView) v.findViewById(R.id.tv_book_leave_ToDate);
        tvFromTime = (TextView) v.findViewById(R.id.tv_book_leave_FromeTime);
        tvToTime = (TextView) v.findViewById(R.id.tv_book_leave_ToTime);
        reason = (EditText) v.findViewById(R.id.et_book_leave_Reason);

        getDateTime();
    }

    @Override
    public void onClick(View view) {
//        DialogFragment newFragment;
        switch (view.getId()) {
            case R.id.btn_submit_button:
                if(checkValidation()){
                    hideKeybord(getActivity());
                    executeAssignTask();
                }
                break;
            case R.id.ll_book_leave_selectFromDate:
                DatePickerDialog.newInstance(BookALeave.this,true,DatePickerDialog.START_DATE,sday,smonth,syear)
                        .show(getFragmentManager(),"datePickerDialog");
                break;
            case R.id.ll_book_leave_selectToDate:
                DatePickerDialog.newInstance(BookALeave.this,true,DatePickerDialog.END_DATE,eday,emonth,eyear)
                        .show(getFragmentManager(),"datePickerDialog");

                break;
            case R.id.ll_book_leave_selectFromTime:
                TimePickerDialog.newInstance(BookALeave.this,false,TimePickerDialog.TIME_START,shours,smin)
                        .show(getFragmentManager(),"timePickerDialog");

                break;
            case R.id.ll_book_leave_selectToTime:
                TimePickerDialog.newInstance(BookALeave.this,false,TimePickerDialog.TIME_END,ehours,emin)
                        .show(getFragmentManager(),"timePickerDialog");
                break;

        }

    }

    public void executeAssignTask() {
        if (isOnline()) {
            assignTask = new AssignTask(getActivity(),reason.getText().toString());
            assignTask.execute(AppSettings.RequestLeave);
        } else {
            showInternetAlertBox(getActivity());
        }
    }

    private boolean checkValidation() {
        boolean ret = true;


        if (startDateStamp < currentDate) {
            Toast.makeText(getActivity(), "Please select a future from date", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }
        if (endDateStamp < currentDate) {
            Toast.makeText(getActivity(), "Please select a future to date", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }
        if (startDateStamp > endDateStamp) {
            Toast.makeText(getActivity(), "Start date must be less than end date", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }
        if (endDateStamp < startDateStamp) {
            Toast.makeText(getActivity(), "End date must be grater than start date", Toast.LENGTH_SHORT)
                    .show();

            return false;
        }
        if (sTime == null) {
            Toast.makeText(getActivity(), "Please select start time", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (eTime == null) {
            Toast.makeText(getActivity(), "Please select end time", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (startTimeStamp > endTimeStamp) {
            Toast.makeText(getActivity(), "Start time must be less than end time", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }
        if (endTimeStamp < startTimeStamp) {
            Toast.makeText(getActivity(), "End time must be grater than start time", Toast.LENGTH_SHORT)
                    .show();

            return false;
        }

        if (!Validation.isEmpty(reason, true)) {
            return false;
        }
        return ret;
    }

    @Override
    public void onSelectDateFlagClick(DialogFragment dialog, int dateType, int day, int month, int year) {
        if (dateType == DatePickerDialog.START_DATE) {
            startDay = DateTimeUtility.convertDateDDMMYYYY(day, month, year);
            tvFromDate.setText(startDay);
            startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year, month, day));
            syear = year;
            smonth = month;
            sday = day;
            startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));

        } else {
            endDay = DateTimeUtility.convertDateDDMMYYYY(day, month, year);
            tvToDate.setText(endDay);
            endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year, month, day));
            eyear = year;
            emonth = month;
            eday = day;
            endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear, emonth, eday, ehours, emin));

        }
    }


//    @SuppressLint("ValidFragment")
//    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Calendar calendar = Calendar.getInstance();
//            DatePickerDialog dpd;
//            if (status == START_DATE) {
//                dpd = new DatePickerDialog(getActivity(), this, syear, smonth, sday);
//
//            } else {
//                dpd = new DatePickerDialog(getActivity(), this, eyear, emonth, eday);
//
//            }
//            // Set the DatePicker minimum date selection to current date
//            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
//            // dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way
//            // to get the current day
//
//            // //Add 6 days with current date
//            // calendar.add(Calendar.DAY_OF_MONTH,6);
//            //
//            // //Set the maximum date to select from DatePickerDialog
//            // dp.setMaxDate(calendar.getTimeInMillis());
//
//            return dpd;
//        }
//
//        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
//            populateSetDate(yy, mm, dd);
////			storeTimeStamp(yy,mm,dd);
//
//
//        }
//
//
//        public void populateSetDate(int year, int month, int day) {
//            if (status == START_DATE) {
//                startDay = DateTimeUtility.convertDateDDMMYYYY(day, month, year);
//                tvFromDate.setText(startDay);
//                startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year, month, day));
//                syear = year;
//                smonth = month;
//                sday = day;
//                startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
//
//            } else {
//                endDay = DateTimeUtility.convertDateDDMMYYYY(day, month, year);
//                tvToDate.setText(endDay);
//                endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year, month, day));
//                eyear = year;
//                emonth = month;
//                eday = day;
//                endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear, emonth, eday, ehours, emin));
//
//            }
//        }
//
//    }
//	private int storeTimeStamp(int yy, int mm, int dd) {
//		String year=String.valueOf(yy);
//		String month=String.valueOf(mm);
//		String date=String.valueOf(dd);
//		try {
//			return Integer.parseInt(year + month + date);
//		} catch (NumberFormatException e) {
//			L.m(e.toString());
//		}
//		return 0;
//	}

    public void getDateTime() {
        String date, time;
        final Calendar cal = Calendar.getInstance();
        syear = cal.get(Calendar.YEAR);
        smonth = cal.get(Calendar.MONTH);
        sday = cal.get(Calendar.DAY_OF_MONTH);
        eyear = cal.get(Calendar.YEAR);
        emonth = cal.get(Calendar.MONTH);
        eday = cal.get(Calendar.DAY_OF_MONTH);

        shours = cal.get(Calendar.HOUR_OF_DAY);
        smin = cal.get(Calendar.MINUTE);
        ehours = cal.get(Calendar.HOUR_OF_DAY);
        emin = cal.get(Calendar.MINUTE);

        date = DateTimeUtility.convertDateDDMMYYYY(sday, smonth, syear);
        currentDate = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));
        startDay = date;
        endDay = date;
        startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));
        endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));

        startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
        endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
        tvFromDate.setText(date);
        tvToDate.setText(date);

        time = new SimpleDateFormat("h:mm a").format(cal.getTime());

        tvFromTime.setText(time);
        tvToTime.setText(time);

    }

    @Override
    public void onSelectDateFlagClick(DialogFragment dialog, int dateType, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);

        String format = new SimpleDateFormat("h:mm a").format(c.getTime());

        if (dateType == TimePickerDialog.TIME_START) {
            sTime = format;
            tvFromTime.setText(format);
            shours = hourOfDay;
            smin = minute;
            startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));

        } else {
            eTime = format;
            tvToTime.setText(format);
            ehours = hourOfDay;
            emin = minute;
            endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear, emonth, eday, ehours, emin));

        }
    }

//    @SuppressLint("ValidFragment")
//    public class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            //final Calendar calendar = Calendar.getInstance();
//            TimePickerDialog tp;
//            if (status == START_TIME) {
//                tp = new TimePickerDialog(getActivity(), this, shours, smin, true);
//            } else {
//                tp = new TimePickerDialog(getActivity(), this, ehours, emin, true);
//            }
//            return tp;
//        }
//
//        @Override
//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            Calendar c = Calendar.getInstance();
//            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            c.set(Calendar.MINUTE, minute);
//
//            String format = new SimpleDateFormat("h:mm a").format(c.getTime());
//
//            if (status == START_TIME) {
//                sTime = format;
//                tvFromTime.setText(format);
//                shours = hourOfDay;
//                smin = minute;
//                startTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear, smonth, sday, shours, smin));
//
//            } else {
//                eTime = format;
//                tvToTime.setText(format);
//                ehours = hourOfDay;
//                emin = minute;
//                endTimeStamp = Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear, emonth, eday, ehours, emin));
//
//            }
//        }
//    }

    private class AssignTask extends AsyncTask<String, Void, String[]> {

        private final String reason;
        Activity activity;

        public AssignTask(Activity activity, String reason) {
            onAttach(activity);
            this.reason = reason;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Leave Request Processing...", Gravity.CENTER);
        }

        @Override
        protected String[] doInBackground(String... urls) {
            String response = "error";
//            List<BasicNameValuePair> params = new ArrayList<>();
            JsonParser jsonParser = new JsonParser(getActivity());
            if (activity != null) {
//                params.add(new BasicNameValuePair("DoctorId", convert(Doctor.getDoctorId(activity))));
//                params.add(new BasicNameValuePair("FromDate", convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear))));
//                params.add(new BasicNameValuePair("ToDate", convert(DateTimeUtility.convertDateYYYYMMDD(eday, emonth, eyear))));
//                params.add(new BasicNameValuePair("FromTime", convert(DateTimeUtility.convertTime(shours, smin))));
//                params.add(new BasicNameValuePair("ToTime", convert(DateTimeUtility.convertTime(ehours, emin))));
//                params.add(new BasicNameValuePair("Reason", convert(reason)));

//            HttpReq ob = new HttpReq();

//            response = ob.makeConnection(urls[0], HttpReq.GET, params);
                response=ApiCall.POST(urls[0], RequestBuilder.setReqLeave(convert(Doctor.getDoctorId(activity)),
                        convert(DateTimeUtility.convertDateYYYYMMDD(sday, smonth, syear)),
                        convert(DateTimeUtility.convertDateYYYYMMDD(eday, emonth, eyear)),
                        convert(DateTimeUtility.convertTime(shours, smin)),
                        convert(DateTimeUtility.convertTime(ehours, emin)),
                        convert(reason)));
            }


            return jsonParser.getBookALeaveDetail(response);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String[] result) {
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result[0].equals("success")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else if (result[0].equals("failure")) {
                        showPopUpDialog(activity, result[1], PopUpDialog.DEFAULT);
                    } else {
                        showPopUpDialog(activity, result[0], PopUpDialog.DEFAULT);
                    }

                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeAssignTask();
                        }
                    });
                }
            }

        }
    }

}