package com.sarvodaya.doctorsapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Admin on 7/10/2017.
 */

public class EditDischargeEntityActivity extends BaseActivity {

    private String summaryTitle,summaryDescription;
    private int groupPosition,childPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_discharge_entity);

        Intent inient = getIntent();
        if(inient!=null){
            summaryTitle=inient.getStringExtra("summaryTitle");
            summaryDescription=inient.getStringExtra("summaryDescription");
            groupPosition=inient.getIntExtra("groupPosition",0);
            childPosition=inient.getIntExtra("childPosition",0);

            initUi();
        }else{
            Toast.makeText(this,"Something went wrong",Toast.LENGTH_LONG).show();
        }
    }

    private void initUi() {
        final TextView tvTitle = (TextView)findViewById(R.id.tv_alert_dialog_title);
        ImageView button1 = (ImageView)findViewById(R.id.iv_edit_save);
        ImageView button2 = (ImageView)findViewById(R.id.iv_edit_cancel);

        final EditText etEdit = (EditText)findViewById(R.id.et_edit_summary);

        tvTitle.setText(summaryTitle);
        etEdit.setText(summaryDescription);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("summaryTitle",summaryTitle);
                intent.putExtra("summaryDescription",etEdit.getText().toString());
                intent.putExtra("groupPosition",groupPosition);
                intent.putExtra("childPosition",childPosition);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }
}
