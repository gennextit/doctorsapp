package com.sarvodaya.doctorsapp;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.model.OnlineUsersAdapter;
import com.sarvodaya.doctorsapp.model.UsersModel;
import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class OnlineUsers extends BaseActivity {
    private static final String DOCTOR_NAME = "doctor_name";
    private static final String DOCTOR_ID = "doctor_id";
    private static final String STATUS = "status";
    private static final String MOBILE = "mobile";

    int appStartupFlag = 0;

    int LoadListFlag = 0;
    TextView uName, uOnline;
    ListView lv;
    ArrayList<UsersModel> sList;
    ScheduledExecutorService scheduleTaskExecutor;
    Future<?> future;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_users);
        tvTitle = (TextView) findViewById(R.id.actionbar_title);
        LinearLayout backButton = (LinearLayout) findViewById(R.id.ll_actionbar_back);
        setActionBack(this,backButton);
        setHeading(tvTitle, "Chat");
        uName = (TextView) findViewById(R.id.tv_online_users_userName);
        uOnline = (TextView) findViewById(R.id.tv_online_users_onlineStatus);
        uName.setText(Doctor.getDoctorName(this));
        uOnline.setText("Online");


        lv = (ListView) findViewById(R.id.lv_online_users_onlineList);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                // TODO Auto-generated method stub
                String cDoctor = sList.get(position).getdoctor_name();
                String cId = sList.get(position).getdoctor_id();
                String ClientMobile = sList.get(position).getmobile();
                String Status = sList.get(position).getstatus();
                if (Status.equalsIgnoreCase("Online")) {
                    // Starting single contact activity

                    Intent in = new Intent(OnlineUsers.this, UserChat.class);
//	                in.putExtra("cDoctor", cDoctor);
//	                in.putExtra("cId", cId);
//	                
                    SavePreferences(AppTokens.ClientDOCTORNAME, cDoctor);
                    SavePreferences(AppTokens.ClientMOBILE, ClientMobile);
                    SavePreferences(AppTokens.ClientDOCTORId, cId);
                    Log.e("Online_ClientMOBILE", ClientMobile);
                    scheduleTaskExecutor.shutdown();

                    startActivity(in);

                    finish();
                }

            }
        });
        refreshPage(9);
        SavePreferences(AppTokens.onlineusers, "");

//		 if(isConnectionAvailable()){
//			 SavePreferences("ALLUSERS","");
//			 new LoadListView().execute(AppSettings.ALL_USERS);
//		 }

    }



    /***********************************************/
/******************* Background Task *******************/
    /***********************************************/
    public void startNewService() {

        startService(new Intent(this, BGServices.class));
    }

    // Stop the  service
    public void stopNewService() {
        scheduleTaskExecutor.shutdown();
        stopService(new Intent(this, BGServices.class));
    }


    private void refreshPage(int sec) {
        scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
        /*This schedules a runnable task every second*/
        future = scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        if (LoadListFlag == 0) {
                            // START NEW TASK HERE
                            if (isOnline()) {
                                Log.e("LoadListView_status", "FINISHED");
                                new LoadListView().execute(AppSettings.ALL_USERS);
                            } else {
                                Log.e("Internet Error", getResources().getString(R.string.internet_error_tag));
                            }

                        }
                    }

                });
            }
        }, 0, sec, TimeUnit.SECONDS);

    }

    public class LoadListView extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoadListFlag = 1;
            if (appStartupFlag == 0) {
                pDialog = new ProgressDialog(OnlineUsers.this);
                pDialog.setMessage("Getting Data ...");
                pDialog.setIndeterminate(false);
                pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
                pDialog.setCancelable(false);
                pDialog.show();
            }

        }

        @Override
        protected String doInBackground(String... urls) {

            String AllUsers = LoadPreferences(AppTokens.onlineusers);
            if (AllUsers.equals("")) {
//                String json;
                return ApiCall.GET(urls[0]);
//                try {
//                    json = GETURL(urls[0]);
//                    return json;
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    Log.e("doInBackground", "IOException");
//                    e.printStackTrace();
//                }

            } else {
                return AllUsers;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (appStartupFlag == 0) {
                pDialog.dismiss();
                appStartupFlag = 1;
            }
            int adapterFlag = 0;
            String ErrorMsg = "Record not Available, check your Internet connection.";

            sList = new ArrayList<UsersModel>();
            LoadListFlag = 0;

            if (result.contains("[")) {
                //Log.e("API ALL_USERS", ":"+result);
                String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

                try {
                    JSONArray jArray = new JSONArray(finalResult);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject data = jArray.getJSONObject(i);

                        if (!data.optString(DOCTOR_NAME).equalsIgnoreCase(Doctor.getDoctorName(getApplicationContext()))) {
                            UsersModel slot = new UsersModel();
                            adapterFlag = 1;
                            slot.setdoctor_name(data.optString(DOCTOR_NAME));
                            slot.setdoctor_id(data.optString(DOCTOR_ID));
                            slot.setmobile(data.optString(MOBILE));
                            slot.setstatus(data.optString(STATUS));

                            sList.add(slot);
                        } else if (!data.optString("ErrorCode").equalsIgnoreCase("")) {
                            ErrorMsg = data.optString("ErrorMsg");
                            //slot.setATime(data.optString(TAG_APPTIME).substring(0, 5));
                            //HashMap<String, String> patient = new HashMap<String, String>();
                            // adding each child node to HashMap key => value

                            // adding contact to contact list

                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                serverTimeOut(result);
                Log.e("API ALL_USERS", ":" + result);

            }

            if (adapterFlag == 1) {
                ArrayAdapter<UsersModel> adapter = new OnlineUsersAdapter(OnlineUsers.this, sList);
                lv.setAdapter(adapter);

            } else {
                String[] emptyArray = {ErrorMsg};
                ArrayAdapter adapter = new ArrayAdapter<String>(OnlineUsers.this, R.layout.empty_no_record_found, emptyArray);
                lv.setAdapter(adapter);
            }
			
           /* ListAdapter adapter = new SimpleAdapter(
                    OPDActivity.this, patientList,R.layout.opd_custon_list_view, new String[] { TAG_APPID, TAG_APPNAME,
                    		TAG_APPTIME,TAG_APPDATE }, new int[] { R.id.tv_opd_custom_list_appId,
                            R.id.tv_opd_custom_list_name, R.id.tv_opd_custom_list_appTime,R.id.tv_opd_custom_list_appDate });
 			*/
        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        scheduleTaskExecutor.shutdown();

        finish();
        super.onBackPressed();
    }
}
