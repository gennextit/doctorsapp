package com.sarvodaya.doctorsapp;


import java.io.IOException;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.sarvodaya.doctorsapp.util.ApiCall;

public class OfflineServices extends Service {

    SharedPreferences sharedPreferences;
    String OFFLINE = "http://SarvodayaHospital.com/doctorapp/index.php/Doctor/offline";//?mobile=
    String UserMOBILE = "usermobile";
    boolean conn = false;
    ConnectivityManager cm;


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("BGServices", "onStart");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);

        if (isOnline()) {
            new AsyncRequest().execute(OFFLINE + "?mobile=" + LoadPref(UserMOBILE));

        }
        return START_STICKY;
    }


    public String LoadPref(String key) {
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public class AsyncRequest extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

//		String json;
//		try {
//			json = GETURL(urls[0]);
//			return json;
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "ClientProtocolException";
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "Input/Output error please check connectivity";
//		}
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            if (result.contains("offline")) {
                Log.e("API offline ", ":" + result);
                SavePref("offline", "offline");
                onDestroy();
            } else {
                Log.e("API offline Error", ":" + result);
                Toast.makeText(OfflineServices.this, result, Toast.LENGTH_SHORT);
                onDestroy();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
            Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
            conn = true;
        } else {
            Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
            Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
            conn = false;
        }

        return conn;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.e("offlineServices", "onDestroy");

    }
}