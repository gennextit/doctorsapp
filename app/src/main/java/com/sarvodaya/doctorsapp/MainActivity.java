package com.sarvodaya.doctorsapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.pubnub.api.Pubnub;
import com.sarvodaya.doctorsapp.dialog.ChangeDoctorDetail;
import com.sarvodaya.doctorsapp.doctor.HomeMenu;
import com.sarvodaya.doctorsapp.doctor.ipd.EditDischargeSummary;
import com.sarvodaya.doctorsapp.doctor.ipd.IPDFragment;
import com.sarvodaya.doctorsapp.doctor.opd.OPDFragment;
import com.sarvodaya.doctorsapp.doctor.otBooking.OTBooking;
import com.sarvodaya.doctorsapp.doctor.selfHelp.SelfHelpMenu;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.L;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements ChangeDoctorDetail.ChangeDoctorListener {
    ImageView drImage, drImageBackground, btnOpd, btnIpd, btnHelp, btnOtBooking, btnSOS, btnChat;
    Pubnub pubnub;
    LinearLayout lvMenu;
    FragmentManager manager;
    TextView tvTitle;
    private Activity context;
    private PermissionListener onPermissionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;
        lvMenu = (LinearLayout) findViewById(R.id.ll_actionbar_home);
        tvTitle = (TextView) findViewById(R.id.actionbar_title);
        setHeading(getSt(R.string.doctor_s_app));
        manager = getSupportFragmentManager();

        SetDrawer(context, lvMenu);

        setHomeScreen();

        setCallPermission();
    }

    private void setCallPermission() {
        onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent ob = new Intent(Intent.ACTION_CALL);
                ob.setData(Uri.parse("tel:" + "105959"));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(ob);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(context, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void setHeading(String title) {
        setHeading(tvTitle, title);
    }


    private void setHomeScreen() {
        addFragmentWithoutBackstack(HomeMenu.newInstance(), R.id.container_main, "homeMenu");

    }

    public void setOPD() {
        if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
            addFragment(OPDFragment.newInstance(context), "opdFragment");
        } else {
            Toast.makeText(context, "Not working in test mode", Toast.LENGTH_LONG).show();
        }
    }

    public void setIPD() {
        if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
            addFragment(IPDFragment.newInstance(context, Doctor.getDoctorName(context)
                    , Doctor.getDoctorIpdId(context)), "ipdFragment");
        } else {
            ChangeDoctorDetail.newInstance(MainActivity.this, Doctor.getDoctorIpdId(context), "NA")
                    .show(getSupportFragmentManager(), "");
        }

    }

    public void setOTBooking() {
        if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
            addFragment(OTBooking.newInstance(context), "otBooking");
        } else {
            Toast.makeText(context, "Not working in test mode", Toast.LENGTH_LONG).show();
        }
    }

    public void setSelfHelp() {
        if (AppSettings.APP_TEST_PROD == AppTokens.APP_UNDER_PRODUCTION) {
            addFragment(SelfHelpMenu.newInstance(context), "selfHelpMenu");
        } else {
            Toast.makeText(context, "Not working in test mode", Toast.LENGTH_LONG).show();
        }
    }

    public void setSOSOption() {
        new TedPermission(MainActivity.this)
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.permission_denied_explanation))
                .setRationaleMessage(getString(R.string.permission_rationale))
                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }

    public void setChatOption() {
        Intent intent = new Intent(context, OnlineUsers.class);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EditDischargeSummary fragment = (EditDischargeSummary) getSupportFragmentManager().findFragmentByTag("editDischargeSummary");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, requestCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (manager.getBackStackEntryCount() > 0) {
            //L.m("MainActivity" + "popping backstack: "+mannager.getBackStackEntryCount());
            int count = manager.getBackStackEntryCount();
            //mannager.popBackStack("mainRSSFeed", 0);
            manager.popBackStack();
            if (count <= 1) {
                setHeading(getSt(R.string.doctor_s_app));
            }
//			for(int i=count-1;i>=0;i--){
//				FragmentManager.BackStackEntry entry=mannager.getBackStackEntryAt(i);
//				L.m("BackButton pressed "+entry.getName());
//			}
        } else {
            L.m("MainActivity" + "nothing on backstack, calling super");
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onChangeDoctorClick(ChangeDoctorDetail modeOfPaymentDialog, String newDoctorId, String newDoctorName) {
        addFragment(IPDFragment.newInstance(context, newDoctorName
                , newDoctorId), "ipdFragment");
    }
}
