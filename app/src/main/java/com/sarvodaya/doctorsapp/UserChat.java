package com.sarvodaya.doctorsapp;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.sarvodaya.doctorsapp.model.JSONCreatorClass;
import com.sarvodaya.doctorsapp.model.UserChatAdapter;
import com.sarvodaya.doctorsapp.model.UserChatModel;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class UserChat extends BaseActivity {
	Pubnub pubnub;
    
    
    //private static final String USER = "Aman";
    //private static final String CUSER = "Abhijit";
	ProgressDialog pDialog;
	EditText etMsg;
	Button send;
	ListView lv;
	String cDoctorName="",cDoctorMobile="",DoctorName="",DoctorMobile="";//,cId="",Id=""
	ArrayList<UserChatModel> sList;
	ArrayList<UserChatModel> viewList;
	ArrayList<UserChatModel> viewListForOthers;
	
	UserChatAdapter adapter;
	Utility util;
	List<UserChatModel> ChatList;
	boolean typingFlag=false;
	private NotificationManager mNotificationManager;
    List<UserChatModel> tempChatList1;
    
    boolean ProcessingFlag=false;
	TextView tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_chat);
		util=new Utility(UserChat.this);
		cDoctorName=LoadPreferences(AppTokens.ClientDOCTORNAME);
		cDoctorMobile=LoadPreferences(AppTokens.ClientMOBILE);
		//cId=LoadPreferences("cDoctorId");
		tvTitle=(TextView) findViewById(R.id.actionbar_title);

		LinearLayout backButton = (LinearLayout) findViewById(R.id.ll_actionbar_back);
		setActionBack(this,backButton);
		
		DoctorName= Doctor.getDoctorName(this);
		DoctorMobile=Doctor.getDoctorMobile(this);
		//Id=LoadPreferences("DoctorId");
		
//		cDoctor="Dr. RVS Bhalla";
//		cId="LSHHI81";
//		
//		Doctor="Dr. Shruti Kohli";
//		Id="LSHHI10";
		
		
		if(!cDoctorName.equals("")){
			setHeading(tvTitle,cDoctorName);
		}
		init();
		
		
		etMsg=(EditText)findViewById(R.id.et_user_chat_sendMessage);
		send=(Button)findViewById(R.id.btn_user_chat_send);
		lv = ( ListView )findViewById( R.id.lv_user_chat_chatList );

		sList = new ArrayList<UserChatModel>();
		viewList = new ArrayList<UserChatModel>();
        adapter = new UserChatAdapter(UserChat.this,R.layout.user_chat_custom_right_slot, viewList);
        lv.setAdapter(adapter);
        
        
		send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(typingFlag&&!etMsg.getText().toString().equals("")){
					String ukey=DoctorName+"_"+DoctorMobile;
					String ckey=cDoctorName+"_"+cDoctorMobile;
					Log.e("SendButtonClicked", ukey+"-"+ckey+":"+etMsg.getText().toString());
					String storeTxt=ukey+"-"+ckey+":"+etMsg.getText().toString();
					publish(AppTokens.CHANNEL,storeTxt);
					sendTxtMessage(etMsg.getText().toString());
				}else{
					Toast.makeText(getApplicationContext(), "Please wait while connecting.", Toast.LENGTH_LONG).show();
	                   
				}
			}

			
		});
		etMsg.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	
                	if(typingFlag&&!etMsg.getText().toString().equals("")){
                		String ukey=DoctorName+"_"+DoctorMobile;
        				String ckey=cDoctorName+"_"+cDoctorMobile;
        				
                    	Log.e("SendButtonClicked", ukey+"-"+ckey+":"+etMsg.getText().toString());
        				
                    	String storeTxt=ukey+"-"+ckey+":"+etMsg.getText().toString();
        				publish(AppTokens.CHANNEL,storeTxt);
        				sendTxtMessage(etMsg.getText().toString());
        				return true;
                	}else{
    					Toast.makeText(getApplicationContext(), "Please wait while connecting.", Toast.LENGTH_LONG).show();
 	                   
    				}
              }
            return false;
        }});
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		SavePreferences(AppTokens.isNotificationActive,"false");
		
		super.onResume();
		 if(!LoadPreferences(AppTokens.isServicesStart).equalsIgnoreCase("stop")){
			 stopNewService();
		 }
		
		//disconnectAndResubscribe();
		
		
		if(!LoadPreferences(AppTokens.currentChatHistory).equalsIgnoreCase("")){
			LoadPreviousChatHistory(LoadPreferences(AppTokens.currentChatHistory));
		}
		
		if(!LoadPreferences(AppTokens.tempChatHistory).equalsIgnoreCase("")){
			Log.e("tempChatHistory",":"+ LoadPreferences(AppTokens.tempChatHistory));
			LoadPreviousChatHistory(LoadPreferences(AppTokens.tempChatHistory));
		}
		
	}
	
	
	private void LoadPreviousChatHistory(String result) {
		// TODO Auto-generated method stub
		int loadListFlag=0;
		if (result.contains("[")) {
			String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
			
			try {
				JSONArray jArray = new JSONArray(finalResult);
	            for(int i=0;i<jArray.length();i++)
	            {
	            	JSONObject data = jArray.getJSONObject(i);
	            	if(data.optString("uname").equalsIgnoreCase(DoctorMobile)&&data.optString("cuname").equalsIgnoreCase(cDoctorMobile)
	            	||data.optString("uname").equalsIgnoreCase(cDoctorMobile)&&data.optString("cuname").equalsIgnoreCase(DoctorMobile) ){
	            		loadListFlag=1;
	            		UserChatModel slot=new UserChatModel();
	                	slot.setUName(data.optString("uname"));
	                	slot.setCUname(data.optString("cuname"));
	                	slot.setText(data.optString("text"));
	                	slot.setTime(data.optString("time"));
	                	slot.setDate(data.optString("date"));
	                	slot.setPosition(data.optString("Position"));
	                	viewList.add(slot);
	            	}
	            	
	            	UserChatModel slot1=new UserChatModel();
                	slot1.setUName(data.optString("uname"));
                	slot1.setCUname(data.optString("cuname"));
                	slot1.setText(data.optString("text"));
                	slot1.setTime(data.optString("time"));
                	slot1.setDate(data.optString("date"));
                	slot1.setPosition(data.optString("Position"));
                	sList.add(slot1);
					
				}
	            if(loadListFlag==1){
	            	adapter.notifyDataSetChanged();
		            
		            lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		            lv.setStackFromBottom(true);
	            }
	            
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	
	
	public void sendTxtMessage(String msg){
		   
		UserChatModel slot=new UserChatModel();
    	slot.setUName(DoctorMobile);
    	slot.setCUname(cDoctorMobile);
    	slot.setText(msg);
    	slot.setTime(getTime());
    	slot.setDate(getDate());
    	slot.setPosition("right");
    	viewList.add(slot);
    	sList.add(slot);
    	
        adapter.notifyDataSetChanged();
        
        etMsg.setText("");
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        lv.setStackFromBottom(true);
	
	}
	public void reciveTxtMessage(String msg){
		   
		UserChatModel slot=new UserChatModel();
    	slot.setUName(DoctorMobile);
    	slot.setCUname(cDoctorMobile);
    	slot.setText(msg);
    	slot.setTime(getTime());
    	slot.setDate(getDate());
    	slot.setPosition("left");
    	viewList.add(slot);
    	sList.add(slot);
    	
        adapter.notifyDataSetChanged();
        
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        lv.setStackFromBottom(true);
	
	}
//	public void reciveTxtMessageFromOthers(String uMobile,String cMobile,String msg){
//		   
//		UserChatModel slot=new UserChatModel();
//    	slot.setUName(uMobile);
//    	slot.setCUname(cMobile);
//    	slot.setText(msg);
//    	slot.setTime(getTime());
//    	slot.setDate(getDate());
//    	slot.setPosition("left");
//    	viewListForOthers.add(slot);
//    	
//    }
	
	public void reciveTxtOffline(String msg){
		   
		UserChatModel slot=new UserChatModel();
    	slot.setUName(DoctorMobile);
    	slot.setCUname(cDoctorMobile);
    	slot.setText(cDoctorName+" "+msg);
    	slot.setTime(getTime());
    	slot.setDate(getDate());
    	slot.setPosition("center");
    	viewList.add(slot);
    	//sList.add(slot);
    	
        adapter.notifyDataSetChanged();
        
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        lv.setStackFromBottom(true);
	
	}
	private void init() {
		pubnub = new Pubnub( AppTokens.PUBLISH_KEY, AppTokens.SUBSCRIBE_KEY);
		subscribe(AppTokens.CHANNEL);
    }

	public String getTime(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("h:mm a");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df.format(c.getTime());
		
	}
  public String getDate() {
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	    String formattedDate = df.format(c.getTime());
	    return formattedDate;
	}
	
	private void notifyUser(Object message,final String method) {
		
		 if(isOnline()){
			 try {
		            if (message instanceof JSONObject) {
		                final JSONObject obj = (JSONObject) message;
		                this.runOnUiThread(new Runnable() {
		                    public void run() {
		                    	
		                        //Toast.makeText(getApplicationContext(), obj.toString(),  Toast.LENGTH_LONG).show();

		                        Log.e("Received msg : 1", String.valueOf(obj)+" Method :"+method);
		                    }
		                });

		            } else if (message instanceof String) {
		                final String obj = (String) message;
		                this.runOnUiThread(new Runnable() {
		                    public void run() {
		                    	
//		                    	if(method.equalsIgnoreCase("send")){
		                    	Log.e("Received msg : 2",obj);	
		                    		typingFlag=true;
//		                    	}else 
		                    	if(method.equalsIgnoreCase("recive")){
		                    		//Dr. Lucky Kharbanda7053500601-Dr. Lucky Kharbanda$7053500601:online
		                    		
		                    		if(obj.contains(":")&&obj.contains("-")&&obj.contains("_")){
		                    			
		                    			String[] separated = obj.split(":");
		                        		String mixId=separated[0]; // this will contain "id1-id2"
		                        		String msg=separated[1];
		                        		
		                        		String[] separated2 = mixId.split("-");
		                        		String ukey=separated2[0]; // this will contain "id1"
		                        		String ckey=separated2[1];
		                        		
		                        		String[] separated3 = ukey.split("_");
		                        		String uName=separated3[0]; // this will contain "id1"
		                        		String uMobile=separated3[1];
		                        		
		                        		String[] separated4 = ckey.split("_");
		                        		String cName=separated4[0]; // this will contain "id1"
		                        		String cMobile=separated4[1];
		                        		
		                        		if(cMobile.equalsIgnoreCase(cDoctorMobile)&&msg.equalsIgnoreCase("offline")){
		                        			reciveTxtOffline(msg);
		                            		
		                        		}else if(cMobile.equalsIgnoreCase(cDoctorMobile)&&msg.equalsIgnoreCase("online")){
		                        			reciveTxtOffline(msg);
		                        		}else{

		                            		if(uMobile.equalsIgnoreCase(cDoctorMobile)&&cMobile.equalsIgnoreCase(DoctorMobile)){
		                            			reciveTxtMessage(msg);
		                            		}else if(!uMobile.equalsIgnoreCase(cDoctorMobile)&&cMobile.equalsIgnoreCase(DoctorMobile)){
		                            			//reciveTxtMessageFromOthers(uMobile,cMobile,msg);
		                            		}
		                        		}
		                        		
		                    		}else{
		                    			 Log.e("Received msg : 2",obj);
		                    		}
		                    		
		                    		
		                    		
		                    	}
		                        //Toast.makeText(getApplicationContext(), obj, Toast.LENGTH_LONG).show();
		                        //Log.e("Received msg : 2",obj);
		                    }
		                });

		            } else if (message instanceof JSONArray) {
		                final JSONArray obj = (JSONArray) message;
		                this.runOnUiThread(new Runnable() {
		                    public void run() {
		                    	
		                        //Toast.makeText(getApplicationContext(), obj.toString(), Toast.LENGTH_LONG).show();
		                        Log.e("Received msg 3: ", obj.toString());
		                    }
		                });
		            }

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		 }
		
        
    }
	

//	   private void showNotification(String cName,String cMobile,String title,String msg) {
//		   Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//		   Intent intent= new Intent(this, UserChat.class);
////		   intent.putExtra("cDoctor", cUserId);
////		   intent.putExtra("cId", cUserId);
//		   UserChatModel model=new UserChatModel();
//		   
//		   		//if(LoadPref("tempCDoctorName").equals("")||LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)){
//		   			model.setChatList(tempChatList1);
//					String storeJSONMsg=JSONCreatorClass.toJSonArray(model);
//					SavePref(AppTokens.tempChatHistory, storeJSONMsg);
//					SavePref(AppTokens.ClientDOCTORNAME,cName);
//	        		SavePref(AppTokens.ClientMOBILE,cMobile);
//	        		
////				}else if(!LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)){
////					
////					model.setChatList(tempChatList1);
////					String storeJSONMsg=JSONCreatorClass.toJSonArray(model);
////					SavePref(AppTokens.tempChatHistory, storeJSONMsg);
////					SavePref("cDoctorName",cUserId);
////				}
//			
//	       PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
//	       Notification notification = new Notification.Builder(this)
//	       .setSound(soundUri)
//	       .setAutoCancel(true)
//	       .setContentTitle(title)
//	       .setContentText(msg).setSmallIcon(R.drawable.ic_launcher)
//	       .setContentIntent(contentIntent).build();
//	       mNotificationManager = 
//	                 (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//	       notification.flags = notification.flags
//	               | Notification.FLAG_ONGOING_EVENT;
//	       notification.flags |= Notification.FLAG_AUTO_CANCEL;
//	       
//	       mNotificationManager.notify(0, notification); 
//
//	   }
//	   
//	   public void cancelNotification(int notificationId){
//
//	       if (Context.NOTIFICATION_SERVICE!=null) {
//	           String ns = Context.NOTIFICATION_SERVICE;
//	           NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
//	           nMgr.cancel(notificationId);
//	       }
//	   }
	
	// this method is used for send message.
	private void publish(final String channel,String sendTxt) {
		Callback publishCallback = new Callback() {
            @Override
            public void successCallback(String channel,
                                        Object message) {
                notifyUser(message,"send");
                
            }

            @Override
            public void errorCallback(String channel,
                                      PubnubError error) {
                notifyUser("PUBLISH : " + error,"error");
            }
        };

        try {
            Integer i = Integer.parseInt(sendTxt);
            pubnub.publish(channel, i, publishCallback);
            return;
        } catch (Exception e) {
        }

        try {
            Double d = Double.parseDouble(sendTxt);
            pubnub.publish(channel, d, publishCallback);
            return;
        } catch (Exception e) {
        }


        try {
            JSONArray js = new JSONArray(sendTxt);
            pubnub.publish(channel, js, publishCallback);
            return;
        } catch (Exception e) {
        }

        try {
            JSONObject js = new JSONObject(sendTxt);
            pubnub.publish(channel, js, publishCallback);
            return;
        } catch (Exception e) {
        }

        pubnub.publish(channel, sendTxt, publishCallback);
    }

	private void subscribe(String channel) {
		try {
            pubnub.subscribe(channel, new Callback() {
                @Override
                public void connectCallback(String channel,
                                            Object message) {
                    notifyUser("SUBSCRIBE : CONNECT on channel:"
                            + channel
                            + " MESSAGE : "
                            + message.toString(),"error");
                }

                @Override
                public void disconnectCallback(String channel,
                                               Object message) {
                    notifyUser("SUBSCRIBE : DISCONNECT on channel:"
                    		+ channel
                            + " MESSAGE : "
                            + message.toString(),"error");
                }

                @Override
                public void reconnectCallback(String channel,
                                              Object message) {
                    notifyUser("SUBSCRIBE : RECONNECT on channel:"
                    		+ channel
                            + " MESSAGE : "
                            + message.toString(),"error");
                }

                @Override
                public void successCallback(String channel,
                                            Object message) {
                    notifyUser(message.toString(),"recive");
                }

                @Override
                public void errorCallback(String channel,
                                          PubnubError error) {
                	//Toast.makeText(UserChat.this, "Internet connection not working properly", Toast.LENGTH_LONG).show();
                    Log.e("errorCallback", "Internet connection not working properly");
                    notifyUser("SUBSCRIBE : ERROR on channel "
                    		+ channel
                            + " erROR : "
                            + error.toString(),"error");
                }
            });

        } catch (Exception e) {

        }

    }
	//Enter channel name
	
	 private void unsubscribe(String channel) {
			Log.e("unsubscribe", "Success");
			UserOffline() ;
	 		pubnub.unsubscribe(channel);
	 	 }
	 
	 private void UserOffline() {
			// TODO Auto-generated method stub
			Log.e("unsubscribe", "Success");
			String ukey=Doctor.getDoctorName(this)+"_"+Doctor.getDoctorMobile(this);
			String ckey=Doctor.getDoctorName(this)+"_"+Doctor.getDoctorMobile(this);
			
	    	String storeTxt=ukey+"-"+ckey+":"+"offline";
			if(isConnectionAvailable()){
				 publish(AppTokens.CHANNEL,storeTxt); 
			}
				
			
		}
	 
//	 private void disconnectAndResubscribe() {
//	        pubnub.disconnectAndResubscribe();
//
//	 }

	 public void startNewService() {
			
			startService(new Intent(this, ChatServices.class));
		}

		// Stop the  service
		public void stopNewService() {
			
			stopService(new Intent(this, ChatServices.class));
		}
	 
	@Override
	// Detect when the back button is pressed
	public void onBackPressed() {
		//Toast.makeText(UserChat.this, "You are going offline.", Toast.LENGTH_LONG).show();
        Log.e("onBackPressed", "You are going offline.");
        if(!ProcessingFlag){
			executeStop();
		}
	 }

	private void executeStop() {
		// TODO Auto-generated method stub
		ProcessingFlag=true;
       
		SavePreferences(AppTokens.tempChatHistory, "");
		SavePreferences(AppTokens.isNotificationActive,"true");
		SavePreferences("cDoctorName","");
		SavePreferences("cDoctorId","");
		unsubscribe(AppTokens.CHANNEL);
		SaveCurrentChatHistory();
		//this.moveTaskToBack(true);
		finish();
		 
	}

	private void SaveCurrentChatHistory() {
		// TODO Auto-generated method stub
		ChatList = new ArrayList<UserChatModel>();
		
		if(!sList.isEmpty()){
			for (UserChatModel m1 : sList) 
			{
				UserChatModel m2=new UserChatModel();
				m2.setUName(m1.getUName());
				m2.setCUname(m1.getCUName());
				m2.setText(m1.getText());
				m2.setDate(m1.getDate());
				m2.setTime(m1.getTime());
				m2.setPosition(m1.getPosition());
				
				ChatList.add(m2);
				
			}
			
			UserChatModel model=new UserChatModel();
			model.setChatList(ChatList);
			String storeCurrentJSONChat=JSONCreatorClass.toJSonArray(model);
			
			SavePreferences(AppTokens.currentChatHistory,storeCurrentJSONChat);
			
			
			
		}
//		if(!viewListForOthers.isEmpty()){
//			for (UserChatModel m1 : viewListForOthers) 
//			{
//				UserChatModel m2=new UserChatModel();
//				m2.setUName(m1.getUName());
//				m2.setCUname(m1.getCUName());
//				m2.setText(m1.getText());
//				m2.setDate(m1.getDate());
//				m2.setTime(m1.getTime());
//				m2.setPosition(m1.getPosition());
//				
//				ChatList.add(m2);
//				
//			}
//			
//			UserChatModel model=new UserChatModel();
//			model.setChatList(ChatList);
//			String storeCurrentJSONChat=JSONCreatorClass.toJSonArray(model);
//			
//			SavePreferences(AppTokens.othersChatHistory,storeCurrentJSONChat);
//		
//		
//	}
		}

}