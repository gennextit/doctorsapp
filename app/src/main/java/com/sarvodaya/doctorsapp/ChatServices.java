package com.sarvodaya.doctorsapp;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.sarvodaya.doctorsapp.model.JSONCreatorClass;
import com.sarvodaya.doctorsapp.model.UserChatModel;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.Doctor;
import com.sarvodaya.doctorsapp.util.Utility;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class ChatServices extends Service {
	Pubnub pubnub;
    String tempDrId="";
    SharedPreferences sharedPreferences ;
    private NotificationManager mNotificationManager;
    List<UserChatModel> tempChatList;
    String storeMsg="";
    //List<UserChatModel> tempChatList1;
    //Utility util;
    boolean conn=false;
	ConnectivityManager cm ;
    
   @Override
   public IBinder onBind(Intent arg0) {
      return null;
   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
      // Let it continue running until it is stopped.
	  // util=new Utility(this);
     // Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
	  
	  tempChatList = new ArrayList<UserChatModel>();
	  sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
	  cm = (ConnectivityManager)getSystemService(this.CONNECTIVITY_SERVICE);
	  
	  SavePref(AppTokens.tempChatHistory, "");
      SavePref(AppTokens.isNotificationActive, "true");
      init();
      SavePref(AppTokens.isServicesStart, "start");
      SavePref(AppTokens.AppStop,"false");
      return START_STICKY;
   }
   
   private void init() {
	   
	   if(isOnline()){
		   pubnub = new Pubnub( AppTokens.PUBLISH_KEY, AppTokens.SUBSCRIBE_KEY);
		   subscribe(AppTokens.CHANNEL);
	   }
		
   }
   
   private void notifyUser(Object message,final String method) {
	   
	   if(isOnline()){
		   try {
	           if (message instanceof JSONObject) {
	               final JSONObject obj = (JSONObject) message;
	               
	               Log.e("Received msg : 1", String.valueOf(obj)+" Method :"+method);
	                
	               

	           } else if (message instanceof String) {
	               final String obj = (String) message;
	               
	                   	if(method.equalsIgnoreCase("recive")){
	                   		String[] separated = obj.split(":");
	                		String mixId=separated[0]; // this will contain "id1-id2"
	                		String msg=separated[1];
	                		
	                		String[] separated2 = mixId.split("-");
	                		String ukey=separated2[0]; // this will contain "id1"
	                		String ckey=separated2[1];
	                		
	                		String[] separated3 = ukey.split("_");
	                		String uName=separated3[0]; // this will contain "id1"
	                		String uMobile=separated3[1];
	                		
	                		String[] separated4 = ckey.split("_");
	                		String cName=separated4[0]; // this will contain "id1"
	                		String cMobile=separated4[1];
	                		
	                   		//tempDrId=id;
	                		
	                		if(!uMobile.equalsIgnoreCase(Doctor.getDoctorMobile(this))&&cMobile.equalsIgnoreCase(Doctor.getDoctorMobile(this))){
	                   			//reciveTxtMessage(msg);
	                   			//if(LoadPref("tempCDoctorName").equals("")||LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)&&LoadPref(AppTokens.isNotificationActive).equalsIgnoreCase("true")){
	                   				//SavePref("tempCDoctorName",tempDrId);
	                       			UserChatModel slot=new UserChatModel();
	                       	    	slot.setUName(uMobile);
	                       	    	slot.setCUname(cMobile);
	                       	    	slot.setText(msg);
	                       	    	slot.setTime(getTime());
	                       	    	slot.setDate(getDate());
	                       	    	slot.setPosition("left");
	                       	    	tempChatList.add(slot);
	                				
	                       			storeMsg=msg;
	                       			showNotification(uName,uMobile,uName+" Sending a new message",storeMsg);
	                   			//}
//	                   			else if(!LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)){
//	                   				SavePref("tempCDoctorName",tempDrId);
//	                       			UserChatModel slot1=new UserChatModel();
//	                       	    	slot1.setUName(id);
//	                       	    	slot1.setCUname(cid);
//	                       	    	slot1.setText(msg);
//	                       	    	slot1.setTime(getTime());
//	                       	    	slot1.setDate(getDate());
//	                       	    	slot1.setPosition("left");
//	                       	    	tempChatList1.add(slot1);
//	                				
//	                       			storeMsg=msg;
//	                       			showNotification(id,id+" Sending a new message",storeMsg);
//	                   			}
	                   			
	                   		}
	                   	}
	                       //Toast.makeText(getApplicationContext(), obj, Toast.LENGTH_LONG).show();
	                       Log.e("Received msg : 2",obj);
	                

	           } else if (message instanceof JSONArray) {
	               final JSONArray obj = (JSONArray) message;
	              	
	                       //Toast.makeText(getApplicationContext(), obj.toString(), Toast.LENGTH_LONG).show();
	                       Log.e("Received msg 3: ", obj.toString());
	                 
	           }

	       } catch (Exception e) {
	           e.printStackTrace();
	       }
	   }
       
   }
   //.setContentTitle(this.getString(R.string.service_label))
   @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
   private void showNotification(String cName, String cMobile, String title, String msg) {
	   Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

	   Intent intent= new Intent(this, UserChat.class);
//	   intent.putExtra("cDoctor", cUserId);
//	   intent.putExtra("cId", cUserId);
	   UserChatModel model=new UserChatModel();
	   
	   		//if(LoadPref("tempCDoctorName").equals("")||LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)){
	   			model.setChatList(tempChatList);
				String storeJSONMsg=JSONCreatorClass.toJSonArray(model);
				SavePref(AppTokens.tempChatHistory, storeJSONMsg);
				SavePref(AppTokens.ClientDOCTORNAME,cName);
        		SavePref(AppTokens.ClientMOBILE,cMobile);
        		
//			}else if(!LoadPref("tempCDoctorName").equalsIgnoreCase(tempDrId)){
//				
//				model.setChatList(tempChatList1);
//				String storeJSONMsg=JSONCreatorClass.toJSonArray(model);
//				SavePref(AppTokens.tempChatHistory, storeJSONMsg);
//				SavePref("cDoctorName",cUserId);
//			}
		
       PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
       Notification notification = new Notification.Builder(this)
       .setSound(soundUri)
       .setAutoCancel(true)
       .setContentTitle(title)
       .setContentText(msg).setSmallIcon(R.mipmap.ic_launcher)
       .setContentIntent(contentIntent).build();
       mNotificationManager = 
                 (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
       notification.flags = notification.flags
               | Notification.FLAG_ONGOING_EVENT;
       notification.flags |= Notification.FLAG_AUTO_CANCEL;
       
       mNotificationManager.notify(0, notification); 

   }
   
   public void cancelNotification(int notificationId){

       if (Context.NOTIFICATION_SERVICE!=null) {
           String ns = Context.NOTIFICATION_SERVICE;
           NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
           nMgr.cancel(notificationId);
       }
   }
   
   public String LoadPref(String key){   
	    String  data = sharedPreferences.getString(key, "") ;
	    return data;
   }
   public void SavePref(String key, String value){
       SharedPreferences.Editor editor = sharedPreferences.edit();
       editor.putString(key, value);
       editor.commit();
   }
   private void subscribe(String channel) {
	   if(isOnline()){
		   try {
	           pubnub.subscribe(channel, new Callback() {
	               @Override
	               public void connectCallback(String channel,
	                                           Object message) {
	                   notifyUser("SUBSCRIBE : CONNECT on channel:"
	                           + channel
	                           + " MESSAGE : "
	                           + message.toString(),"error");
	               }

	               @Override
	               public void disconnectCallback(String channel,
	                                              Object message) {
	                   notifyUser("SUBSCRIBE : DISCONNECT on channel:"
	                   		+ channel
	                           + " MESSAGE : "
	                           + message.toString(),"error");
	               }

	               @Override
	               public void reconnectCallback(String channel,
	                                             Object message) {
	                   notifyUser("SUBSCRIBE : RECONNECT on channel:"
	                   		+ channel
	                           + " MESSAGE : "
	                           + message.toString(),"error");
	               }

	               @Override
	               public void successCallback(String channel,
	                                           Object message) {
	                   notifyUser(message.toString(),"recive");
	               }

	               @Override
	               public void errorCallback(String channel,
	                                         PubnubError error) {
	            	   //Toast.makeText(ChatServices.this, "Internet connection not working properly", Toast.LENGTH_LONG).show();
	                   Log.e("errorCallback", "Internet connection not working properly");
//	                   notifyUser("SUBSCRIBE : ERROR on channel "
//	                   		+ channel
//	                           + " erROR : "
//	                           + error.toString(),"error");
	               }
	           });

	       } catch (Exception e) {

	       }
	   }
		
		
   }
   
// this method is used for send message.
	private void publish(final String channel,String sendTxt) {
		Callback publishCallback = new Callback() {
            @Override
            public void successCallback(String channel,
                                        Object message) {
                notifyUser(message,"send");
                
            }

            @Override
            public void errorCallback(String channel,
                                      PubnubError error) {
                notifyUser("PUBLISH : " + error,"error");
            }
        };

        try {
            Integer i = Integer.parseInt(sendTxt);
            pubnub.publish(channel, i, publishCallback);
            return;
        } catch (Exception e) {
        }

        try {
            Double d = Double.parseDouble(sendTxt);
            pubnub.publish(channel, d, publishCallback);
            return;
        } catch (Exception e) {
        }


        try {
            JSONArray js = new JSONArray(sendTxt);
            pubnub.publish(channel, js, publishCallback);
            return;
        } catch (Exception e) {
        }

        try {
            JSONObject js = new JSONObject(sendTxt);
            pubnub.publish(channel, js, publishCallback);
            return;
        } catch (Exception e) {
        }

        pubnub.publish(channel, sendTxt, publishCallback);
    }
   
 //Enter channel name
	 private void unsubscribe(String channel) {
		 if(isOnline()){
			 pubnub.unsubscribe(channel);
		 }
			
	 	 }
   
 	public String getTime(){
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("h:mm a");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    return df.format(c.getTime());
		
	}
  public String getDate() {
		Calendar c = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	    String formattedDate = df.format(c.getTime());
	    return formattedDate;
	}
  public boolean isOnline() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;
	    
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    if(haveConnectedWifi==true || haveConnectedMobile==true){
	    	Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
	    	Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
	    	conn=true;
	    }else{
	    	Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
	    	Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
	    	conn=false;
	    }
	    
	    return conn;
	}
 	 
   @Override
   public void onDestroy() {
      super.onDestroy();
      SavePref(AppTokens.isNotificationActive, "false");
      unsubscribe(AppTokens.CHANNEL);
      UserOffline();
      cancelNotification(0);

      //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
      SavePref(AppTokens.isServicesStart, "stop");
   }

private void UserOffline() {
	// TODO Auto-generated method stub
	Log.e("unsubscribe", "Success");
	if(!LoadPref("AppStop").equalsIgnoreCase("true")){
		
		String ukey=Doctor.getDoctorName(this)+"_"+Doctor.getDoctorMobile(this);
		String ckey=Doctor.getDoctorName(this)+"_"+Doctor.getDoctorMobile(this);
		
    	String storeTxt=ukey+"-"+ckey+":"+"online";
		if(isOnline()){
			 publish(AppTokens.CHANNEL,storeTxt); 
		}
		
	}
}
}