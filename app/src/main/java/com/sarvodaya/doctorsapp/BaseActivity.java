package com.sarvodaya.doctorsapp;


import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sarvodaya.doctorsapp.model.SideMenu;
import com.sarvodaya.doctorsapp.model.SideMenuAdapter;
import com.sarvodaya.doctorsapp.util.TypefaceSpan;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {
    boolean conn = false;
    Builder alertDialog;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

	  protected void setHeading(TextView tv,String title) {
		  	SpannableString s = new SpannableString(title);
			s.setSpan(new TypefaceSpan(BaseActivity.this, "CircularStd-Book.otf"), 0, s.length(),
			            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
			s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			tv.setText(s);
	  }

    protected void setActionBack(final Activity activity, LinearLayout backButton) {

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }
//	  public boolean isConnected(){
//
//		  if (isOnline()==false){
//			  	//showAlertInternet(getSt(R.string.internet_error_tag),getSt(R.string.internet_error_msg), false);
//	    	  	return false;
//		  }else{
//				return true;
//		  }
//	  }


    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public boolean isConnectionAvailable() {

        if (isOnline() == false) {
            return false;
        } else {
            return true;
        }
    }

    public String LoadPreferences(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(BaseActivity.this);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePreferences(String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(BaseActivity.this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

//    public void serverTimeOut(String result) {
//        Utility.showToast(this, getResources().getString(R.string.server_time_out) + result, false);
//    }

    public boolean isOnline() {
//        boolean haveConnectedWifi = false;
//        boolean haveConnectedMobile = false;
//
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        if (haveConnectedWifi == true || haveConnectedMobile == true) {
//            Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
//            Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
//            conn = true;
//        } else {
//            Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
//            Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
//            conn = false;
//        }

        return true;
    }

    //	  public void showAlertInternet( String title, String message,Boolean status) {
//			 alertDialog = new Builder(BaseActivity.this);
//
//			// Setting Dialog Title
//			alertDialog.setTitle(title);
//
//			// Setting Dialog Message
//			alertDialog.setMessage(message);
//
//			if(status != null)
//				// Setting alert dialog icon
//				alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
//
//			// On pressing Settings button
//	       alertDialog.setPositiveButton("SETTING", new DialogInterface.OnClickListener() {
//	           public void onClick(DialogInterface dialog,int which) {
//	           	EnableMobileIntent();
//	           }
//	       });
//			// Showing Alert Message
//			alertDialog.show();
//		}

    public String getSt(int id) {
        return getResources().getString(id);
    }

    private void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public String viewTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getTime12() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getTime24() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
        df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df1.format(c.getTime());
    }

    public String getTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());
    }

    // give format like dd-MMM-yyyy,dd/MMM/yyyy
    public String viewDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String vDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    // Enter Format eg.dd-MMM-yyyy,dd/MM/yyyy
    public String viewFormatDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;

    }

    // missing 0 issue resolve
    // enter Date eg. dd-MM-yyyy
    // enter dateFormat eg. dd-MM-yyyy
    public String currectFormateDate(String Date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date cDate = null;
        try {
            cDate = sdf.parse(Date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String formattedDate = sdf.format(cDate);
        return formattedDate;
    }

    // enter fix Date eg. dd-MM-yyyy
    // enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
    public String convertFormateDate(String Date, String dateFormat) {
        //String s = "12:18:00";
        String finalDate = Date;
        String Day = Date.substring(0, 2);
        String middle = Date.substring(2, 3);
        String Month = Date.substring(3, 5);
        String Year = Date.substring(6, 10);

        switch (dateFormat) {
            case "dd-MM-yyyy":
                finalDate = Day + middle + Month + middle + Year;
                break;
            case "yyyy-MM-dd":
                finalDate = Year + middle + Month + middle + Day;
                break;
            case "MM-dd-yyyy":
                finalDate = Month + middle + Day + middle + Year;
                break;
            default:
                finalDate = "Date Format Incorrest";
        }
        return finalDate;
    }


    protected void SetDrawer(final Activity act, LinearLayout iv) {
        // TODO Auto-generated method stub
//        ImageLoader imageLoader;
//        imageLoader = new ImageLoader(act);

        iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                if (!drawerOpen) {
                    dLayout.openDrawer(dList);
                } else {
                    dLayout.closeDrawer(dList);
                }

            }
        });

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
//        LayoutInflater inflater = getLayoutInflater();
//        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
//        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
//        ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
//
//        imageLoader.DisplayImage(KidProfile.getStudentImage(BaseActivity.this), ivProfileToggle, "blank", false);
//        imageLoader.DisplayImage(KidProfile.getStudentImage(BaseActivity.this), ivProfile, "blank", false);
//        tvName.setText(KidProfile.getStudentName(BaseActivity.this));
//
//
//        dList.addHeaderView(listHeaderView);
        SideMenu s1 = new SideMenu("Facebook Feeds", R.mipmap.ic_menu_facebook);
        SideMenu s2 = new SideMenu("Twitter Feeds", R.mipmap.ic_menu_twitter);
        SideMenu s3 = new SideMenu("Website", R.mipmap.ic_menu_website);
        SideMenu s4 = new SideMenu("Share With Friends", R.mipmap.ic_menu_share);

        // SideMenu s7 = new SideMenu("Logout", R.drawable.menu8);

        sideMenuList = new ArrayList<SideMenu>();
        sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        // sideMenuList.add(s6);
        // sideMenuList.add(s7);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(slideMenuAdapter);
        // dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();

                // FragmentManager mannager = getFragmentManager();
                // FragmentTransaction transaction;

                Intent intent;
                FragmentManager mannager;
                FragmentTransaction transaction;
                Intent browserIntent1;
                switch (position) {

                    case 0:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Sarvodayafaridabad"));
                        startActivity(browserIntent1);
//                        ProfileSelection profileSelection = new ProfileSelection();
//                        profileSelection.setCallStatus(ProfileSelection.BASE_ACT);
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, profileSelection, "profileSelection");
//                        transaction.addToBackStack("profileSelection");
//                        transaction.commit();
                        break;
                    case 1:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/sarvodaya_care"));
                        startActivity(browserIntent1);
                           /* CoachRequestCoin coachRequestCoin = new CoachRequestCoin();
                            mannager = getFragmentManager();
                            transaction = mannager.beginTransaction();
                            transaction.replace(android.R.id.content, coachRequestCoin, "coachRequestCoin");
                            transaction.addToBackStack("coachRequestCoin");
                            transaction.commit();*/
                        break;
                    case 2:
                        browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://sarvodayahospital.com/"));
                        startActivity(browserIntent1);
//                        Feedback feedback = new Feedback();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, feedback, "feedback");
//                        transaction.addToBackStack("feedback");
//                        transaction.commit();
                        break;
                    case 3:
                        Intent in = new Intent("android.intent.action.SEND");
                        in.setType("text/plain");
                        in.putExtra("android.intent.extra.TEXT", "Check out this app!  https://play.google.com/store/apps");
                        startActivity(Intent.createChooser(in, "Share With Fiends"));
                       /* try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
                            i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) { // e.toString();
                        }*/
                        break;
                    case 4:
                      /*  UserGuide userGuide = new UserGuide();
                        mannager = getFragmentManager();
                        transaction = mannager.beginTransaction();
                        transaction.replace(android.R.id.content, userGuide, "userGuide");
                        transaction.addToBackStack("userGuide");
                        transaction.commit();*/

                        break;

                }

            }

        });
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, String tag) {
        addFragmentWithoutBackstack(fragment,android.R.id.content, tag);
    }
    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }

}