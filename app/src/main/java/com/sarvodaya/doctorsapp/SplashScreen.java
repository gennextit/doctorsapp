package com.sarvodaya.doctorsapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sarvodaya.doctorsapp.doctor.FragSplashScreen;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class SplashScreen extends BaseActivity {
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        manager = getSupportFragmentManager();

        setSplashScreen();
    }

    private void setSplashScreen() {
        FragSplashScreen fragSplashScreen = new FragSplashScreen();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, fragSplashScreen, "fragSplashScreen");
        transaction.commit();
    }
}
