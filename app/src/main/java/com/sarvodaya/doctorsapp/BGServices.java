package com.sarvodaya.doctorsapp;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


import com.sarvodaya.doctorsapp.util.ApiCall;
import com.sarvodaya.doctorsapp.util.AppSettings;
import com.sarvodaya.doctorsapp.util.AppTokens;
import com.sarvodaya.doctorsapp.util.RequestHandler;
import com.sarvodaya.doctorsapp.util.Utility;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class BGServices extends Service {

    Utility util;
    Timer timer;
    TimerTask tTask;
    private static long ImageDelay = 5000;
    int taskFlag = 0;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("BGServices", "onStart");

        util = new Utility(this);
        ImageTimerDelay();

        return START_STICKY;
    }


    public void ImageTimerDelay() {
        // Create a Timer
        timer = new Timer();

        // Task to do when the timer ends
        tTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("BgServices TimerDelay", "Start");
                //ivGen.setVisibility(View.VISIBLE);
                if (taskFlag == 0) {
                    if (util.isConnectionAvailable()) {
                        new AsyncRequest().execute(AppSettings.ALL_USERS);
                    }

                }
            }
        };

        // Start the timer
        timer.schedule(tTask, ImageDelay);
    }

    private void stopTimer() {
        // TODO Auto-generated method stub
        if (timer != null)
            timer.cancel();
        timer = null;
        if (tTask != null)
            tTask = null;
    }

    public class AsyncRequest extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            taskFlag = 1;
        }

        @Override
        protected String doInBackground(String... urls) {
            Log.e("BGServices_background", "execute");

            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.e("BGServices_postExecute", "execute");
            taskFlag = 0;
            util.SavePreferences(AppTokens.onlineusers, result);

            //new AsyncRequest().execute(AppSettings.ALL_USERS);

            //finish();
        }

        @Override
        protected void onCancelled() {

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        Log.e("BGServices", "onDestroy");

    }
}