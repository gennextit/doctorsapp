package com.sarvodaya.doctorsapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sarvodaya.doctorsapp.doctor.login.LoginMobile;

/**
 * Created by Abhijit on 24-Sep-16.
 */

public class LoginActivity extends BaseActivity {
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        manager = getSupportFragmentManager();

        setLoginScreen();
    }

    private void setLoginScreen() {
        LoginMobile loginMobile = new LoginMobile();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, loginMobile, "loginMobile");
        transaction.commit();
    }
}
